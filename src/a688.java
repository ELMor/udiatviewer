// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

import a0.a104;
import a0.a441;
import a0.a932;
import a282.a470;
import a282.a516;
import a282.a540;
import a282.a906;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;
import java.awt.image.ImageObserver;
import java.net.URL;

public class a688 extends Panel
    implements a441
{

    URL a422;
    Viewer a692;
    Label a693;
    a470 a694;
    a470 a695;
    a470 a696;
    a470 a697;
    a470 a698;
    a470 a699;
    a470 a700;
    a470 a701;
    a470 a702;
    a470 a703;
    a516 a704;
    a906 a705;
    a470 a706;
    a470 a195;
    a470 a196;
    a470 a197;
    a470 a707;
    a470 a708;
    a470 a709;
    a470 a190;
    a470 a710;
    a470 a711;
    a470 a712;
    a470 a713;
    a470 a714;
    a470 a715;
    a470 a716;
    a470 a717;
    a470 a718[];
    a470 a719;
    a906 a362;
    a906 a720;
    String a271[];
    String a721[][];
    String a722;
    String a723;
    String a724;
    String a725;
    String a726;
    String a727;
    String a728;
    String a729;
    String a730;
    String a731;
    String a732;
    String a733;
    String a734;
    String a735;
    String a736;
    String a737;
    String a738;
    String a739;
    String a740;
    String a741;
    String a742;
    String a743;
    String a744;
    String a745;
    String a746;
    String a747;
    String a748;
    String a749;
    String a750;
    String a751;
    String a752;
    String a753;
    String a754;
    String a755;
    String a756;
    String a757;
    String a758;
    String a759;
    String a760;
    String a761;
    String a762;
    String a763;
    String a764;
    String a765;
    String a766;
    String a767;
    String a768;
    String a769;
    String a770;
    String a771;
    String a772;
    Panel a773;
    Panel a774;
    static final int a558 = 3;

    public a688(URL url, String s, boolean flag, Viewer viewer, String as[], String as1[][])
    {
        a722 = "Previous image/s";
        a723 = "Next image/s";
        a724 = "Previous 5 images/screens";
        a725 = "Next 5 images/screens";
        a726 = "First image/s";
        a727 = "Last image/s";
        a728 = "One image display";
        a729 = "Four images display";
        a730 = "Nine images display";
        a731 = "Sixteen images display";
        a732 = "Previous stack image";
        a733 = "Next stack image";
        a734 = "Previous stack series";
        a735 = "Next stack series";
        a736 = "Add stack series";
        a737 = "Delete stack series";
        a738 = "Synchronise the stacks";
        a739 = "Full image display";
        a740 = "Increment image size";
        a741 = "Decrement image size";
        a742 = "Set the scroll mode";
        a743 = "Lens, magnifying glass";
        a744 = "Increase magnifying factor";
        a745 = "Decrease magnifying factor";
        a746 = "Increase magnifying area";
        a747 = "Decrease magnifying area";
        a748 = "Rectangle region zoom";
        a749 = "Zoom the selected area";
        a750 = "Image text";
        a751 = "Stack mode display";
        a752 = "90\272 clockwise rotation";
        a753 = "90\272 anticlockwise rotation";
        a754 = "Flip the image vertically";
        a755 = "Flip the image horizontally";
        a756 = "Print the displayed images";
        a757 = "Save the selected image";
        a758 = "New window display";
        a759 = "New zoomed display";
        a760 = "Reload selected image";
        a761 = "Zoom factor 1";
        a762 = "Additional tools";
        a763 = "Distance measurement";
        a764 = "Gray value under pointer";
        a765 = "Angle measurement";
        a766 = "Text annotation";
        a767 = "Arrow annotation";
        a768 = "Circle annotation";
        a769 = "Rectangle annotation";
        a770 = "Delete all annotations";
        a771 = "Invert image (negative)";
        a772 = "Presentation State";
        a422 = url;
        a692 = viewer;
        a271 = as;
        a721 = as1;
        a963 a963_1 = new a963(viewer.getDisplay());
        a773 = new Panel();
        a774 = new Panel();
        setBackground(Color.lightGray);
        setLayout(new BorderLayout());
        a774.setLayout(new FlowLayout());
        if(flag)
            a774.add(new a906(url, "ICONS/logo.gif"));
        else
            a774.add(new a906());
        a773.setLayout(new GridLayout(15, 1));
        Panel panel = new Panel();
        panel.setLayout(new FlowLayout());
        a693 = new Label("Please Wait", 1);
        panel.add(a693);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a695 = new a470(url, "ICONS/back.gif", " < ");
        a695.a287(a963_1);
        panel.add(a695);
        a694 = new a470(url, "ICONS/ford.gif", " > ");
        a694.a287(a963_1);
        panel.add(a694);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a698 = new a470(url, "ICONS/first.gif", "|<");
        a698.a287(a963_1);
        panel.add(a698);
        a697 = new a470(url, "ICONS/back5.gif", "<<");
        a697.a287(a963_1);
        panel.add(a697);
        a696 = new a470(url, "ICONS/ford5.gif", ">>");
        a696.a287(a963_1);
        panel.add(a696);
        a699 = new a470(url, "ICONS/last.gif", ">|");
        a699.a287(a963_1);
        panel.add(a699);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a700 = new a470(url, "ICONS/bild1.gif", "1");
        a700.a287(a963_1);
        panel.add(a700);
        a701 = new a470(url, "ICONS/bild2.gif", "4");
        a701.a287(a963_1);
        panel.add(a701);
        a702 = new a470(url, "ICONS/bild3.gif", "9");
        a702.a287(a963_1);
        panel.add(a702);
        a703 = new a470(url, "ICONS/bild4.gif", "16");
        a703.a287(a963_1);
        panel.add(a703);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a704 = new a516();
        a704.a475(140);
        panel.add(a704);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a706 = new a470(url, "ICONS/bfit.gif", "bestFit", false);
        a706.a287(a963_1);
        panel.add(a706);
        a195 = new a470(url, "ICONS/zoomi.gif", "zoomInc");
        a195.a287(a963_1);
        panel.add(a195);
        a196 = new a470(url, "ICONS/zoomd.gif", "zoomDec");
        a196.a287(a963_1);
        panel.add(a196);
        a197 = new a470(url, "ICONS/121.gif", "zoomOne");
        a197.a287(a963_1);
        panel.add(a197);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a707 = new a470(url, "ICONS/hand.gif", "scroll", false);
        a707.a287(a963_1);
        panel.add(a707);
        a708 = new a470(url, "ICONS/info.gif", "info", false);
        a708.a476(true);
        a708.a287(a963_1);
        panel.add(a708);
        a709 = new a470(url, "ICONS/refresh.gif", "reload");
        a709.a287(a963_1);
        panel.add(a709);
        a190 = new a470(url, "ICONS/stack.gif", "stack", false);
        a190.a287(a963_1);
        panel.add(a190);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a710 = new a470(url, "ICONS/rot90r.gif", "rot90r");
        a710.a287(a963_1);
        panel.add(a710);
        a711 = new a470(url, "ICONS/rot90l.gif", "rot90l");
        a711.a287(a963_1);
        panel.add(a711);
        a712 = new a470(url, "ICONS/flip.gif", "flip");
        a712.a287(a963_1);
        panel.add(a712);
        a713 = new a470(url, "ICONS/mirror.gif", "mirror");
        a713.a287(a963_1);
        panel.add(a713);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a714 = new a470(url, "ICONS/tools.gif", "additionalTools", false);
        a714.a287(a963_1);
        panel.add(a714);
        a715 = new a470(url, "ICONS/inverse.gif", "invert");
        a715.a287(a963_1);
        panel.add(a715);
        a716 = new a470(url, "ICONS/save.gif", "save");
        a716.a287(a963_1);
        panel.add(a716);
        a717 = new a470(url, "ICONS/print.gif", "print");
        a717.a287(a963_1);
        panel.add(a717);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a362 = new a906("W:");
        a362.a475(70);
        panel.add(a362);
        a720 = new a906("L:");
        a720.a475(70);
        panel.add(a720);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a470 a470_1 = new a470("wOrig");
        a470_1.a541("Original Image WL");
        a470_1.a475(70);
        a470_1.a287(a963_1);
        panel.add(a470_1);
        a470_1 = new a470("wAuto");
        a470_1.a475(70);
        a470_1.a287(a963_1);
        a470_1.a541("Automatic WL");
        panel.add(a470_1);
        a773.add(panel);
        a718 = new a470[6];
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a718[0] = new a470(as1[0][0]);
        a718[0].a541(as1[0][0] + " WL");
        a718[0].a475(70);
        a718[0].a287(a963_1);
        panel.add(a718[0]);
        a718[1] = new a470(as1[0][1]);
        a718[1].a541(as1[0][1] + " WL");
        a718[1].a475(70);
        a718[1].a287(a963_1);
        panel.add(a718[1]);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a718[2] = new a470(as1[0][2]);
        a718[2].a541(as1[0][2] + " WL");
        a718[2].a475(70);
        a718[2].a287(a963_1);
        panel.add(a718[2]);
        a718[3] = new a470(as1[0][3]);
        a718[3].a541(as1[0][3] + " WL");
        a718[3].a475(70);
        a718[3].a287(a963_1);
        panel.add(a718[3]);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a718[4] = new a470(as1[0][4]);
        a718[4].a541(as1[0][4] + " WL");
        a718[4].a475(70);
        a718[4].a287(a963_1);
        panel.add(a718[4]);
        a718[5] = new a470(as1[0][5]);
        a718[5].a541(as1[0][5] + " WL");
        a718[5].a475(70);
        a718[5].a287(a963_1);
        panel.add(a718[5]);
        a773.add(panel);
        panel = new Panel();
        panel.setLayout(new FlowLayout());
        a719 = new a470("Keep Current WL", false);
        panel.add(a719);
        a773.add(panel);
        add(s, a773);
        add("South", a774);
    }

    public Insets getInsets()
    {
        return new Insets(10, 0, 0, 0);
    }

    public void a442(boolean flag)
    {
        a719.a476(flag);
    }

    public boolean a443()
    {
        return a719.a477();
    }

    public void a444(String s, String s1)
    {
        a693.setText(s);
        if(a705 != null)
        {
            a705.a472(s1);
            a773.validate();
        }
    }

    public void a445(int i, int j)
    {
        a362.a472("W: " + i);
        a720.a472("L: " + j);
    }

    public void a410(boolean flag)
    {
        a703.a476(false);
        a691(flag);
        if(flag)
        {
            a695.a541(a732);
            a694.a541(a733);
            a698.a474(a422, "ICONS/empty.gif", "");
            a698.a541(null);
            a697.a541(a734);
            a696.a541(a735);
            a699.a474(a422, "ICONS/empty.gif", "");
            a699.a541(null);
            a700.a474(a422, "ICONS/empty.gif", "");
            a700.a541(null);
            a701.a474(a422, "ICONS/insert.gif", "insertSt");
            a701.a541(a736);
            a702.a474(a422, "ICONS/clear.gif", "clearSt");
            a702.a541(a737);
            a703.a474(a422, "ICONS/sync.gif", "sync", false);
            a703.a541(a738);
            return;
        } else
        {
            a695.a541(a722);
            a694.a541(a723);
            a698.a474(a422, "ICONS/first.gif", "|<");
            a698.a541(a726);
            a697.a541(a724);
            a696.a541(a725);
            a699.a474(a422, "ICONS/last.gif", ">|");
            a699.a541(a727);
            a700.a474(a422, "ICONS/bild1.gif", "1");
            a700.a541(a728);
            a701.a474(a422, "ICONS/bild2.gif", "4");
            a701.a541(a729);
            a702.a474(a422, "ICONS/bild3.gif", "9");
            a702.a541(a730);
            a703.a474(a422, "ICONS/bild4.gif", "16", true);
            a703.a541(a731);
            return;
        }
    }

    public void a689(boolean flag)
    {
        if(flag)
        {
            a700.a474(a422, "ICONS/empty.gif", "");
            a700.a541(null);
            a701.a474(a422, "ICONS/empty.gif", "");
            a701.a541(null);
            a702.a474(a422, "ICONS/empty.gif", "");
            a702.a541(null);
            a703.a474(a422, "ICONS/empty.gif", "");
            a703.a541(null);
            a706.a476(false);
            a706.a474(a422, "ICONS/empty.gif", "", true);
            a706.a541(null);
            return;
        } else
        {
            a700.a474(a422, "ICONS/bild1.gif", "1");
            a700.a541(a728);
            a701.a474(a422, "ICONS/bild2.gif", "4");
            a701.a541(a729);
            a702.a474(a422, "ICONS/bild3.gif", "9");
            a702.a541(a730);
            a703.a474(a422, "ICONS/bild4.gif", "16");
            a703.a541(a731);
            a706.a474(a422, "ICONS/bfit.gif", "bestFit", false);
            a706.a541(a739);
            a706.a476(true);
            return;
        }
    }

    public void a447(boolean flag)
    {
        if(flag)
        {
            a195.a474(a422, "ICONS/empty.gif", "", true);
            a195.a541(null);
            a197.a474(a422, "ICONS/bfit.gif", "blowUp");
            a197.a541(a749);
            a190.a474(a422, "ICONS/empty.gif", "", true);
            a190.a541(null);
        } else
        {
            a195.a474(a422, "ICONS/zoom.gif", "magGlass", false);
            a195.a541(a743);
            a197.a474(a422, "ICONS/empty.gif", "");
            a197.a541(null);
            a190.a474(a422, "ICONS/stack.gif", "stack", false);
            a190.a541(a751);
        }
        a689(flag);
    }

    public void a448(boolean flag)
    {
        if(flag)
        {
            a196.a474(a422, "ICONS/zoomi.gif", "magGlassZoomInc", true);
            a196.a541(a744);
            a197.a474(a422, "ICONS/zoomd.gif", "magGlassZoomDec");
            a197.a541(a745);
            a709.a474(a422, "ICONS/inc.gif", "magGlassSquareInc");
            a709.a541(a746);
            a190.a474(a422, "ICONS/dec.gif", "magGlassSquareDec");
            a190.a541(a747);
        } else
        {
            a196.a474(a422, "ICONS/rectZoom.gif", "rectangularZoom", false);
            a196.a541(a748);
            a197.a474(a422, "ICONS/empty.gif", "");
            a197.a541(null);
            a709.a474(a422, "ICONS/refresh.gif", "reload");
            a709.a541(a760);
            a190.a474(a422, "ICONS/stack.gif", "stack", false);
            a190.a541(a751);
        }
        a689(flag);
    }

    public void a449(String s)
    {
        int i = -1;
        for(int j = 0; j < a271.length; j++)
            if(s.equals(a271[j]))
                i = j;

        if(i == -1)
            i = a271.length - 1;
        for(int k = 0; k < a721[i].length; k++)
        {
            a718[k].a472(a721[i][k]);
            a718[k].a541(a721[i][k] + " WL");
        }

    }

    public void a446(int i)
    {
        if(i == 0)
        {
            a706.a476(false);
            a706.a474(a422, "ICONS/empty.gif", "", true);
            a706.a541(null);
            a195.a476(false);
            a195.a474(a422, "ICONS/empty.gif", "", true);
            a195.a541(null);
            a196.a476(false);
            a196.a474(a422, "ICONS/empty.gif", "", true);
            a196.a541(null);
            a197.a476(false);
            a197.a474(a422, "ICONS/empty.gif", "");
            a197.a541(null);
            a707.a476(false);
            a707.a474(a422, "ICONS/empty.gif", "", true);
            a707.a541(null);
            a714.a476(false);
            a714.a474(a422, "ICONS/empty.gif", "", true);
            a714.a541(null);
            return;
        }
        if(i == 1)
        {
            a706.a474(a422, "ICONS/bfit.gif", "bestFit", false);
            a706.a541(a739);
            a706.a476(true);
            a195.a474(a422, "ICONS/zoom.gif", "magGlass", false);
            a195.a541(a743);
            a196.a474(a422, "ICONS/rectZoom.gif", "rectangularZoom", false);
            a196.a541(a748);
            a197.a476(false);
            a197.a474(a422, "ICONS/empty.gif", "");
            a197.a541(null);
            a707.a476(false);
            a707.a474(a422, "ICONS/empty.gif", "", true);
            a707.a541(null);
            a714.a476(false);
            a714.a474(a422, "ICONS/empty.gif", "", true);
            a714.a541(null);
            return;
        } else
        {
            a706.a474(a422, "ICONS/bfit.gif", "bestFit", false);
            a706.a541(a739);
            a706.a476(false);
            a195.a476(false);
            a195.a474(a422, "ICONS/zoomi.gif", "zoomInc", true);
            a195.a541(a740);
            a196.a476(false);
            a196.a474(a422, "ICONS/zoomd.gif", "zoomDec", true);
            a196.a541(a741);
            a197.a474(a422, "ICONS/121.gif", "zoomOne");
            a197.a541(a742);
            a707.a474(a422, "ICONS/hand.gif", "scroll", false);
            a707.a541(a742);
            a714.a474(a422, "ICONS/tools.gif", "additionalTools", false);
            a714.a541(a762);
            return;
        }
    }

    public void a194(boolean flag)
    {
        a706.a476(flag);
    }

    public void a690()
    {
        a695.a541(a722);
        a694.a541(a723);
        a698.a541(a726);
        a697.a541(a724);
        a696.a541(a725);
        a699.a541(a727);
        a700.a541(a728);
        a701.a541(a729);
        a702.a541(a730);
        a703.a541(a731);
        a706.a541(a739);
        a195.a541(a740);
        a196.a541(a741);
        a197.a541(a761);
        a707.a541(a742);
        a708.a541(a750);
        a709.a541(a760);
        a190.a541(a751);
        a710.a541(a752);
        a711.a541(a753);
        a712.a541(a754);
        a713.a541(a755);
        a714.a541(a762);
        a715.a541(a771);
        a716.a541(a757);
        a717.a541(a756);
    }

    public void a450(Object obj)
    {
        a692.setFrameTitle(obj);
    }

    public void a191(boolean flag)
    {
        if(flag)
        {
            a700.a474(a422, "ICONS/empty.gif", "");
            a700.a541(null);
            a701.a474(a422, "ICONS/empty.gif", "");
            a701.a541(null);
            a702.a474(a422, "ICONS/empty.gif", "");
            a702.a541(null);
            a703.a474(a422, "ICONS/empty.gif", "");
            a703.a541(null);
            a706.a476(false);
            a706.a474(a422, "ICONS/textan.gif", "annotationText", false);
            a706.a541(a766);
            a195.a476(false);
            a195.a474(a422, "ICONS/arrowan.gif", "annotationArrow", false);
            a195.a541(a767);
            a196.a476(false);
            a196.a474(a422, "ICONS/circlean.gif", "annotationCircle", false);
            a196.a541(a768);
            a197.a474(a422, "ICONS/rectan.gif", "annotationRectangle", false);
            a197.a541(a769);
            a707.a476(false);
            a707.a474(a422, "ICONS/torax.gif", "annotationAndMeasurementReset", true);
            a707.a541(a770);
            a709.a474(a422, "ICONS/zoom1.gif", "fullView");
            a709.a541(a758);
            a190.a474(a422, "ICONS/zoom2.gif", "zoom", true);
            a190.a541(a759);
            a710.a474(a422, "ICONS/distance.gif", "distanceMeasurement", false);
            a710.a541(a763);
            a711.a474(a422, "ICONS/grayval.gif", "grayValueMeasurement", false);
            a711.a541(a764);
            a712.a474(a422, "ICONS/angle.gif", "angleMeasurement", false);
            a712.a541(a765);
            a713.a474(a422, "ICONS/empty.gif", "");
            a713.a541(null);
            return;
        } else
        {
            a700.a474(a422, "ICONS/bild1.gif", "1");
            a700.a541(a728);
            a701.a474(a422, "ICONS/bild2.gif", "4");
            a701.a541(a729);
            a702.a474(a422, "ICONS/bild3.gif", "9");
            a702.a541(a730);
            a703.a474(a422, "ICONS/bild4.gif", "16");
            a703.a541(a731);
            a706.a476(false);
            a706.a474(a422, "ICONS/bfit.gif", "bestFit", false);
            a706.a541(a739);
            a195.a476(false);
            a195.a474(a422, "ICONS/zoomi.gif", "zoomInc", true);
            a195.a541(a740);
            a196.a476(false);
            a196.a474(a422, "ICONS/zoomd.gif", "zoomDec", true);
            a196.a541(a741);
            a197.a476(false);
            a197.a474(a422, "ICONS/121.gif", "zoomOne", true);
            a197.a541(a742);
            a707.a474(a422, "ICONS/hand.gif", "scroll", false);
            a707.a541(a742);
            a709.a474(a422, "ICONS/refresh.gif", "reload");
            a709.a541(a760);
            a190.a474(a422, "ICONS/stack.gif", "stack", false);
            a190.a541(a751);
            a710.a476(false);
            a710.a474(a422, "ICONS/rot90r.gif", "rot90r", true);
            a710.a541(a752);
            a711.a476(false);
            a711.a474(a422, "ICONS/rot90l.gif", "rot90l", true);
            a711.a541(a753);
            a712.a476(false);
            a712.a474(a422, "ICONS/flip.gif", "flip", true);
            a712.a541(a754);
            a713.a476(false);
            a713.a474(a422, "ICONS/mirror.gif", "mirror", true);
            a713.a541(a755);
            return;
        }
    }

    public void a192(int i)
    {
        if(i != 1)
            a706.a476(false);
        if(i != 2)
            a195.a476(false);
        if(i != 3)
            a196.a476(false);
        if(i != 4)
            a197.a476(false);
        if(i != 5)
            a710.a476(false);
        if(i != 6)
            a711.a476(false);
        if(i != 7)
            a712.a476(false);
    }

    public void a452(String as[])
    {
        for(int i = 0; i < 4; i++)
        {
            a470 a470_1 = a700;
            switch(i)
            {
            case 0: // '\0'
                a470_1 = a700;
                break;

            case 1: // '\001'
                a470_1 = a701;
                break;

            case 2: // '\002'
                a470_1 = a702;
                break;

            case 3: // '\003'
                a470_1 = a703;
                break;
            }
            String s = "PS" + i;
            String s1 = "ICONS/ps" + i + ".gif";
            if(as[i] != null)
            {
                if(as[i].startsWith(s))
                {
                    a470_1.a474(a422, s1, as[i]);
                    a470_1.a541(a772);
                } else
                {
                    a470_1.a474(a422, "ICONS/empty.gif", "");
                    a470_1.a541(null);
                }
            } else
            {
                a470_1.a474(a422, "ICONS/empty.gif", "");
                a470_1.a541(null);
            }
        }

    }

    public void a451(int i, int j, int k)
    {
        switch(i)
        {
        case 3: // '\003'
            a704.a517(k);
            return;

        case 8: // '\b'
            a704.a518(j);
            return;
        }
    }

    void a691(boolean flag)
    {
        if(flag)
        {
            Component component = a773.getComponent(5);
            a773.remove(component);
            Panel panel = new Panel();
            panel.setLayout(new FlowLayout());
            if(a705 == null)
                a705 = new a906("Wait a minute");
            panel.add(a705);
            a773.add(panel, 5);
            a773.validate();
            return;
        } else
        {
            Component component1 = a773.getComponent(5);
            a773.remove(component1);
            Panel panel1 = new Panel();
            panel1.setLayout(new FlowLayout());
            panel1.add(a706);
            panel1.add(a195);
            panel1.add(a196);
            panel1.add(a197);
            a773.add(panel1, 5);
            a773.validate();
            return;
        }
    }
}
