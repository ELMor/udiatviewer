// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

import a0.a127.a918;
import a0.a167;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.util.Date;

public class Viewer extends Applet
{

    final String a804[] = {
        "CT", "MR", "Default"
    };
    final String a805[][] = {
        {
            "Head", "Head FP", "Tissues", "Lumbar", "Lung", "Bone"
        }, {
            "MR WL1", "MR WL2", "MR WL3", "MR WL4", "MR WL5", "MR WL6"
        }, {
            "WL 1", "WL 2", "WL 3", "WL 4", "WL 5", "WL 6"
        }
    };
    final int a806[][][] = {
        {
            {
                35, 90
            }, {
                35, 120
            }, {
                30, 300
            }, {
                35, 250
            }, {
                -600, 1500
            }, {
                400, 2000
            }
        }, {
            {
                128, 256
            }, {
                256, 512
            }, {
                512, 1024
            }, {
                768, 2048
            }, {
                1536, 4096
            }, {
                3072, 8192
            }
        }, {
            {
                128, 256
            }, {
                256, 512
            }, {
                512, 1024
            }, {
                896, 2048
            }, {
                1792, 4096
            }, {
                3584, 8192
            }
        }
    };
    final int a807 = 10;
    a688 a255;
    a167 a0;
    a616 a808;
    String a809[];
    String a418[];
    String a810[];
    String a424;
    String a271[];
    String a721[][];
    int a269[][][];
    int a272[];
    a918 a426;
    boolean a311;
    boolean a811;
    boolean a194;
    boolean a812;
    String a813[];
    int a814[];
    int a815[];
    String a816[];
    String a817[];
    String a818[];
    String a819[];
    String a820[];
    String a821[];
    String a822[];
    String a823[];
    String a824[];
    boolean a825[];
    String a428;
    String a266;
    String a826[];

    public void setFrameTitle(Object obj)
    {
        if(a808 == null)
            return;
        if(a824[0] == null)
            return;
        a918 a918_1 = (a918)obj;
        String s = a918_1.a648(null, null, a824[0]);
        for(int i = 1; i < 3; i++)
            if(a824[i] != null)
                s = s + " - " + a918_1.a648(null, null, a824[i]);

        a808.setTitle(s);
    }

    public a167 getDisplay()
    {
        return a0;
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void init()
    {
        a794();
        a795(false);
        a798();
        a797();
        a792();
        a799();
        if(a418 != null)
            a793();
    }

    public void start()
    {
        Date date = new Date();
        Date date1 = new Date(0x10fac9b5c00L);
        if(date.after(date1))
        {
            Container container;
            for(container = getParent(); !(container instanceof Frame); container = container.getParent());
            Frame frame = (Frame)container;
            new a682(frame, "This version has expired. Contact UDIAT(labimg@udiat.es) for further information.", "RAIM Java Viewer");
        }
        if(a255 != null)
            a255.a690();
        if(a808 == null)
        {
            validate();
            return;
        }
        a808.validate();
        a808.pack();
        Dimension dimension = getToolkit().getScreenSize();
        if(dimension.height > 700)
            a808.setBounds(0, 0, 900, 700);
        else
        if(dimension.height < 520)
            a808.setBounds(0, 0, 640, 520);
        else
            a808.setBounds(0, 0, 700, 609);
        a808.show();
    }

    public static void main(String args[])
    {
        if(args.length == 2 && args[1].equals("Application"))
        {
            Viewer viewer = new Viewer();
            viewer.init(args[0]);
        }
        System.out.println("This is an applet, not an application!");
    }

    boolean a791(String s)
    {
        if(s.endsWith(".JPG"))
            return true;
        if(s.endsWith(".GIF"))
            return true;
        if(s.endsWith(".jpg"))
            return true;
        if(s.endsWith(".gif"))
            return true;
        if(s.endsWith(".JPEG"))
            return true;
        if(s.endsWith(".jpeg"))
            return true;
        if(s.endsWith(".DCM"))
            return true;
        if(s.endsWith(".dcm"))
            return true;
        if(s.endsWith(".DIC"))
            return true;
        if(s.endsWith(".dic"))
            return true;
        if(s.endsWith(".DICM"))
            return true;
        return s.endsWith(".dicm");
    }

    void init(String s)
    {
        File file = new File(s);
        if(!file.exists())
        {
            System.err.println("File or directory do not exists.");
            System.exit(0);
        }
        if(file.isDirectory())
        {
            String as[] = file.list();
            int i = 0;
            for(int j = 0; j < as.length; j++)
                if(a791(as[j]))
                {
                    as[i] = as[j];
                    i++;
                }

            if(i == 0)
            {
                System.err.println("DICOM files not found. File name should have .DICM extension");
                System.exit(0);
            }
            a418 = new String[i];
            for(int k = 0; k < i; k++)
                a418[k] = s + File.separator + as[k];

        } else
        {
            a418 = new String[1];
            a418[0] = s;
        }
        a424 = "empty";
        a271 = a804;
        a269 = a806;
        a721 = a805;
        a795(true);
        a792();
        a793();
        start();
    }

    public Viewer()
    {
        a311 = true;
        a811 = false;
        a194 = false;
        a812 = false;
        a824 = new String[3];
        setBackground(Color.black);
        setFont(new Font("SansSerif", 0, 14));
    }

    public void executeAction(String s)
    {
        if(a0 != null)
            a0.a171(s);
    }

    public void showTools(boolean flag)
    {
        if(a255 == null)
            return;
        int i = getComponentCount();
        if(flag)
        {
            if(i == 1)
                add("East", a255);
        } else
        if(i == 2)
            remove(a255);
        validate();
    }

    public void numberOfImages(int i)
    {
        a418 = new String[i];
    }

    public void numberOfSeries(int i)
    {
        a810 = new String[i];
    }

    public void addImage(String s, int i)
    {
        if(a418 != null)
            a418[i] = s;
    }

    public void addSerie(String s, int i)
    {
        if(a810 != null)
            a810[i] = s;
    }

    public void initializeApplet()
    {
        stop();
        if(a418 != null)
        {
            a792();
            a793();
        }
        start();
    }

    public void resetApplet()
    {
        if(a311)
        {
            if(a808 != null)
                a808.dispose();
            a808 = null;
        } else
        {
            if(a255 != null)
                remove(a255);
            if(a0 != null)
                remove(a0);
            Graphics g = getGraphics();
            g.setColor(getBackground());
            g.fillRect(0, 0, getSize().width, getSize().height);
            g.setColor(getForeground());
            g.dispose();
        }
        a255 = null;
        a0 = null;
        a418 = null;
        a810 = null;
        System.gc();
    }

    public boolean isBestFit()
    {
        return a194;
    }

    public void setBestFit(boolean flag)
    {
        a194 = flag;
    }

    public boolean isKeepCurrentWL()
    {
        return a811;
    }

    public void setKeepCurrentWL(boolean flag)
    {
        a811 = flag;
    }

    public boolean isFrame()
    {
        return a311;
    }

    public void setFrame(boolean flag)
    {
        a311 = flag;
    }

    public String getMultiframe()
    {
        return a424;
    }

    public void setMultiframe(String s)
    {
        a424 = s;
    }

    public String[] getWLModality()
    {
        return a271;
    }

    public String getWLModality(int i)
    {
        if(a271.length > i)
            return null;
        else
            return a271[i];
    }

    public void setWLModality(String as[])
    {
        a271 = as;
    }

    public void setWLModality(int i, String s)
    {
        if(i < a271.length)
            a271[i] = s;
    }

    public String[] getPresetLabel()
    {
        return a813;
    }

    public String getPresetLabel(int i)
    {
        if(a813.length > i)
            return null;
        else
            return a813[i];
    }

    public void setPresetLabel(String as[])
    {
        a813 = as;
    }

    public void setPresetLabel(int i, String s)
    {
        if(i < a813.length)
            a813[i] = s;
    }

    public int[] getPresetWindow()
    {
        return a814;
    }

    public int getPresetWindow(int i)
    {
        if(a814.length > i)
            return -1;
        else
            return a814[i];
    }

    public void setPresetWindow(int ai[])
    {
        a814 = ai;
    }

    public void setPresetWindow(int i, int j)
    {
        if(i < a814.length)
            a814[i] = j;
    }

    public int[] getPresetLevel()
    {
        return a815;
    }

    public int getPresetLevel(int i)
    {
        if(a815.length > i)
            return -1;
        else
            return a815[i];
    }

    public void setPresetLevel(int ai[])
    {
        a815 = ai;
    }

    public void setPresetLevel(int i, int j)
    {
        if(i < a815.length)
            a815[i] = j;
    }

    public String[] getTopLeft()
    {
        return a816;
    }

    public String getTopLeft(int i)
    {
        if(a816 == null)
            return null;
        if(a816.length > i)
            return null;
        else
            return a816[i];
    }

    public void setTopLeft(String as[])
    {
        a816 = as;
    }

    public void setTopLeft(int i, String s)
    {
        if(a816 == null)
            a816 = new String[10];
        if(i < a816.length)
            a816[i] = s;
    }

    public String[] getTopRight()
    {
        return a817;
    }

    public String getTopRight(int i)
    {
        if(a817 == null)
            return null;
        if(a817.length > i)
            return null;
        else
            return a817[i];
    }

    public void setTopRight(String as[])
    {
        a817 = as;
    }

    public void setTopRight(int i, String s)
    {
        if(a817 == null)
            a817 = new String[10];
        if(i < a817.length)
            a817[i] = s;
    }

    public String[] getBottomLeft()
    {
        return a818;
    }

    public String getBottomLeft(int i)
    {
        if(a818 == null)
            return null;
        if(a818.length > i)
            return null;
        else
            return a818[i];
    }

    public void setBottomLeft(String as[])
    {
        a818 = as;
    }

    public void setBottomLeft(int i, String s)
    {
        if(a818 == null)
            a818 = new String[10];
        if(i < a818.length)
            a818[i] = s;
    }

    public String[] getBottomRight()
    {
        return a819;
    }

    public String getBottomRight(int i)
    {
        if(a819 == null)
            return null;
        if(a819.length > i)
            return null;
        else
            return a819[i];
    }

    public void setBottomRight(String as[])
    {
        a819 = as;
    }

    public void setBottomRight(int i, String s)
    {
        if(a819 == null)
            a819 = new String[10];
        if(i < a819.length)
            a819[i] = s;
    }

    public String[] getTopLeftText()
    {
        return a820;
    }

    public String getTopLeftText(int i)
    {
        if(a820 == null)
            return null;
        if(a820.length > i)
            return null;
        else
            return a820[i];
    }

    public void setTopLeftText(String as[])
    {
        a820 = as;
    }

    public void setTopLeftText(int i, String s)
    {
        if(a820 == null)
            a820 = new String[10];
        if(i < a820.length)
            a820[i] = s;
    }

    public String[] getTopRightText()
    {
        return a821;
    }

    public String getTopRightText(int i)
    {
        if(a821 == null)
            return null;
        if(a821.length > i)
            return null;
        else
            return a821[i];
    }

    public void setTopRightText(String as[])
    {
        a821 = a821;
    }

    public void setTopRightText(int i, String s)
    {
        if(a821 == null)
            a821 = new String[10];
        if(i < a821.length)
            a821[i] = s;
    }

    public String[] getBottomLeftText()
    {
        return a822;
    }

    public String getBottomLeftText(int i)
    {
        if(a822 == null)
            return null;
        if(a822.length > i)
            return null;
        else
            return a822[i];
    }

    public void setBottomLeftText(String as[])
    {
        a822 = a822;
    }

    public void setBottomLeftText(int i, String s)
    {
        if(a822 == null)
            a822 = new String[10];
        if(i < a822.length)
            a822[i] = s;
    }

    public String[] getBottomRightText()
    {
        return a823;
    }

    public String getBottomRightText(int i)
    {
        if(a823 == null)
            return null;
        if(a823.length > i)
            return null;
        else
            return a823[i];
    }

    public void setBottomRightText(String as[])
    {
        a823 = as;
    }

    public void setBottomRightText(int i, String s)
    {
        if(a823 == null)
            a823 = new String[10];
        if(i < a823.length)
            a823[i] = s;
    }

    public String[] getTip()
    {
        return a826;
    }

    public String getTip(int i)
    {
        if(a826.length > i)
            return null;
        else
            return a826[i];
    }

    public void setTip(String as[])
    {
        a826 = as;
    }

    public void setTip(int i, String s)
    {
        if(i < a826.length)
            a826[i] = s;
    }

    public void setTextColor(String s)
    {
        a266 = s;
    }

    public String getTextColor()
    {
        return a266;
    }

    public boolean[] getDisabledImages()
    {
        return a825;
    }

    public boolean getDisabledImages(int i)
    {
        if(a825 == null)
            return false;
        if(a825.length > i)
            return false;
        else
            return a825[i];
    }

    public void setDisabledImages(int i, boolean flag)
    {
        if(a418 == null)
            return;
        if(a825 == null)
        {
            int j = a418.length;
            a825 = new boolean[j];
            for(int k = 0; k < j; k++)
                a825[k] = false;

        }
        if(i < a825.length)
            a825[i] = flag;
    }

    public void setDisabledImages(boolean aflag[])
    {
        a825 = aflag;
    }

    void a792()
    {
        if(a418 == null)
            return;
        Dimension dimension = getToolkit().getScreenSize();
        URL url = null;
        try
        {
            url = getCodeBase();
        }
        catch(NullPointerException _ex) { }
        if(url != null)
        {
            String s = url.toString();
            if(s.indexOf('+') != -1)
                url = null;
        }
        a800();
        a801();
        a802();
        a803();
        if(a0 == null)
            a0 = new a167();
        if(a311)
        {
            if(a255 == null)
                if(dimension.height > 590)
                    a255 = new a688(url, "North", true, this, a271, a721);
                else
                    a255 = new a688(url, "East", false, this, a271, a721);
        } else
        if(a255 == null)
            if(getSize().height > 550)
                a255 = new a688(url, "North", true, this, a271, a721);
            else
            if(getSize().height < 500)
                a255 = new a688(url, "East", false, this, a271, a721);
            else
                a255 = new a688(url, "East", true, this, a271, a721);
        a0.a169(url, a418, a810, a424, a426);
        a0.a170(a428, a825, a266, a809);
    }

    void a793()
    {
        if(a311)
        {
            a808 = new a616();
            a808.setTitle("RAIM Java (DICOM Java Viewer)");
            a808.setLayout(new BorderLayout());
            if(a255 != null)
                a808.add("East", a255);
            if(a0 != null)
                a808.add("Center", a0);
        } else
        {
            setBackground(Color.black);
            setFont(new Font("SansSerif", 0, 14));
            setLayout(new BorderLayout());
            if(a255 != null)
                add("East", a255);
            if(a255 != null)
                add("Center", a0);
        }
        if(a255 != null)
            a0.a168(a255, a811, a194, a269, a721, a271, a272);
    }

    void a794()
    {
        int i = 0;
        a418 = null;
        String s = getParameter("nrSlices");
        if(s != null)
        {
            i = Integer.parseInt(s);
            a418 = new String[i];
            for(int j = 0; j < i; j++)
                a418[j] = getParameter("image" + j);

        }
        boolean flag = false;
        for(int k = 0; k < i; k++)
        {
            s = getParameter("disable" + k);
            if(s != null)
                flag = true;
        }

        if(flag)
        {
            a825 = new boolean[i];
            for(int l = 0; l < i; l++)
            {
                s = getParameter("disable" + l);
                if(s == null)
                    a825[l] = false;
                else
                if(s.equals("true"))
                    a825[l] = true;
                else
                    a825[l] = false;
            }

        }
        s = getParameter("nrSeries");
        a810 = null;
        if(s != null)
        {
            int i1 = Integer.parseInt(s, 10);
            a810 = new String[i1];
            for(int k1 = 0; k1 < i1; k1++)
                a810[k1] = getParameter("serie" + k1);

        }
        s = getParameter("bestFit");
        if(s == null)
            a194 = false;
        else
        if(s.equals("true"))
            a194 = true;
        s = getParameter("keepCurrentWL");
        if(s == null)
            a811 = false;
        else
        if(s.equals("true"))
            a811 = true;
        a424 = getParameter("multiframe");
        if(a424 == null)
            a424 = "empty";
        s = getParameter("frame");
        if(s == null)
            a311 = true;
        else
        if(s.equals("false"))
            a311 = false;
        a428 = getParameter("emptyImage");
        a266 = getParameter("textColor");
        int j1 = 0;
        do
        {
            String s1 = getParameter("wlModality" + j1);
            if(s1 == null)
                break;
            j1++;
        } while(true);
        if(j1 < 3)
            j1 = 3;
        a271 = new String[j1];
        for(int l1 = 0; l1 < j1; l1++)
        {
            String s2 = getParameter("wlModality" + l1);
            if(s2 == null)
                a271[l1] = a804[l1];
            else
                a271[l1] = s2;
        }

        a269 = new int[j1][6][2];
        a721 = new String[j1][6];
        a272 = new int[j1];
        for(int i2 = 0; i2 < j1; i2++)
        {
            for(int j2 = 0; j2 < 6; j2++)
            {
                String s3 = getParameter("wlLabel" + i2 + "_" + j2);
                if(s3 == null)
                {
                    if(i2 < 3)
                        a721[i2][j2] = a805[i2][j2];
                    else
                        a721[i2][j2] = "";
                } else
                {
                    a721[i2][j2] = s3;
                }
                s3 = getParameter("wlLevel" + i2 + "_" + j2);
                if(s3 == null)
                {
                    if(i2 < 3)
                        a269[i2][j2][0] = a806[i2][j2][0];
                } else
                {
                    a269[i2][j2][0] = Integer.parseInt(s3);
                }
                s3 = getParameter("wlWidth" + i2 + "_" + j2);
                if(s3 == null)
                {
                    if(i2 < 3)
                        a269[i2][j2][1] = a806[i2][j2][1];
                } else
                {
                    a269[i2][j2][1] = Integer.parseInt(s3);
                }
            }

            String s4 = getParameter("wlAuto" + i2);
            if(s4 == null)
                a272[i2] = -1;
            else
            if(s4.equals("NONE"))
                a272[i2] = -1;
            else
            if(s4.equals("AUTO"))
                a272[i2] = 7;
            else
                a272[i2] = Integer.parseInt(s4);
        }

        j1 = 0;
        do
        {
            String s5 = getParameter("ps" + j1);
            if(s5 == null)
                break;
            j1++;
        } while(true);
        if(j1 > 0)
            a809 = new String[j1];
        for(int k2 = 0; k2 < j1; k2++)
            a809[k2] = getParameter("ps" + k2);

    }

    void a795(boolean flag)
    {
        a426 = new a918();
        if(flag)
        {
            a796(a426);
            return;
        }
        int i = 0;
        do
        {
            String s = getParameter("topLeft" + i);
            if(s == null)
                break;
            int k = s.indexOf('-');
            if(k == -1)
                a426.a643(s, null, null, "topLeft" + i);
            else
                a426.a643(s.substring(0, k), s.substring(k + 1), null, "topLeft" + i);
            i++;
        } while(true);
        int j = 0;
        do
        {
            String s1 = getParameter("topRight" + j);
            if(s1 == null)
                break;
            int i1 = s1.indexOf('-');
            if(i1 == -1)
                a426.a643(s1, null, null, "topRight" + j);
            else
                a426.a643(s1.substring(0, i1), s1.substring(i1 + 1), null, "topRight" + j);
            j++;
        } while(true);
        int l = 0;
        do
        {
            String s2 = getParameter("bottomLeft" + l);
            if(s2 == null)
                break;
            int k1 = s2.indexOf('-');
            if(k1 == -1)
                a426.a643(s2, null, null, "bottomLeft" + l);
            else
                a426.a643(s2.substring(0, k1), s2.substring(k1 + 1), null, "bottomLeft" + l);
            l++;
        } while(true);
        int j1 = 0;
        do
        {
            String s3 = getParameter("bottomRight" + j1);
            if(s3 == null)
                break;
            int l1 = s3.indexOf('-');
            if(l1 == -1)
                a426.a643(s3, null, null, "bottomRight" + j1);
            else
                a426.a643(s3.substring(0, l1), s3.substring(l1 + 1), null, "bottomRight" + j1);
            j1++;
        } while(true);
        if(j == 0 && i == 0 && l == 0 && j1 == 0)
        {
            a796(a426);
            return;
        } else
        {
            a426.a919(i, j, l, j1);
            return;
        }
    }

    void a796(a918 a918_1)
    {
        a918_1.a643("00100010", null, "patientName", "topLeft0");
        a918_1.a643("00100020", null, "patientId", "topLeft1");
        a918_1.a643("00100030", null, "patientBirthdate", "topLeft2");
        a918_1.a643("00080080", null, "institutionName", "topRight0");
        a918_1.a643("00080020", null, "studyDate", "bottomLeft0");
        a918_1.a643("00200010", null, "study", "bottomLeft1");
        a918_1.a643("00200013", null, "imageNumber", "bottomRight0");
        a918_1.a643("00080060", null, "modality", "bottomRight1");
        a918_1.a919(3, 1, 2, 2);
    }

    void a797()
    {
        for(int i = 0; i < 3; i++)
        {
            String s = getParameter("frameTitle" + i);
            if(s == null)
                break;
            int j = s.indexOf('-');
            String s1 = null;
            String s2 = null;
            if(j == -1)
            {
                s1 = s;
            } else
            {
                s1 = s.substring(0, j);
                s2 = s.substring(j + 1);
            }
            a824[i] = a426.a925(s1);
            if(a824[i] == null)
            {
                a824[i] = "frameTitle" + i;
                a426.a643(s1, s2, null, a824[i]);
            }
        }

    }

    void a798()
    {
        int i = 0;
        do
        {
            String s = getParameter("topLeftText" + i);
            if(s == null)
                break;
            a426.a643(s, "topLeft" + i);
            i++;
        } while(true);
        int j = 0;
        do
        {
            String s1 = getParameter("topRightText" + j);
            if(s1 == null)
                break;
            a426.a643(s1, "topRight" + j);
            j++;
        } while(true);
        int k = 0;
        do
        {
            String s2 = getParameter("bottomLeftText" + k);
            if(s2 == null)
                break;
            a426.a643(s2, "bottomLeft" + k);
            k++;
        } while(true);
        int l = 0;
        do
        {
            String s3 = getParameter("bottomRightText" + l);
            if(s3 == null)
                break;
            a426.a643(s3, "bottomRight" + l);
            l++;
        } while(true);
        if(j == 0 && i == 0 && k == 0 && l == 0)
            a812 = false;
        else
            a812 = true;
        a426.a920(i, j, k, l);
    }

    void a799()
    {
        String s = getParameter("tipBackward");
        if(s != null)
            a255.a722 = s;
        s = getParameter("tipForward");
        if(s != null)
            a255.a723 = s;
        s = getParameter("tipBackward5");
        if(s != null)
            a255.a724 = s;
        s = getParameter("tipForward5");
        if(s != null)
            a255.a725 = s;
        s = getParameter("tipFirst");
        if(s != null)
            a255.a726 = s;
        s = getParameter("tipLast");
        if(s != null)
            a255.a727 = s;
        s = getParameter("tipOneImage");
        if(s != null)
            a255.a728 = s;
        s = getParameter("tipFourImages");
        if(s != null)
            a255.a729 = s;
        s = getParameter("tipNineImages");
        if(s != null)
            a255.a730 = s;
        s = getParameter("tipSixteenImages");
        if(s != null)
            a255.a731 = s;
        s = getParameter("tipStackBackward");
        if(s != null)
            a255.a732 = s;
        s = getParameter("tipStackForward");
        if(s != null)
            a255.a733 = s;
        s = getParameter("tipStackSeriesBackward");
        if(s != null)
            a255.a734 = s;
        s = getParameter("tipStackSeriesForward");
        if(s != null)
            a255.a736 = s;
        s = getParameter("tipAddSeries");
        if(s != null)
            a255.a736 = s;
        s = getParameter("tipDeleteSeries");
        if(s != null)
            a255.a737 = s;
        s = getParameter("tipSynchronise");
        if(s != null)
            a255.a738 = s;
        s = getParameter("tipfullImage");
        if(s != null)
            a255.a739 = s;
        s = getParameter("tipZoomIn");
        if(s != null)
            a255.a740 = s;
        s = getParameter("tipZoomOut");
        if(s != null)
            a255.a741 = s;
        s = getParameter("tipZoomOut");
        if(s != null)
            a255.a741 = s;
        s = getParameter("tipScrollMode");
        if(s != null)
            a255.a742 = s;
        s = getParameter("tipMagnifyingGlass");
        if(s != null)
            a255.a743 = s;
        s = getParameter("tipIncMagnifyingGlass");
        if(s != null)
            a255.a744 = s;
        s = getParameter("tipDecMagnifyingGlass");
        if(s != null)
            a255.a745 = s;
        s = getParameter("tipIncMagnifyingGlassArea");
        if(s != null)
            a255.a746 = s;
        s = getParameter("tipDecMagnifyingGlassArea");
        if(s != null)
            a255.a747 = s;
        s = getParameter("tipZoomedArea");
        if(s != null)
            a255.a748 = s;
        s = getParameter("tipBlowUp");
        if(s != null)
            a255.a749 = s;
        s = getParameter("tipRot90R");
        if(s != null)
            a255.a752 = s;
        s = getParameter("tipRot90L");
        if(s != null)
            a255.a753 = s;
        s = getParameter("tipFlipV");
        if(s != null)
            a255.a754 = s;
        s = getParameter("tipFlipH");
        if(s != null)
            a255.a755 = s;
        s = getParameter("tipPrint");
        if(s != null)
            a255.a756 = s;
        s = getParameter("tipSave");
        if(s != null)
            a255.a757 = s;
        s = getParameter("tipNew");
        if(s != null)
            a255.a758 = s;
        s = getParameter("tipZoom");
        if(s != null)
            a255.a759 = s;
        s = getParameter("tipRefresh");
        if(s != null)
            a255.a760 = s;
        s = getParameter("tip121");
        if(s != null)
            a255.a761 = s;
        s = getParameter("tipAdTools");
        if(s != null)
            a255.a762 = s;
        s = getParameter("tipDistance");
        if(s != null)
            a255.a763 = s;
        s = getParameter("tipGrayvalue");
        if(s != null)
            a255.a764 = s;
        s = getParameter("tipAngle");
        if(s != null)
            a255.a765 = s;
        s = getParameter("tipTextAnnotation");
        if(s != null)
            a255.a766 = s;
        s = getParameter("tipArrowAnnotation");
        if(s != null)
            a255.a767 = s;
        s = getParameter("tipCircleAnnotation");
        if(s != null)
            a255.a768 = s;
        s = getParameter("tipRectangleAnnotation");
        if(s != null)
            a255.a769 = s;
        s = getParameter("tipDeleteAnnotation");
        if(s != null)
            a255.a770 = s;
        s = getParameter("tipInversion");
        if(s != null)
            a255.a771 = s;
        s = getParameter("tipPS");
        if(s != null)
            a255.a772 = s;
    }

    void a800()
    {
    }

    void a801()
    {
    }

    void a802()
    {
        if(a812)
            return;
        int i;
        for(i = 0; i < 10; i++)
        {
            if(a820 == null || a820[i] == null)
                break;
            a426.a643(a820[i], "topLeft" + i);
        }

        int j;
        for(j = 0; j < 10; j++)
        {
            if(a821 == null || a821[j] == null)
                break;
            a426.a643(a821[j], "topRight" + j);
        }

        int k;
        for(k = 0; k < 10; k++)
        {
            if(a822 == null || a822[k] == null)
                break;
            a426.a643(a822[k], "bottomLeft" + k);
        }

        int l;
        for(l = 0; l < 10; l++)
        {
            if(a823 == null || a823[l] == null)
                break;
            a426.a643(a823[l], "bottomRight" + l);
        }

        a426.a920(i, j, k, l);
    }

    void a803()
    {
    }
}
