// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import a0.a125.a298;
import a0.a125.a346;
import a0.a125.a358;
import a0.a125.a596;
import a0.a125.a599;
import a0.a125.a787;
import a0.a125.a961;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Vector;

// Referenced classes of package a0:
//            a167

public class a104
{

    public static final int a116 = -1;
    public static final int a117 = 1;
    public static final int a118 = 2;
    public static final int a119 = 3;
    public static final int a120 = 4;
    public static final int a121 = 5;
    public static final int a122 = 6;
    public static final int a123 = 7;
    int a124;
    Vector a125;
    a599 a126;

    public boolean a105()
    {
        return a124 != -1;
    }

    public void a106(int i)
    {
        if(a124 == i)
        {
            a124 = -1;
            return;
        } else
        {
            a124 = i;
            return;
        }
    }

    public void a107(Graphics g)
    {
        if(a125 == null)
            return;
        for(int i = 0; i < a125.size(); i++)
        {
            a599 a599_1 = (a599)a125.elementAt(i);
            a599_1.a107(g, true);
        }

        if(a124 == 6 && a126 != null)
            a126.a107(g, true);
    }

    public void a108(int i, int j, a167 a167)
    {
        if(a124 == -1)
            return;
        a167.setCursor(new Cursor(13));
        switch(a124)
        {
        case 1: // '\001'
            a126 = new a961(a167);
            break;

        case 2: // '\002'
            a126 = new a358(a167);
            break;

        case 3: // '\003'
            a126 = new a787(a167, true);
            break;

        case 4: // '\004'
            a126 = new a787(a167, false);
            break;

        case 5: // '\005'
            a126 = new a298(a167);
            break;

        case 6: // '\006'
            a126 = new a596(a167);
            break;

        case 7: // '\007'
            a126 = new a346(a167);
            break;
        }
        a126.a299(i, j);
        if((a126 instanceof a358) || (a126 instanceof a596))
        {
            Graphics g = a167.getGraphics();
            a126.a300(i, j);
            a126.a107(g, false);
            g.dispose();
        }
    }

    public void a109(int i, int j, Component component)
    {
        if(a124 == -1)
            return;
        if(a125 == null)
            a125 = new Vector();
        Graphics g = component.getGraphics();
        a126.a300(i, j);
        a126.a107(g, false);
        g.dispose();
    }

    public void a110(int i, int j, Component component)
    {
        if(a124 == -1)
            return;
        switch(a124)
        {
        case 6: // '\006'
            a126 = null;
            Graphics g = component.getGraphics();
            Dimension dimension = component.getSize();
            g.setColor(component.getBackground());
            g.fillRect(0, 0, dimension.width, dimension.height);
            g.setColor(component.getForeground());
            g.dispose();
            break;

        case 7: // '\007'
            a346 a346_1 = (a346)a126;
            a346_1.a347(a125);
            // fall through

        default:
            if(a125 == null)
                a125 = new Vector();
            a126.a300(i, j);
            a125.addElement(a126);
            break;
        }
        component.repaint();
    }

    public void a111(Component component)
    {
        a126 = null;
        if(a125 == null)
        {
            return;
        } else
        {
            a125.removeAllElements();
            a124 = -1;
            Graphics g = component.getGraphics();
            Dimension dimension = component.getSize();
            g.setColor(component.getBackground());
            g.fillRect(0, 0, dimension.width, dimension.height);
            g.setColor(component.getForeground());
            g.dispose();
            component.repaint();
            return;
        }
    }

    public void a112()
    {
        if(a125 == null)
            return;
        for(int i = 0; i < a125.size(); i++)
        {
            a599 a599_1 = (a599)a125.elementAt(i);
            a599_1.a112();
        }

    }

    public void a113()
    {
        if(a125 == null)
            return;
        for(int i = 0; i < a125.size(); i++)
        {
            a599 a599_1 = (a599)a125.elementAt(i);
            a599_1.a113();
        }

    }

    public void a114()
    {
        if(a125 == null)
            return;
        for(int i = 0; i < a125.size(); i++)
        {
            a599 a599_1 = (a599)a125.elementAt(i);
            a599_1.a114();
        }

    }

    public void a115()
    {
        if(a125 == null)
            return;
        for(int i = 0; i < a125.size(); i++)
        {
            a599 a599_1 = (a599)a125.elementAt(i);
            a599_1.a115();
        }

    }

    public a104()
    {
        a124 = -1;
    }
}
