// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import a0.a1.a617;
import a0.a127.a918;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.PrintGraphics;
import java.awt.PrintJob;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;

// Referenced classes of package a0:
//            a104, a490, a397, a510, 
//            a932, a674, a441, a775

public class a167 extends Component
{

    static final int a227 = 8;
    static final int a228 = 5;
    int a229[][];
    int a230[];
    int a231[];
    int a232[];
    int a233[];
    a397 a234;
    Image a235[];
    Image a236;
    a490 a225;
    a674 a226;
    a104 a237[];
    int a238;
    int a239;
    int a240;
    int a241;
    int a242;
    int a243;
    int a244;
    int a245;
    int a246;
    int a247;
    int a248;
    int a249;
    int a250;
    int a251;
    int a252;
    int a253;
    int a254;
    a441 a255;
    a775 a256;
    boolean a257;
    boolean a258;
    boolean a194;
    boolean a259;
    boolean a260;
    boolean a261;
    boolean a262;
    boolean a263;
    float a264;
    float a265;
    Color a266;
    int a267;
    int a268;
    int a269[][][];
    String a270[][];
    String a271[];
    int a272[];
    String a273;
    String a274;
    String a275;
    a932 a276;
    int a277;
    int a278;
    int a279;
    int a280;
    int a281;

    public void a168(a441 a441_1, boolean flag, boolean flag1, int ai[][][], String as[][], String as1[], int ai1[])
    {
        a255 = a441_1;
        a269 = ai;
        a270 = as;
        a271 = as1;
        if(ai1 == null)
        {
            int i = a271.length;
            a272 = new int[i];
            for(int j = 0; j < i; j++)
                a272[j] = -1;

        } else
        {
            a272 = ai1;
        }
        if(flag)
        {
            a441_1.a442(true);
            a267 = a234.a406(a241);
            a268 = a234.a405(a241);
        }
        a194 = flag1;
    }

    public a167()
    {
        a249 = 1;
        a250 = 1;
        a251 = 1;
        a252 = 1;
        a257 = true;
        a258 = true;
        a194 = false;
        a259 = false;
        a260 = false;
        a261 = false;
        a262 = false;
        a263 = false;
        a264 = 1.0F;
        a265 = -1F;
        a273 = "CT";
        a274 = "CT";
        enableEvents(16L);
        enableEvents(32L);
        enableEvents(1L);
        setBackground(Color.black);
        setFont(new Font("SansSerif", 0, 14));
    }

    public void a169(URL url, String as[], String as1[], String s, a918 a918_1)
    {
        a240 = as.length;
        int i = a240 >= 32 ? 32 : a240;
        a234 = new a397(as, url, this, a918_1, i);
        if(as1 != null)
        {
            a239 = as1.length;
            a229 = new int[a239][];
            int j = 0;
            int k = 0;
            for(; j < a239; j++)
            {
                int l = Integer.parseInt(as1[j], 10);
                a229[j] = new int[l];
                for(int j1 = 0; j1 < l;)
                {
                    a229[j][j1] = k;
                    j1++;
                    k++;
                }

            }

        }
        boolean flag;
        boolean flag1;
        if(s.equals("empty"))
        {
            flag = false;
            flag1 = true;
        } else
        {
            flag = true;
            if(s.equals("true"))
                flag1 = true;
            else
                flag1 = false;
        }
        if(flag1)
            if(flag)
            {
                if(a229 == null)
                {
                    a234.a143(true);
                    a229 = a234.a413();
                } else
                {
                    a234.a412(a229);
                }
                a239 = a240;
                a240 = a234.a411();
            } else
            if(a234.a143(false))
            {
                a239 = a240;
                a229 = a234.a413();
                a240 = a234.a411();
            }
        a237 = new a104[a240];
        for(int i1 = 0; i1 < a240; i1++)
            a237[i1] = new a104();

        a238 = a240 >= 16 ? 16 : a240;
        a235 = new Image[a238];
    }

    public void a170(String s, boolean aflag[], String s1, String as[])
    {
        if(as != null)
            a276 = new a932(a234.a398(), as);
        if(a234 == null)
            return;
        if(s1 == null)
            a266 = Color.orange;
        else
        if(s1.length() < 9)
        {
            a266 = Color.orange;
        } else
        {
            int i = Integer.parseInt(s1.substring(0, 3));
            int j = Integer.parseInt(s1.substring(3, 6));
            int k = Integer.parseInt(s1.substring(6, 9));
            a266 = new Color(i, j, k);
        }
        if(s == null)
            return;
        a234.a400(s);
        if(aflag == null)
        {
            return;
        } else
        {
            a234.a399(aflag);
            return;
        }
    }

    public void a171(String s)
    {
        if(s == null)
            return;
        if(a225 != null)
            a225.a111();
        if(a226 != null)
            a226.a111();
        if(s.equals(" > "))
        {
            a203(1);
            return;
        }
        if(s.equals(" < "))
        {
            a204(1);
            return;
        }
        if(s.equals(">>"))
        {
            a203(5);
            return;
        }
        if(s.equals("<<"))
        {
            a204(5);
            return;
        }
        if(s.equals(">|"))
        {
            a202(a240 - 1);
            return;
        }
        if(s.equals("|<"))
        {
            a202(0);
            return;
        }
        if(s.equals("1"))
        {
            a215(1);
            return;
        }
        if(s.equals("4"))
        {
            a215(2);
            return;
        }
        if(s.equals("9"))
        {
            a215(3);
            return;
        }
        if(s.equals("16"))
        {
            a215(4);
            return;
        }
        if(s.equals("insertSt"))
        {
            a177();
            return;
        }
        if(s.equals("clearSt"))
        {
            a178();
            return;
        }
        if(s.equals("sync"))
        {
            a179();
            return;
        }
        if(s.equals("bestFit"))
        {
            a194();
            return;
        }
        if(s.equals("zoomInc"))
        {
            a195();
            return;
        }
        if(s.equals("zoomDec"))
        {
            a196();
            return;
        }
        if(s.equals("zoomOne"))
        {
            a197();
            return;
        }
        if(s.equals("scroll"))
        {
            a189();
            return;
        }
        if(s.equals("info"))
        {
            a223();
            return;
        }
        if(s.equals("reload"))
        {
            a198();
            return;
        }
        if(s.equals("stack"))
        {
            a190();
            return;
        }
        if(s.equals("mirror"))
        {
            a113();
            return;
        }
        if(s.equals("flip"))
        {
            a112();
            return;
        }
        if(s.equals("rot90l"))
        {
            a115();
            return;
        }
        if(s.equals("rot90r"))
        {
            a114();
            return;
        }
        if(s.equals("additionalTools"))
        {
            a191();
            return;
        }
        if(s.equals("annotationText"))
        {
            a192(1);
            return;
        }
        if(s.equals("annotationArrow"))
        {
            a192(2);
            return;
        }
        if(s.equals("annotationCircle"))
        {
            a192(3);
            return;
        }
        if(s.equals("annotationRectangle"))
        {
            a192(4);
            return;
        }
        if(s.equals("distanceMeasurement"))
        {
            a192(5);
            return;
        }
        if(s.equals("grayValueMeasurement"))
        {
            a192(6);
            return;
        }
        if(s.equals("angleMeasurement"))
        {
            a192(7);
            return;
        }
        if(s.equals("annotationAndMeasurementReset"))
        {
            a192(-1);
            return;
        }
        if(s.equals("fullView"))
        {
            a220(1.0F);
            return;
        }
        if(s.equals("zoom"))
        {
            a220(1.5F);
            return;
        }
        if(s.equals("invert"))
        {
            a135();
            return;
        }
        if(s.equals("save"))
        {
            a224();
            return;
        }
        if(s.equals("print"))
        {
            print();
            return;
        }
        if(s.equals("magGlass"))
        {
            a225();
            return;
        }
        if(s.equals("magGlassZoomInc"))
        {
            a225.a220(true);
            return;
        }
        if(s.equals("magGlassZoomDec"))
        {
            a225.a220(false);
            return;
        }
        if(s.equals("magGlassSquareInc"))
        {
            a225.a491(true);
            return;
        }
        if(s.equals("magGlassSquareDec"))
        {
            a225.a491(false);
            return;
        }
        if(s.equals("rectangularZoom"))
        {
            a226();
            return;
        }
        if(s.equals("blowUp"))
        {
            a226.a220();
            return;
        }
        if(s.equals("wOrig"))
        {
            a212();
            return;
        }
        if(s.equals("wAuto"))
        {
            a213();
            return;
        }
        if(s.startsWith("PS"))
        {
            a276.a171(s, this);
            return;
        }
        if(s.equals(""))
            return;
        int i = -1;
        for(int j = 0; j < a271.length; j++)
            if(a274.equals(a271[j]))
                i = j;

        if(i == -1)
            i = a271.length - 1;
        for(int k = 0; k < a270[i].length; k++)
            if(s.equals(a270[i][k]))
                a214(i, k);

    }

    public void paint(Graphics g)
    {
        Cursor cursor = getCursor();
        if(!a259)
            setCursor(new Cursor(3));
        if(a261)
        {
            for(int i = 0; i < a249; i++)
                a176(g, i);

        } else
        if(a225 == null)
        {
            a175(g);
        } else
        {
            a175(a225.a494());
            a225.a495(g);
        }
        if(!a259)
        {
            if(a249 == 1 && a226 == null)
                a237[a241].a107(g);
            setCursor(cursor);
        }
    }

    public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1)
    {
        if(image == null)
        {
            if(a255 != null)
                a255.a451(i, k, i1);
            return false;
        } else
        {
            return super.imageUpdate(image, i, j, k, l, i1);
        }
    }

    void a172(int i)
    {
        if(a234.a407(i))
        {
            a918 a918_1 = a234.a152(i);
            String s = a918_1.a648(null, "modality", null);
            int j = -1;
            int k = a271.length;
            for(int l = 0; l < k; l++)
                if(s.equals(a271[l]))
                    j = l;

            if(j == -1)
                return;
            if(a272[j] < 0)
                return;
            int i1;
            int j1;
            if(a272[j] > 5)
            {
                i1 = a234.a138(i);
                j1 = a234.a139(i);
            } else
            {
                int k1 = a272[j];
                i1 = a269[j][k1][0];
                j1 = a269[j][k1][1];
            }
            a234.a137(i1, j1, i);
        }
    }

    void a173(int i, int j)
    {
        a172(j);
        int k = a234.a406(j);
        int l = a234.a405(j);
        if(a232[i] == -1 && a233[i] == -1)
        {
            a232[i] = k;
            a233[i] = l;
        }
        if(a255.a443() && !a259)
        {
            if(k != a232[i] || l != a233[i])
            {
                a234.a137(a232[i], a233[i], j);
                a235[i] = null;
            }
            return;
        } else
        {
            a232[i] = k;
            a233[i] = l;
            return;
        }
    }

    void a174(Graphics g, int i, int j)
    {
        Dimension dimension = getSize();
        int k = a234.a150(i);
        int l = a234.a151(i);
        int i1 = dimension.width - 2;
        int j1 = dimension.height - 2;
        if(i1 > j1)
        {
            if(l > j1)
            {
                a248 = j1;
                a247 = k * j1;
                a247 /= l;
                l = a248;
                k = a247;
            } else
            {
                a248 = l;
            }
            if(k > i1)
            {
                a247 = i1;
                a248 = l * i1;
                a248 /= k;
            } else
            {
                a247 = k;
            }
        } else
        {
            if(k > i1)
            {
                a247 = i1;
                a248 = l * i1;
                a248 /= k;
                l = a248;
                k = a247;
            } else
            {
                a247 = k;
            }
            if(l > j1)
            {
                a248 = j1;
                a247 = k * j1;
                a247 /= l;
            } else
            {
                a248 = l;
            }
        }
        a243 = (i1 - a247) / 2;
        a244 = (j1 - a248) / 2;
        a243++;
        a244++;
        g.drawImage(a235[j], a243, a244, a247, a248, Color.black, this);
        g.setColor(Color.red);
        g.drawRect(a243 - 1, a244 - 1, a247 + 1, a248 + 1);
        a245 = a243;
        a246 = a244;
        a265 = (float)a247 / (float)k;
        int k1 = getToolkit().getScreenResolution();
        a265 *= (float)a254 / (float)k1;
        a265 *= 1.2D;
    }

    void a175(Graphics g)
    {
        if(a258)
        {
            a193();
            if(a264 < 0.1F)
                return;
            a258 = false;
            if(a194)
            {
                a255.a194(a194);
                a194 = false;
                a194();
                return;
            } else
            {
                a199(true);
                return;
            }
        }
        int i = a241 - a242;
        if(i < 0)
            i = a240 + i;
        int j = a249 >= a240 ? a240 : a249;
        for(int k = 0; k < j; k++)
        {
            a172(i);
            int l = a234.a406(i);
            int i1 = a234.a405(i);
            if(a255.a443() && !a259)
            {
                if(a249 == 1)
                {
                    if(l != a267 || i1 != a268)
                    {
                        a234.a137(a267, a268, i);
                        a235[k] = null;
                    }
                } else
                if(l != a267 || i1 != a268 || a234.a424)
                {
                    a234.a137(a267, a268, i);
                    a235[k] = null;
                }
            } else
            if(k == a242 && (l != a267 || i1 != a268))
            {
                a267 = l;
                a268 = i1;
            }
            if(a235[k] == null)
                a235[k] = a234.a136(i);
            if(a249 == 1)
            {
                if(g instanceof PrintGraphics)
                {
                    a174(g, i, k);
                } else
                {
                    a265 = -1F;
                    a186(i);
                    a187(k);
                    if(a236 == null)
                        if(a234.a408(i))
                            a236 = a222(a235[k]);
                        else
                            a236 = a221(a235[k]);
                    if(a226 == null)
                    {
                        g.drawImage(a236, a245, a246, Color.black, this);
                    } else
                    {
                        a226.a199(g);
                        if(a226.a675())
                        {
                            a226.a107(g, a236, a243, a244, a247, a248);
                        } else
                        {
                            g.drawImage(a236, a245, a246, Color.black, this);
                            a226.a107(g, null, a243, a244, a247, a248);
                        }
                    }
                    if(a225 != null)
                        a225.a107(g, a236, a243, a244, a247, a248);
                }
            } else
            {
                a186(i);
                a187(k);
                g.drawImage(a235[k], a243, a244, a247, a248, Color.black, this);
                if(k == a242)
                    a181(g, true);
                else
                if(g instanceof PrintGraphics)
                    a181(g, true);
            }
            if(a257 && !a259)
                a184(i, g);
            i = ++i % a240;
        }

        a183(a241);
        a255.a444((a241 + 1) + " of " + a240, "");
        a255.a445(a234.a405(a241), a234.a406(a241));
        if(!a259)
            a255.a450(a234.a152(a241));
    }

    void a176(Graphics g, int i)
    {
        if(a231[i] == -1)
        {
            if(g instanceof PrintGraphics)
            {
                a247 = a253 / a250;
                a248 = a253 / a250;
                a187(i);
                a181(g, true);
                return;
            }
            if(a242 == i)
            {
                a247 = a253 / a250;
                a248 = a253 / a250;
                a187(a242);
                a181(g, true);
                a255.a444("0 of 0", "");
                a255.a445(0, 0);
            }
            return;
        }
        int j = a229[a231[i]][a230[i]];
        a173(i, j);
        if(a235[i] == null)
            a235[i] = a234.a136(j);
        a186(j);
        a187(i);
        g.drawImage(a235[i], a243, a244, a247, a248, Color.black, this);
        if(g instanceof PrintGraphics)
            a181(g, true);
        else
        if(i == a242)
        {
            a181(g, true);
            a183(j);
            a255.a444((a230[i] + 1) + " of " + a229[a231[i]].length, "Series: " + (a231[a242] + 1) + " of " + a239);
            a255.a445(a234.a405(j), a234.a406(j));
            if(!a259)
                a255.a450(a234.a152(j));
        }
        if(a257 && !a259)
            a184(j, g);
    }

    void a177()
    {
        a203(5);
    }

    void a178()
    {
        if(a231[a242] == -1)
        {
            return;
        } else
        {
            a231[a242] = -1;
            a235[a242] = null;
            a199(false);
            return;
        }
    }

    void a179()
    {
        a262 = !a262;
    }

    void a180()
    {
        if(a261 && a231[a242] == -1)
            return;
        int i = a234.a144(a216());
        if(i < 9)
            a251 = 1;
        if(i > 8 && i < 11)
            a251 = 2;
        if(i > 10 && i < 13)
            a251 = 3;
        if(i > 12)
            a251 = 5;
        a252 = a251;
        if(a234.a142(a216()))
            a251 = -a251;
    }

    void a181(Graphics g, boolean flag)
    {
        int i = a253 / a250;
        int j = a243;
        int k = a244;
        j -= (i - a247) / 2;
        k -= (i - a248) / 2;
        if(flag)
            g.setColor(Color.red);
        else
            g.setColor(Color.black);
        g.drawRect(j - 1, k - 1, i + 1, i + 1);
    }

    void a182(boolean flag)
    {
        if(a225 != null)
            a225.a111();
        if(a226 != null)
            a226.a111();
        Dimension dimension = getSize();
        if(dimension.height < dimension.width)
            a253 = dimension.height - 8;
        else
            a253 = dimension.width - 8;
        if(flag && a255 != null)
            a199(false);
    }

    void a183(int i)
    {
        a918 a918_1 = a234.a152(i);
        a274 = a918_1.a648(null, "modality", null);
        if(!a273.equals(a274))
        {
            a255.a449(a274);
            a273 = a274;
        }
        if(a276 != null)
        {
            a275 = a918_1.a648(null, "SOPInstanceUID", null);
            if(a263)
                a276.a933(a275, a255);
        }
    }

    void a184(int i, Graphics g)
    {
        if(a247 < 96 || a248 < 80)
            return;
        a918 a918_1 = a234.a152(i);
        g.setColor(a266);
        byte byte0;
        if(a247 > 1024 && a248 > 1024)
            byte0 = 13;
        else
        if(a247 > 512 && a248 > 256)
            byte0 = 11;
        else
            byte0 = 9;
        int j = byte0 + 1;
        g.setFont(new Font("SansSerif", 1, byte0));
        int k = 0;
        int l = 0;
        int i1 = byte0;
        int j1 = byte0;
        int k1 = a248 - 3 * j - 5;
        boolean flag = true;
        for(boolean flag1 = true; flag || flag1;)
        {
            String s = null;
            String s2 = null;
            do
            {
                if(k < a918_1.a922())
                {
                    s = a918_1.a648(null, null, "topRight" + k);
                    k++;
                    continue;
                }
                flag1 = false;
                break;
            } while(s.equals(""));
            do
            {
                if(l < a918_1.a921())
                {
                    s2 = a918_1.a648(null, null, "topLeft" + l);
                    l++;
                    continue;
                }
                flag = false;
                break;
            } while(s2.equals(""));
            if(flag1)
            {
                int l1 = a185(g, s) + a185(g, s2) + 5;
                if(l1 < a247 && i1 < k1)
                {
                    int j2 = a247 - a185(g, s);
                    g.drawString(s, a243 + j2, a244 + i1);
                    i1 += j;
                }
            }
            if(flag && j1 < k1)
            {
                g.drawString(s2, a243, a244 + j1);
                j1 += j;
            }
        }

        i1 += j;
        j1 += 3 * j;
        int i2 = a248 - 1;
        int k2 = a248 - 1;
        boolean flag2;
        flag = flag2 = true;
        k = l = 0;
        while(flag || flag2) 
        {
            String s1 = null;
            String s3 = null;
            do
            {
                if(k < a918_1.a924())
                {
                    s1 = a918_1.a648(null, null, "bottomRight" + k);
                    k++;
                    continue;
                }
                flag2 = false;
                break;
            } while(s1.equals(""));
            do
            {
                if(l < a918_1.a923())
                {
                    s3 = a918_1.a648(null, null, "bottomLeft" + l);
                    l++;
                    continue;
                }
                flag = false;
                break;
            } while(s3.equals(""));
            if(flag2)
            {
                int l2 = a185(g, s1) + a185(g, s3) + 5;
                if(l2 < a247 && i1 < i2)
                {
                    int i3 = a247 - a185(g, s1);
                    g.drawString(s1, a243 + i3, a244 + i2);
                    i2 -= j;
                }
            }
            if(flag && j1 < k2)
            {
                g.drawString(s3, a243, a244 + k2);
                k2 -= j;
            }
        }
        if(a249 == 1)
        {
            g.drawString("Zoom: " + a264, a243, a244 + k2);
            k2 -= j;
        }
        g.drawString("L: " + a234.a406(i), a243, a244 + k2);
        k2 -= j;
        g.drawString("W: " + a234.a405(i), a243, a244 + k2);
        if(a918_1.a926())
            g.drawString(a274, (a243 + a247) - a185(g, a274), a244 + i2);
    }

    int a185(Graphics g, String s)
    {
        if(s == null)
        {
            return 0;
        } else
        {
            FontMetrics fontmetrics = g.getFontMetrics();
            return fontmetrics.stringWidth(s);
        }
    }

    void a186(int i)
    {
        int j = a234.a150(i);
        int k = a234.a151(i);
        if(a249 == 1)
        {
            j = (int)(a264 * (float)j);
            k = (int)(a264 * (float)k);
            Dimension dimension = getSize();
            int i1 = dimension.width - 1;
            int j1 = dimension.height - 1;
            if(j < i1)
                a247 = j;
            else
                a247 = i1;
            if(k < j1)
            {
                a248 = k;
                return;
            } else
            {
                a248 = j1;
                return;
            }
        }
        int l = a253 / a250;
        if(j > k)
            if(j < l)
            {
                a247 = j;
                a248 = k;
                return;
            } else
            {
                a247 = l;
                a248 = (k * l) / j;
                return;
            }
        if(k < l)
        {
            a247 = j;
            a248 = k;
            return;
        } else
        {
            a248 = l;
            a247 = (j * l) / k;
            return;
        }
    }

    void a187(int i)
    {
        if(a249 == 1)
        {
            if(a245 < 1)
                a243 = 1;
            else
                a243 = a245;
            if(a246 < 1)
            {
                a244 = 1;
                return;
            } else
            {
                a244 = a246;
                return;
            }
        } else
        {
            Dimension dimension = getSize();
            int j = dimension.width / a250;
            int k = dimension.height / a250;
            a243 = (j - a247) / 2 + j * (i % a250);
            a244 = (k - a248) / 2 + k * (i / a250);
            return;
        }
    }

    void a188(boolean flag)
    {
        boolean flag1 = false;
        int i = a234.a150(a241);
        int j = a234.a151(a241);
        i = (int)(a264 * (float)i);
        j = (int)(a264 * (float)j);
        Dimension dimension = getSize();
        int k = dimension.width;
        int l = dimension.height;
        if(flag)
        {
            a245 = (k - i) / 2;
            a246 = (l - j) / 2;
            a236 = null;
            return;
        }
        if(i > k)
        {
            a245 += a279;
            if(a245 > 0)
                a245 = 0;
            if(a245 < k - i)
                a245 = k - i;
            flag1 = true;
        }
        if(j > l)
        {
            a246 += a280;
            if(a246 > 0)
                a246 = 0;
            if(a246 < l - j)
                a246 = l - j;
            flag1 = true;
        }
        if(flag1)
            a201(false);
    }

    void a189()
    {
        if(!a194)
            a260 = !a260;
    }

    void a190()
    {
        a262 = false;
        a261 = !a261;
        a255.a410(a261);
        a234.a410(a261);
        if(a261)
        {
            if(a229 == null)
            {
                a239 = 1;
                a229 = new int[a239][a240];
                for(int i = 0; i < a240; i++)
                    a229[0][i] = i;

            }
            int j = a239 >= 4 ? 4 : a239;
            if(a230 == null)
            {
                a230 = new int[4];
                for(int k = 0; k < 4; k++)
                    a230[k] = 0;

            }
            if(a231 == null)
            {
                a231 = new int[4];
                for(int l = 0; l < 4; l++)
                    if(l < j)
                        a231[l] = l;
                    else
                        a231[l] = -1;

            }
            if(a232 == null)
            {
                a232 = new int[4];
                for(int i1 = 0; i1 < 4; i1++)
                    a232[i1] = -1;

            }
            if(a233 == null)
            {
                a233 = new int[4];
                for(int j1 = 0; j1 < 4; j1++)
                    a233[j1] = -1;

            }
        }
        a215(2);
    }

    void a191()
    {
        a263 = !a263;
        a260 = false;
        if(!a263)
            a237[a241].a106(-1);
        a255.a191(a263);
        if(a263 && a276 != null)
            a276.a933(a275, a255);
    }

    void a192(int i)
    {
        if(i == -1)
            a237[a241].a111(this);
        a237[a241].a106(i);
        a255.a192(i);
    }

    void a193()
    {
        int i = a234.a150(a241);
        int j = a234.a151(a241);
        Dimension dimension = getSize();
        float f = (float)dimension.width / (float)i;
        float f1 = (float)dimension.height / (float)j;
        if(f > 2.0F)
            f = 2.0F;
        if(f1 > 2.0F)
            f1 = 2.0F;
        if(f < f1)
            a264 = f;
        else
            a264 = f1;
        a264 *= 10F;
        a264 = (float)Math.floor(a264);
        a264 /= 10F;
        if(a264 == 1.1F)
            a264 = 1.0F;
    }

    void a194()
    {
        if(a249 == 1)
        {
            a194 = !a194;
            if(a194)
            {
                a260 = false;
                a255.a446(1);
                a199(false);
                return;
            } else
            {
                a255.a446(2);
                return;
            }
        } else
        {
            a255.a446(0);
            a255.a194(a194);
            return;
        }
    }

    void a195()
    {
        a264 += 0.1F;
        a264 *= 10F;
        a264 = Math.round(a264);
        a264 /= 10F;
        if(a264 > 2.0F)
            a264 = 2.0F;
        a199(false);
    }

    void a196()
    {
        a264 -= 0.1F;
        a264 *= 10F;
        a264 = Math.round(a264);
        a264 /= 10F;
        if(a264 < 0.1F)
            a264 = 0.1F;
        a199(false);
    }

    void a197()
    {
        a264 = 1.0F;
        a199(false);
    }

    void a198()
    {
        if(a261 && a231[a242] == -1)
        {
            return;
        } else
        {
            a234.a198(a216());
            a199(true);
            return;
        }
    }

    void a199(boolean flag)
    {
        Graphics g = getGraphics();
        Dimension dimension = getSize();
        g.setColor(getBackground());
        g.fillRect(0, 0, dimension.width, dimension.height);
        g.setColor(getForeground());
        g.dispose();
        if(flag)
        {
            for(int i = 0; i < a238; i++)
                a235[i] = null;

        }
        if(a194)
            a193();
        if(a249 == 1)
            a188(true);
        repaint();
    }

    void a200()
    {
        for(int i = 0; i < a238; i++)
            a235[i] = null;

        if(a249 == 1)
            a236 = null;
        repaint();
    }

    void a201(boolean flag)
    {
        a235[a242] = null;
        if(a249 == 1 && flag)
            a236 = null;
        repaint();
    }

    void a202(int i)
    {
        if(a263)
        {
            a237[a241].a106(-1);
            a255.a192(-1);
        }
        a241 = i;
        a199(true);
    }

    void a203(int i)
    {
        if(a261)
        {
            if(a231[a242] == -1)
            {
                a231[a242] = 0;
                a230[a242] = 0;
            } else
            if(i == 5)
            {
                int j = a231[a242];
                j = ++j != a239 ? j : 0;
                a231[a242] = j;
                a230[a242] = 0;
            } else
            if(a262)
            {
                for(int j1 = 0; j1 < a249; j1++)
                    if(a231[j1] != -1)
                    {
                        int k = a230[j1];
                        k = ++k != a229[a231[j1]].length ? k : 0;
                        a230[j1] = k;
                    }

            } else
            {
                int l = a230[a242];
                l = ++l != a229[a231[a242]].length ? l : 0;
                a230[a242] = l;
            }
            a199(true);
            return;
        }
        if(a240 > a249)
        {
            int i1 = a241 + a249 * i;
            i1 %= a240;
            a202(i1);
        }
    }

    void a204(int i)
    {
        if(a261)
        {
            if(a231[a242] == -1)
            {
                a231[a242] = 0;
                a230[a242] = 0;
            } else
            if(i == 5)
            {
                int j = a231[a242];
                j = --j >= 0 ? j : a239 - 1;
                a231[a242] = j;
                a230[a242] = 0;
            } else
            if(a262)
            {
                for(int j1 = 0; j1 < a249; j1++)
                    if(a231[j1] != -1)
                    {
                        int k = a230[j1];
                        k = --k >= 0 ? k : a229[a231[j1]].length - 1;
                        a230[j1] = k;
                    }

            } else
            {
                int l = a230[a242];
                l = --l >= 0 ? l : a229[a231[a242]].length - 1;
                a230[a242] = l;
            }
            a199(true);
            return;
        }
        if(a240 > a249)
        {
            int i1 = a241 - a249 * i;
            if(i1 < 0)
                i1 = a240 + i1;
            i1 = i1 >= 0 ? i1 : 0;
            a202(i1);
        }
    }

    void a112()
    {
        if(a261 && a231[a242] == -1)
            return;
        a237[a216()].a112();
        a234.a112(a216());
        if(a234.a409(a216()))
        {
            a200();
            return;
        } else
        {
            a201(true);
            return;
        }
    }

    void a113()
    {
        if(a261 && a231[a242] == -1)
            return;
        a237[a216()].a113();
        a234.a113(a216());
        if(a234.a409(a216()))
        {
            a200();
            return;
        } else
        {
            a201(true);
            return;
        }
    }

    void a135()
    {
        if(a261 && a231[a242] == -1)
            return;
        a234.a135(a216());
        if(a234.a409(a216()))
        {
            a200();
            return;
        } else
        {
            a201(true);
            return;
        }
    }

    void a114()
    {
        if(a261 && a231[a242] == -1)
        {
            return;
        } else
        {
            a237[a216()].a114();
            a234.a114(a216());
            a199(true);
            return;
        }
    }

    void a115()
    {
        if(a261 && a231[a242] == -1)
        {
            return;
        } else
        {
            a237[a216()].a115();
            a234.a115(a216());
            a199(true);
            return;
        }
    }

    public float a205()
    {
        if(a265 > 0.0F)
            return a265;
        else
            return a264;
    }

    public float a146()
    {
        if(a249 != 1)
            return 0.0F;
        if(a236 == null)
            return 0.0F;
        else
            return a234.a146(a216());
    }

    public float a147()
    {
        if(a249 != 1)
            return 0.0F;
        if(a236 == null)
            return 0.0F;
        else
            return a234.a147(a216());
    }

    public int a206()
    {
        if(a249 != 1)
            return 0;
        if(a236 == null)
            return 0;
        else
            return a245;
    }

    public int a207()
    {
        if(a249 != 1)
            return 0;
        if(a236 == null)
            return 0;
        else
            return a246;
    }

    public Rectangle a208()
    {
        if(a249 != 1)
            return null;
        if(a236 == null)
            return null;
        int i = a245;
        if(i < 0)
            i = 0;
        int j = a246;
        if(j < 0)
            j = 0;
        int k = a236.getWidth(this);
        if(k > getSize().width)
            k = getSize().width;
        int l = a236.getHeight(this);
        if(l > getSize().height)
            l = getSize().height;
        return new Rectangle(i, j, k, l);
    }

    public int a209(int i, int j)
    {
        if(a249 != 1)
            return 0;
        if(a236 == null)
        {
            return 0;
        } else
        {
            int k = (int)((float)(i - a245) / a264);
            int l = (int)((float)(j - a246) / a264);
            return a234.a134(a216(), k, l);
        }
    }

    public boolean a210()
    {
        return a274.equals("CT");
    }

    void a211(int i, int j)
    {
        if(a261)
        {
            if(a231[a242] == -1)
                return;
            a268 = a233[a242];
            a267 = a232[a242];
        }
        a268 -= i * a252;
        a267 -= j * a251;
        a234.a137(a267, a268, a216());
        if(a261)
        {
            a233[a242] = a268;
            a232[a242] = a267;
        }
        a201(true);
    }

    void a212()
    {
        if(a261 && a231[a242] == -1)
            return;
        int i = a216();
        a267 = a234.a153(i);
        a268 = a234.a154(i);
        a234.a137(a267, a268, i);
        if(a261)
        {
            a233[a242] = a268;
            a232[a242] = a267;
        }
        a201(true);
    }

    void a213()
    {
        if(a261 && a231[a242] == -1)
            return;
        int i = a216();
        a267 = a234.a138(i);
        a268 = a234.a139(i);
        a234.a137(a267, a268, i);
        if(a261)
        {
            a233[a242] = a268;
            a232[a242] = a267;
        }
        a201(true);
    }

    void a214(int i, int j)
    {
        if(a261 && a231[a242] == -1)
            return;
        a267 = a269[i][j][0];
        a268 = a269[i][j][1];
        a234.a137(a267, a268, a216());
        if(a261)
        {
            a233[a242] = a268;
            a232[a242] = a267;
        }
        a201(true);
    }

    void a215(int i)
    {
        if(a250 != i)
        {
            a249 = i * i;
            a250 = i;
            if(a249 == 1)
            {
                a242 = 0;
                if(a194)
                    a255.a446(1);
                else
                    a255.a446(2);
            } else
            {
                if(a261)
                    a242 = 0;
                else
                    a242 = a241 % a249;
                a260 = false;
                a255.a446(0);
            }
        }
        a199(true);
    }

    int a216()
    {
        if(a261)
            return a229[a231[a242]][a230[a242]];
        else
            return a241;
    }

    void a217(Graphics g, int i)
    {
        if(i + 1 > a240)
            return;
        a186(a241);
        a187(a242);
        a181(g, false);
        a241 -= a242;
        if(a241 < 0)
            a241 = a240 + a241;
        a241 += i;
        a241 %= a240;
        a186(a241);
        a187(i);
        a181(g, true);
        a267 = a234.a406(a241);
        a268 = a234.a405(a241);
        a242 = i;
        a180();
        a183(a241);
        a255.a444((a241 + 1) + " of " + a240, "");
        a255.a445(a234.a405(a241), a234.a406(a241));
        if(!a259)
            a255.a450(a234.a152(a241));
    }

    void a218(Graphics g, int i)
    {
        if(a231[a242] == -1)
        {
            a247 = a253 / a250;
            a248 = a253 / a250;
        } else
        {
            a186(a229[a231[a242]][a230[a242]]);
        }
        a187(a242);
        a181(g, false);
        a242 = i;
        if(a231[a242] == -1)
        {
            a247 = a253 / a250;
            a248 = a253 / a250;
            a255.a444("0 of 0", "");
            a255.a445(0, 0);
        } else
        {
            a186(a229[a231[a242]][a230[a242]]);
            a267 = a234.a406(a229[a231[a242]][a230[a242]]);
            a268 = a234.a405(a229[a231[a242]][a230[a242]]);
            a232[a242] = a267;
            a233[a242] = a268;
            a180();
            a183(a229[a231[a242]][a230[a242]]);
            a255.a444((a230[a242] + 1) + " of " + a229[a231[a242]].length, "Series: " + (a231[a242] + 1) + " of " + a239);
            a255.a445(a234.a405(a229[a231[a242]][a230[a242]]), a234.a406(a229[a231[a242]][a230[a242]]));
            if(!a259)
                a255.a450(a234.a152(a229[a231[a242]][a230[a242]]));
        }
        a187(a242);
        a181(g, true);
    }

    void a219(int i, int j)
    {
        Dimension dimension = getSize();
        int k = dimension.width / a250;
        int l = dimension.height / a250;
        if(i > k * a250)
            return;
        int i1 = i / k + a250 * (j / l);
        if(i1 < a249 && i1 != a242)
        {
            Graphics g = getGraphics();
            if(a261)
                a218(g, i1);
            else
                a217(g, i1);
            g.dispose();
        }
    }

    void a220(float f)
    {
        if(a261 && a231[a242] == -1)
            return;
        int i = a216();
        if(a256 != null)
            a256.dispose();
        a256 = new a775(a234.a136(i), f);
        a256.setTitle("Zoom (x" + f + ") image" + (i + 1));
        a256.show();
        a256.a776();
    }

    Image a221(Image image)
    {
        if(a264 == 1.0F)
            return image;
        int i = a234.a150(a241);
        int j = a234.a151(a241);
        i = (int)(a264 * (float)i);
        j = (int)(a264 * (float)j);
        if(a259)
            return image.getScaledInstance(i, j, 2);
        if(a264 < 1.0F)
        {
            return image.getScaledInstance(i, j, 8);
        } else
        {
            Image image1 = createImage(new FilteredImageSource(image.getSource(), new a510()));
            return image1.getScaledInstance(i, j, 8);
        }
    }

    Image a222(Image image)
    {
        if(a264 == 1.0F)
            return image;
        int i = a234.a150(a241);
        int j = a234.a151(a241);
        i = (int)(a264 * (float)i);
        j = (int)(a264 * (float)j);
        if(a259)
            return image.getScaledInstance(i, j, 2);
        if(a264 < 1.0F)
            return image.getScaledInstance(i, j, 8);
        else
            return image.getScaledInstance(i, j, 16);
    }

    void a223()
    {
        a257 = !a257;
        a199(false);
    }

    void print()
    {
        java.awt.Container container;
        for(container = getParent(); !(container instanceof Frame); container = container.getParent());
        Frame frame = (Frame)container;
        PrintJob printjob = null;
        try
        {
            printjob = getToolkit().getPrintJob(frame, "DICOM Java Viewer Print", null);
        }
        catch(SecurityException securityexception)
        {
            System.err.println(securityexception);
        }
        if(printjob != null)
        {
            Dimension dimension = getSize();
            Dimension dimension1 = printjob.getPageDimension();
            a254 = printjob.getPageResolution();
            setSize(dimension1.width - 100, dimension1.height - 100);
            a182(false);
            Graphics g = printjob.getGraphics();
            if(g != null)
            {
                Graphics g1 = g.create(50, 50, dimension1.width - 100, dimension1.height - 100);
                if(g1 != null)
                    try
                    {
                        print(g1);
                    }
                    finally
                    {
                        g1.dispose();
                        g.dispose();
                        setSize(dimension);
                        a182(false);
                    }
            }
            printjob.end();
        }
    }

    void a224()
    {
        java.awt.Container container;
        for(container = getParent(); !(container instanceof Frame); container = container.getParent());
        Frame frame = (Frame)container;
        FileDialog filedialog = new FileDialog(frame, "Saving selected image to a JPEG file", 1);
        filedialog.show();
        String s = null;
        s = filedialog.getFile();
        if(s == null)
            return;
        String s1 = null;
        s1 = filedialog.getDirectory();
        s = filedialog.getFile();
        String s2 = s1 + s;
        File file = new File(s2);
        FileOutputStream fileoutputstream = null;
        try
        {
            fileoutputstream = new FileOutputStream(file);
        }
        catch(IOException ioexception)
        {
            System.err.println(ioexception);
        }
        Image image = a235[a242];
        if(image == null)
            image = a234.a136(a216());
        a617 a617_1 = new a617(image, 80, fileoutputstream);
        a617_1.a620();
        try
        {
            fileoutputstream.close();
            return;
        }
        catch(IOException ioexception1)
        {
            System.err.println(ioexception1);
        }
    }

    void a225()
    {
        if(a225 == null)
        {
            a225 = new a490(this);
            a255.a448(true);
            repaint();
            return;
        } else
        {
            a225 = null;
            a255.a448(false);
            a199(false);
            return;
        }
    }

    void a226()
    {
        if(a226 == null)
        {
            a226 = new a674(this);
            a255.a447(true);
        } else
        {
            Graphics g = getGraphics();
            a226.a199(g);
            a226 = null;
            a255.a447(false);
            g.dispose();
        }
        repaint();
    }

    public void processMouseEvent(MouseEvent mouseevent)
    {
        int i = mouseevent.getX();
        int j = mouseevent.getY();
        switch(mouseevent.getID())
        {
        case 503: 
        case 504: 
        case 505: 
        default:
            break;

        case 501: 
            if(a263)
            {
                a237[a241].a108(i, j, this);
                if(a237[a241].a105())
                    break;
            }
            if(a226 != null)
                a226.a492(i, j);
            else
            if(a225 != null)
                a225.a492(i, j);
            else
            if(a260)
            {
                setCursor(new Cursor(12));
            } else
            {
                setCursor(new Cursor(13));
                a180();
                if(a249 != 1)
                    a219(i, j);
            }
            a277 = i;
            a278 = j;
            a281 = 0;
            break;

        case 502: 
            if(a263)
                a237[a241].a110(i, j, this);
            setCursor(Cursor.getDefaultCursor());
            if(a281 >= 10)
            {
                a259 = false;
                if(a225 != null)
                    a225.a493();
                else
                if(a226 != null)
                    a226.a493();
                else
                if(a260)
                    repaint();
                else
                    a201(true);
            }
            break;
        }
        super.processMouseEvent(mouseevent);
    }

    public void processMouseMotionEvent(MouseEvent mouseevent)
    {
label0:
        {
            int i = mouseevent.getX();
            int j = mouseevent.getY();
            switch(mouseevent.getID())
            {
            default:
                break;

            case 506: 
                if(a263)
                {
                    a237[a241].a109(i, j, this);
                    if(a237[a241].a105())
                        break label0;
                }
                if(a281 < 10)
                {
                    a281++;
                } else
                {
                    a259 = true;
                    if(a225 != null)
                        a225.a109(i, j);
                    else
                    if(a226 != null)
                        a226.a109(i, j);
                    else
                    if(a277 != i || a278 != j)
                    {
                        a279 = i - a277;
                        a280 = j - a278;
                        if(a260)
                            a188(false);
                        else
                            a211(a277 - i, a278 - j);
                        a277 = i;
                        a278 = j;
                    }
                }
                break;
            }
        }
        super.processMouseMotionEvent(mouseevent);
    }

    public void processComponentEvent(ComponentEvent componentevent)
    {
        switch(componentevent.getID())
        {
        case 101: // 'e'
            a182(true);
            break;
        }
        super.processComponentEvent(componentevent);
    }
}
