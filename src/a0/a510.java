// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import java.awt.image.ColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageFilter;
import java.util.Hashtable;

public class a510 extends ImageFilter
{

    protected int a511;
    protected int a512;
    protected int a513;
    protected int a514;
    protected Object a515;

    public a510()
    {
    }

    public void setProperties(Hashtable hashtable)
    {
        hashtable = (Hashtable)hashtable.clone();
        String s = a513 + "x" + a514;
        Object obj = hashtable.get("rescale");
        if(obj != null && (obj instanceof String))
            s = (String)obj + ", " + s;
        hashtable.put("rescale", s);
        super.setProperties(hashtable);
    }

    public void setDimensions(int i, int j)
    {
        a511 = i;
        a512 = j;
        a513 = 2 * a511;
        a514 = 2 * a512;
        super.consumer.setDimensions(a513, a514);
    }

    public void setPixels(int i, int j, int k, int l, ColorModel colormodel, byte abyte0[], int i1, 
            int j1)
    {
        boolean flag = false;
        int k1 = 2 * (j + l);
        if(k1 >= a514)
        {
            k1 = a514 - 1;
            flag = true;
        }
        int l1 = 2 * (i + k);
        if(l1 >= a513)
            l1 = a513 - 1;
        byte abyte1[];
        if(a515 != null && (a515 instanceof byte[]))
        {
            abyte1 = (byte[])a515;
        } else
        {
            abyte1 = new byte[a513];
            abyte1[a513 - 1] = 0;
            a515 = abyte1;
        }
        for(int i2 = j; i2 < k1; i2++)
        {
            int j2 = i2 - j >> 1;
            if(j2 < 0)
                j2 = 0;
            int l2 = i1 + j1 * j2;
            boolean flag1 = i2 % 2 == 0;
            for(int i3 = i; i3 < l1; i3++)
            {
                int j3 = l2 + (i3 >> 1);
                if(flag1)
                {
                    if((i3 & 1) == 0)
                    {
                        abyte1[i3] = abyte0[j3];
                    } else
                    {
                        int k3 = 0xff & abyte0[j3];
                        int j4 = 0xff & abyte0[j3 + 1];
                        abyte1[i3] = (byte)(k3 + j4 >> 1);
                    }
                } else
                if((i3 & 1) == 0)
                {
                    int l3 = 0xff & abyte0[j3];
                    int k4 = 0xff & abyte0[j3 + j1];
                    abyte1[i3] = (byte)(l3 + k4 >> 1);
                } else
                {
                    int i4 = 0xff & abyte0[j3 + 1];
                    int l4 = 0xff & abyte0[j3 + j1];
                    abyte1[i3] = (byte)(i4 + l4 >> 1);
                }
            }

            super.consumer.setPixels(i, i2, k << 1, 1, colormodel, abyte1, i, a513);
        }

        if(flag)
        {
            for(int k2 = i; k2 < l1; k2++)
                abyte1[k2] = 0;

            super.consumer.setPixels(i, a514 - 1, 2 * k, 1, colormodel, abyte1, i, a513);
        }
    }

    public void setPixels(int i, int j, int k, int l, ColorModel colormodel, int ai[], int i1, 
            int j1)
    {
        boolean flag = false;
        int k1 = 2 * (j + l);
        if(k1 >= a514)
        {
            k1 = a514 - 1;
            flag = true;
        }
        int l1 = 2 * (i + k);
        if(l1 >= a513)
            l1 = a513 - 1;
        int ai1[];
        if(a515 != null && (a515 instanceof int[]))
        {
            ai1 = (int[])a515;
        } else
        {
            ai1 = new int[a513];
            ai1[a513 - 1] = 0;
            a515 = ai1;
        }
        for(int i2 = j; i2 < k1; i2++)
        {
            int j2 = i2 - j >> 1;
            if(j2 < 0)
                j2 = 0;
            int l2 = i1 + j1 * j2;
            boolean flag1 = i2 % 2 == 0;
            for(int i3 = i; i3 < l1; i3++)
            {
                int j3 = l2 + (i3 >> 1);
                if(flag1)
                {
                    if((i3 & 1) == 0)
                    {
                        ai1[i3] = ai[j3];
                    } else
                    {
                        int k3 = ai[j3];
                        int j4 = k3 >>> 24;
                        int i5 = k3 >> 16 & 0xff;
                        int l5 = k3 >> 8 & 0xff;
                        int k6 = k3 & 0xff;
                        int j7 = ai[j3 + 1];
                        int i8 = j7 >>> 24;
                        int l8 = j7 >> 16 & 0xff;
                        int k9 = j7 >> 8 & 0xff;
                        int j10 = j7 & 0xff;
                        int i11 = j4 + i8 >> 1;
                        int l11 = i5 + l8 >> 1;
                        int k12 = l5 + k9 >> 1;
                        int j13 = k6 + j10 >> 1;
                        ai1[i3] = i11 << 24 | l11 << 16 | k12 << 8 | j13;
                    }
                } else
                if((i3 & 1) == 0)
                {
                    int l3 = ai[j3];
                    int k4 = l3 >>> 24;
                    int j5 = l3 >> 16 & 0xff;
                    int i6 = l3 >> 8 & 0xff;
                    int l6 = l3 & 0xff;
                    int k7 = ai[j3 + j1];
                    int j8 = k7 >>> 24;
                    int i9 = k7 >> 16 & 0xff;
                    int l9 = k7 >> 8 & 0xff;
                    int k10 = k7 & 0xff;
                    int j11 = k4 + j8 >> 1;
                    int i12 = j5 + i9 >> 1;
                    int l12 = i6 + l9 >> 1;
                    int k13 = l6 + k10 >> 1;
                    ai1[i3] = j11 << 24 | i12 << 16 | l12 << 8 | k13;
                } else
                {
                    int i4 = ai[j3 + 1];
                    int l4 = i4 >>> 24;
                    int k5 = i4 >> 16 & 0xff;
                    int j6 = i4 >> 8 & 0xff;
                    int i7 = i4 & 0xff;
                    int l7 = ai[j3 + j1];
                    int k8 = l7 >>> 24;
                    int j9 = l7 >> 16 & 0xff;
                    int i10 = l7 >> 8 & 0xff;
                    int l10 = l7 & 0xff;
                    int k11 = l4 + k8 >> 1;
                    int j12 = k5 + j9 >> 1;
                    int i13 = j6 + i10 >> 1;
                    int l13 = i7 + l10 >> 1;
                    ai1[i3] = k11 << 24 | j12 << 16 | i13 << 8 | l13;
                }
            }

            super.consumer.setPixels(i, i2, k << 1, 1, colormodel, ai1, i, a513);
        }

        if(flag)
        {
            for(int k2 = i; k2 < l1; k2++)
                ai1[k2] = 0xff000000;

            super.consumer.setPixels(i, a514 - 1, 2 * k, 1, colormodel, ai1, i, a513);
        }
    }
}
