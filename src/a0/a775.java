// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

public class a775 extends Frame
{

    Image a780;
    int a362;
    int a363;
    int a781;
    int a782;
    int a783;
    int a784;
    int a785;
    int a786;

    public a775(Image image, float f)
    {
        a362 = image.getWidth(this);
        a363 = image.getHeight(this);
        if((double)f == 1.0D)
        {
            a780 = image;
        } else
        {
            float f1 = (float)a362 * f;
            float f2 = (float)a363 * f;
            a362 = (int)f1;
            a363 = (int)f2;
            a780 = image.getScaledInstance(a362, a363, 4);
        }
        a781 = (a362 <= 600 ? a362 : 600) + 11;
        a782 = (a363 <= 400 ? a363 : 400) + 25;
        setBackground(Color.black);
        enableEvents(16L);
        enableEvents(32L);
        enableEvents(64L);
        enableEvents(1L);
    }

    public void a776()
    {
        setBounds(0, 0, a781, a782);
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        a779();
        g.drawImage(a780, a783, a784, this);
    }

    public void processMouseEvent(MouseEvent mouseevent)
    {
        switch(mouseevent.getID())
        {
        case 501: 
            setCursor(new Cursor(12));
            a785 = mouseevent.getX();
            a786 = mouseevent.getY();
            break;

        case 502: 
            setCursor(Cursor.getDefaultCursor());
            break;
        }
        super.processMouseEvent(mouseevent);
    }

    public void processMouseMotionEvent(MouseEvent mouseevent)
    {
        boolean flag = false;
        int i = mouseevent.getX();
        int j = mouseevent.getY();
        switch(mouseevent.getID())
        {
        case 506: 
            a778();
            if(a785 != i || a786 != j)
            {
                if(a362 > a781)
                {
                    a783 += i - a785;
                    if(a783 > 0)
                        a783 = 0;
                    if(a783 < a781 - a362)
                        a783 = a781 - a362;
                    flag = true;
                }
                if(a363 > a782)
                {
                    a784 += j - a786;
                    if(a784 > 0)
                        a784 = 0;
                    if(a784 < a782 - a363)
                        a784 = a782 - a363;
                    flag = true;
                }
                if(flag)
                {
                    repaint();
                    a785 = i;
                    a786 = j;
                }
            }
            break;
        }
        super.processMouseMotionEvent(mouseevent);
    }

    public void processWindowEvent(WindowEvent windowevent)
    {
        switch(windowevent.getID())
        {
        case 201: 
            dispose();
            break;
        }
        super.processWindowEvent(windowevent);
    }

    public void processComponentEvent(ComponentEvent componentevent)
    {
        switch(componentevent.getID())
        {
        case 101: // 'e'
            a777();
            break;
        }
        super.processComponentEvent(componentevent);
    }

    void a777()
    {
        Graphics g = getGraphics();
        a783 = 0;
        a784 = 0;
        Object obj;
        for(obj = this; !(obj instanceof Window); obj = ((Window)obj).getParent());
        ((Window)obj).update(g);
        g.dispose();
    }

    void a778()
    {
        Rectangle rectangle = getBounds();
        a781 = rectangle.width;
        a782 = rectangle.height - 23;
    }

    void a779()
    {
        a778();
        if(a781 >= a362)
            a783 = (a781 - a362) / 2;
        if(a782 >= a363)
            a784 = (a782 - a363) / 2;
    }
}
