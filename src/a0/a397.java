// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import a0.a127.a683;
import a0.a127.a898;
import a0.a127.a912;
import a0.a127.a918;
import a0.a127.a964;
import java.awt.Component;
import java.awt.Image;
import java.net.URL;

public class a397
{

    int a417;
    int a240;
    String a418[];
    a964 a235[];
    boolean a419[];
    int a420[];
    Component a421;
    URL a422;
    int a423;
    boolean a261;
    boolean a424;
    int a425[][];
    a918 a426;
    boolean a427[];
    String a428;

    public a397(String as[], URL url, Component component, a918 a918, int i)
    {
        a261 = false;
        a424 = false;
        a417 = i;
        a235 = new a964[a417 + 1];
        a419 = new boolean[a417 + 1];
        a420 = new int[a417];
        for(int j = 0; j < a417; j++)
            a420[j] = -1;

        a418 = as;
        a240 = as.length;
        a426 = a918;
        a421 = component;
        a422 = url;
    }

    public URL a398()
    {
        return a422;
    }

    public void a399(boolean aflag[])
    {
        a427 = aflag;
    }

    public void a400(String s)
    {
        a428 = s;
    }

    public void a401()
    {
    }

    public void a402()
    {
    }

    public void a403()
    {
    }

    public void a404()
    {
    }

    public void a198(int i)
    {
        if(a427 == null)
        {
            return;
        } else
        {
            a427[i] = false;
            return;
        }
    }

    public Image a136(int i)
    {
        if(a424)
        {
            for(int j = 0; j < a240; j++)
            {
                for(int k = 0; k < a425[j].length; k++)
                    if(a425[j][k] == i)
                        return a235[a415(i)].a136(k);

            }

        }
        return a235[a415(i)].a136();
    }

    public int a144(int i)
    {
        return a235[a415(i)].a144();
    }

    public boolean a142(int i)
    {
        return a235[a415(i)].a142();
    }

    public a918 a152(int i)
    {
        return a235[a415(i)].a152();
    }

    public int a150(int i)
    {
        return a235[a415(i)].a150();
    }

    public int a151(int i)
    {
        return a235[a415(i)].a151();
    }

    public int a405(int i)
    {
        return a235[a415(i)].a405();
    }

    public int a406(int i)
    {
        return a235[a415(i)].a406();
    }

    public int a154(int i)
    {
        return a235[a415(i)].a154();
    }

    public int a153(int i)
    {
        return a235[a415(i)].a153();
    }

    public int a139(int i)
    {
        return a235[a415(i)].a139();
    }

    public int a138(int i)
    {
        return a235[a415(i)].a138();
    }

    public void a137(int i, int j, int k)
    {
        int l = a415(k);
        a235[l].a137(i, j);
        if(a419[l])
            a419[l] = false;
    }

    public boolean a407(int i)
    {
        return a419[a415(i)];
    }

    public boolean a408(int i)
    {
        a964 a964_1 = a235[a415(i)];
        if(a964_1 instanceof a683)
            return true;
        return a964_1 instanceof a898;
    }

    public boolean a409(int i)
    {
        return a235[a415(i)].a143();
    }

    public void a135(int i)
    {
        a235[a415(i)].a135();
    }

    public void a112(int i)
    {
        a235[a415(i)].a112();
    }

    public void a113(int i)
    {
        a235[a415(i)].a113();
    }

    public void a114(int i)
    {
        a235[a415(i)].a114();
    }

    public void a115(int i)
    {
        a235[a415(i)].a115();
    }

    public int a134(int i, int j, int k)
    {
        if(a424)
        {
            for(int l = 0; l < a240; l++)
            {
                for(int i1 = 0; i1 < a425[l].length; i1++)
                    if(a425[l][i1] == i)
                        return a235[a415(i)].a134(i1, j, k);

            }

        }
        return a235[a415(i)].a134(0, j, k);
    }

    public float a146(int i)
    {
        return a235[a415(i)].a146();
    }

    public float a147(int i)
    {
        return a235[a415(i)].a147();
    }

    public void a410(boolean flag)
    {
        a261 = flag;
    }

    public int a411()
    {
        if(a424)
        {
            int i = 0;
            for(int j = 0; j < a240; j++)
                i += a425[j].length;

            return i;
        } else
        {
            return a240;
        }
    }

    public void a412(int ai[][])
    {
        a424 = true;
        a425 = ai;
    }

    public int[][] a413()
    {
        return a425;
    }

    public boolean a143(boolean flag)
    {
        if(flag)
            a424 = true;
        else
        if(a235[a415(0)].a143())
            a424 = true;
        else
            a424 = a235[a415(a240 / 2)].a143();
        if(a424)
        {
            a424 = false;
            a425 = new int[a240][];
            int i = 0;
            int j = 0;
            for(; i < a240; i++)
            {
                int k = a235[a415(i)].a145();
                a425[i] = new int[k];
                for(int l = 0; l < k;)
                {
                    a425[i][l] = j;
                    l++;
                    j++;
                }

            }

            a424 = true;
        }
        return a424;
    }

    int a414(int i)
    {
        for(int j = 0; j < a417; j++)
            if(a420[j] == -1)
                return j;

        int k = 0;
        int l = 0;
        for(int i1 = 0; i1 < a417; i1++)
        {
            int j1 = i - a420[i1];
            if(j1 < 0)
                j1 = -j1;
            int k1 = a240 - j1;
            int l1 = j1 >= k1 ? k1 : j1;
            if(k < l1)
            {
                k = l1;
                l = i1;
            }
        }

        return l;
    }

    int a415(int i)
    {
        if(a424)
        {
            boolean flag = false;
            for(int k = 0; k < a240; k++)
            {
                for(int l = 0; l < a425[k].length; l++)
                    if(a425[k][l] == i)
                    {
                        i = k;
                        l = a425[k].length;
                        flag = true;
                    }

                if(flag)
                    k = a240;
            }

        }
        if(a427 != null && a427[i])
        {
            if(a235[a417] == null)
                a235[a417] = a912.a136(a422, a428, a421, a426);
            return a417;
        }
        for(int j = 0; j < a417; j++)
            if(i == a420[j])
                return j;

        return a416(i);
    }

    int a416(int i)
    {
        int j;
        if(a261)
        {
            j = a423;
            a423++;
            a423 = a423 != a417 ? a423 : 0;
        } else
        {
            j = a414(i);
        }
        a235[j] = a912.a136(a422, a418[i], a421, a426);
        a420[j] = i;
        a419[j] = true;
        return j;
    }
}
