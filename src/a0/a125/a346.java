// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a125;

import a0.a167;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

// Referenced classes of package a0.a125:
//            a599

public class a346 extends a599
{

    int a305;
    int a306;
    Point a354;
    int a355;
    boolean a356;
    a346 a357;

    public a346(a167 a167)
    {
        a600(a167);
    }

    public void a299(int i, int j)
    {
        super.a605 = i;
        super.a606 = j;
        super.a366 = i;
        super.a367 = j;
    }

    public void a300(int i, int j)
    {
        a305 = super.a366;
        a306 = super.a367;
        super.a366 = i;
        super.a367 = j;
    }

    public void a347(Vector vector)
    {
        if(vector == null)
            return;
        for(int i = 0; i < vector.size(); i++)
        {
            Object obj = vector.elementAt(i);
            if((obj instanceof a346) && a348((a346)obj))
                return;
        }

    }

    public void a107(Graphics g, boolean flag)
    {
        g.setColor(Color.cyan);
        if(!flag)
        {
            Color color = new Color(192, 63, 255);
            g.setXORMode(color);
            g.drawLine(super.a605, super.a606, a305, a306);
        }
        a601();
        g.drawLine(super.a605, super.a606, super.a366, super.a367);
        if(a357 != null)
        {
            a352();
            int i = Math.max(super.a605, super.a366);
            int j;
            if(i == super.a605)
                j = super.a606;
            else
                j = super.a367;
            i += 3;
            g.setFont(new Font("SansSerif", 1, 11));
            g.drawString("Angle " + a355 + "\272", i, j);
        }
    }

    boolean a348(a346 a346_1)
    {
        if(a346_1.a356)
        {
            return false;
        } else
        {
            a346_1.a356 = true;
            a356 = true;
            a357 = a346_1;
            return true;
        }
    }

    Point a349()
    {
        if(a357 != null)
        {
            a354 = a353();
            a357.a354 = a354;
        }
        double d = a350(a354, new Point(super.a605, super.a606));
        double d1 = a350(a354, new Point(super.a366, super.a367));
        if(d1 > d)
            return new Point(super.a366 - super.a605, super.a367 - super.a606);
        else
            return new Point(super.a605 - super.a366, super.a606 - super.a367);
    }

    double a350()
    {
        Point point = new Point(super.a366 - super.a605, super.a367 - super.a606);
        return Math.sqrt(point.x * point.x + point.y * point.y);
    }

    double a350(Point point, Point point1)
    {
        Point point2 = new Point(point1.x - point.x, point1.y - point.y);
        return Math.sqrt(point2.x * point2.x + point2.y * point2.y);
    }

    int a351()
    {
        Point point = a349();
        Point point1 = a357.a349();
        return point.x * point1.x + point.y * point1.y;
    }

    void a352()
    {
        double d = a357.a350() * a350();
        double d1;
        if(d == 0.0D)
        {
            d1 = 0.0D;
        } else
        {
            int i = a351();
            d1 = Math.acos((double)i / d);
        }
        a355 = (int)Math.round((180D * d1) / 3.1415926535897931D);
    }

    Point a353()
    {
        Point point = new Point(super.a366 - super.a605, super.a367 - super.a606);
        Point point1 = new Point(((a599) (a357)).a366 - ((a599) (a357)).a605, ((a599) (a357)).a367 - ((a599) (a357)).a606);
        if(point.y == 0 && point1.y == 0)
            return new Point(super.a605, super.a606);
        if(point.y == 0)
        {
            int l = super.a606;
            int i = l - ((a599) (a357)).a606;
            i *= point1.x;
            i /= point1.y;
            i += ((a599) (a357)).a605;
            return new Point(i, l);
        }
        if(point1.y == 0)
        {
            int i1 = ((a599) (a357)).a606;
            int j = i1 - super.a606;
            j *= point.x;
            j /= point.y;
            j += super.a605;
            return new Point(j, i1);
        }
        double d = (double)point.x / (double)point.y - (double)point1.x / (double)point1.y;
        if(d == 0.0D)
        {
            return new Point(super.a605, super.a606);
        } else
        {
            double d1 = (((double)point.x * (double)super.a606) / (double)point.y + (double)((a599) (a357)).a605) - (double)super.a605 - ((double)point1.x * (double)((a599) (a357)).a606) / (double)point1.y;
            int j1 = (int)Math.round(d1 / d);
            int k = j1 - super.a606;
            k *= point.x;
            k /= point.y;
            k += super.a605;
            return new Point(k, j1);
        }
    }
}
