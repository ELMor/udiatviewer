// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a125;

import a0.a167;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

// Referenced classes of package a0.a125:
//            a599

public class a298 extends a599
{

    static final int a304 = 10;
    int a305;
    int a306;
    int a307;

    public a298(a167 a167_1)
    {
        a307 = -1;
        a600(a167_1);
    }

    public void a299(int i, int j)
    {
        super.a605 = i;
        super.a606 = j;
        super.a366 = i;
        super.a367 = j;
    }

    public void a300(int i, int j)
    {
        a305 = super.a366;
        a306 = super.a367;
        super.a366 = i;
        super.a367 = j;
    }

    public void a107(Graphics g, boolean flag)
    {
        if(flag)
        {
            a302(g);
            return;
        } else
        {
            a301(g);
            return;
        }
    }

    void a301(Graphics g)
    {
        g.setColor(Color.cyan);
        Color color = new Color(192, 63, 255);
        g.setXORMode(color);
        g.drawLine(super.a605, super.a606, a305, a306);
        g.drawLine(super.a605, super.a606, super.a366, super.a367);
    }

    void a302(Graphics g)
    {
        a601();
        g.setColor(Color.cyan);
        g.drawLine(super.a605, super.a606, super.a366, super.a367);
        float f = super.a0.a146();
        float f1 = super.a0.a147();
        int i = super.a366 - super.a605;
        int j = super.a367 - super.a606;
        if(a307 < 0)
            if(f == 0.0F || f1 == 0.0F)
            {
                a307 = (int)Math.round(Math.sqrt(i * i + j * j) / (double)super.a0.a205());
            } else
            {
                i = (int)((float)i * f);
                j = (int)((float)j * f1);
                a307 = (int)Math.round(Math.sqrt(i * i + j * j) / (double)super.a0.a205());
            }
        String s;
        if(f == 0.0F || f1 == 0.0F)
            s = a307 + " pix";
        else
            s = a307 + " mm";
        g.setFont(new Font("SansSerif", 1, 11));
        Dimension dimension = a185(g, s);
        int k = (super.a605 + super.a366) / 2 - dimension.width / 2;
        int l = (super.a606 + super.a367) / 2 - dimension.height / 2;
        int i1 = Math.abs(i);
        int j1 = Math.abs(j);
        if(i1 < dimension.width && j1 < dimension.height)
            if(i1 > j1)
                l += dimension.height / 2 + 3 + j1;
            else
                k += dimension.width / 2 + 3 + i1;
        g.setColor(Color.black);
        g.fillRect(k, l, dimension.width, dimension.height);
        g.setColor(Color.cyan);
        g.drawString(s, k + 5, l + a303(g));
    }

    Dimension a185(Graphics g, String s)
    {
        if(s == null)
        {
            return null;
        } else
        {
            FontMetrics fontmetrics = g.getFontMetrics();
            return new Dimension(fontmetrics.stringWidth(s) + 10, fontmetrics.getHeight() + 5);
        }
    }

    int a303(Graphics g)
    {
        FontMetrics fontmetrics = g.getFontMetrics();
        return fontmetrics.getAscent() + 2;
    }
}
