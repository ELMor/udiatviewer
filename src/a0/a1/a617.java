// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a1;

import java.awt.Image;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package a0.a1:
//            a528, a368, a313

public class a617
{

    BufferedOutputStream a626;
    Image a627;
    a313 a628;
    a368 a629;
    a528 a630;
    int a319;
    int a320;
    int a631;
    int a383;
    public static int a396[] = {
        0, 1, 8, 16, 9, 2, 3, 10, 17, 24, 
        32, 25, 18, 11, 4, 5, 12, 19, 26, 33, 
        40, 48, 41, 34, 27, 20, 13, 6, 7, 14, 
        21, 28, 35, 42, 49, 56, 57, 50, 43, 36, 
        29, 22, 15, 23, 30, 37, 44, 51, 58, 59, 
        52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 
        47, 55, 62, 63
    };

    public a617(Image image, int i, OutputStream outputstream)
    {
        a631 = i;
        a628 = new a313(image);
        a319 = a628.a319;
        a320 = a628.a320;
        a626 = new BufferedOutputStream(outputstream);
        a630 = new a528(a631);
        a629 = new a368(a320, a319);
    }

    public void a618(int i)
    {
        a630 = new a528(i);
    }

    public int a619()
    {
        return a631;
    }

    public void a620()
    {
        a623(a626);
        a621(a626);
        a622(a626);
        try
        {
            a626.flush();
            return;
        }
        catch(IOException ioexception)
        {
            System.out.println("IO Error: " + ioexception.getMessage());
        }
    }

    public void a621(BufferedOutputStream bufferedoutputstream)
    {
        float af1[][] = new float[8][8];
        double ad[][] = new double[8][8];
        int ai[] = new int[64];
        int ai2[] = new int[a628.a324];
        boolean flag1 = false;
        boolean flag2 = false;
        int k3 = a320 % 8 == 0 ? a320 : (int)(Math.floor((double)a320 / 8D) + 1.0D) * 8;
        int l3 = a319 % 8 == 0 ? a319 : (int)(Math.floor((double)a319 / 8D) + 1.0D) * 8;
        for(int k1 = 0; k1 < a628.a324; k1++)
        {
            k3 = Math.min(k3, a628.a321[k1]);
            l3 = Math.min(l3, a628.a322[k1]);
        }

        boolean flag = false;
        for(int k = 0; k < l3; k++)
        {
            for(int l = 0; l < k3; l++)
            {
                int i2 = l * 8;
                int j2 = k * 8;
                for(int l1 = 0; l1 < a628.a324; l1++)
                {
                    int i3 = a628.a321[l1];
                    int j3 = a628.a322[l1];
                    float af[][] = (float[][])a628.a325[l1];
                    for(int i = 0; i < a628.a328[l1]; i++)
                    {
                        for(int j = 0; j < a628.a327[l1]; j++)
                        {
                            int k2 = j * 8;
                            int l2 = i * 8;
                            for(int i1 = 0; i1 < 8; i1++)
                            {
                                for(int j1 = 0; j1 < 8; j1++)
                                    af1[i1][j1] = af[j2 + l2 + i1][i2 + k2 + j1];

                            }

                            double ad1[][] = a630.a530(af1);
                            int ai1[] = a630.a531(ad1, a628.a329[l1]);
                            a629.a369(bufferedoutputstream, ai1, ai2[l1], a628.a330[l1], a628.a331[l1]);
                            ai2[l1] = ai1[0];
                        }

                    }

                }

            }

        }

        a629.a371(bufferedoutputstream);
    }

    public void a622(BufferedOutputStream bufferedoutputstream)
    {
        byte abyte0[] = {
            -1, -39
        };
        a624(abyte0, bufferedoutputstream);
    }

    public void a623(BufferedOutputStream bufferedoutputstream)
    {
        byte abyte0[] = {
            -1, -40
        };
        a624(abyte0, bufferedoutputstream);
        byte abyte1[] = new byte[18];
        abyte1[0] = -1;
        abyte1[1] = -32;
        abyte1[2] = 0;
        abyte1[3] = 16;
        abyte1[4] = 74;
        abyte1[5] = 70;
        abyte1[6] = 73;
        abyte1[7] = 70;
        abyte1[8] = 0;
        abyte1[9] = 1;
        abyte1[10] = 0;
        abyte1[11] = 0;
        abyte1[12] = 0;
        abyte1[13] = 1;
        abyte1[14] = 0;
        abyte1[15] = 1;
        abyte1[16] = 0;
        abyte1[17] = 0;
        a625(abyte1, bufferedoutputstream);
        String s = new String();
        s = a628.a315();
        int j2 = s.length();
        byte abyte2[] = new byte[j2 + 4];
        abyte2[0] = -1;
        abyte2[1] = -2;
        abyte2[2] = (byte)(j2 >> 8 & 0xff);
        abyte2[3] = (byte)(j2 & 0xff);
        System.arraycopy(a628.a317.getBytes(), 0, abyte2, 4, a628.a317.length());
        a625(abyte2, bufferedoutputstream);
        byte abyte3[] = new byte[134];
        abyte3[0] = -1;
        abyte3[1] = -37;
        abyte3[2] = 0;
        abyte3[3] = -124;
        int i2 = 4;
        for(int i = 0; i < 2; i++)
        {
            abyte3[i2++] = (byte)i;
            int ai[] = (int[])a630.a534[i];
            for(int i1 = 0; i1 < 64; i1++)
                abyte3[i2++] = (byte)ai[a396[i1]];

        }

        a625(abyte3, bufferedoutputstream);
        byte abyte4[] = new byte[19];
        abyte4[0] = -1;
        abyte4[1] = -64;
        abyte4[2] = 0;
        abyte4[3] = 17;
        abyte4[4] = (byte)a628.a323;
        abyte4[5] = (byte)(a628.a319 >> 8 & 0xff);
        abyte4[6] = (byte)(a628.a319 & 0xff);
        abyte4[7] = (byte)(a628.a320 >> 8 & 0xff);
        abyte4[8] = (byte)(a628.a320 & 0xff);
        abyte4[9] = (byte)a628.a324;
        int l1 = 10;
        for(int j = 0; j < abyte4[9]; j++)
        {
            abyte4[l1++] = (byte)a628.a326[j];
            abyte4[l1++] = (byte)((a628.a327[j] << 4) + a628.a328[j]);
            abyte4[l1++] = (byte)a628.a329[j];
        }

        a625(abyte4, bufferedoutputstream);
        j2 = 2;
        l1 = 4;
        int i3 = 4;
        byte abyte5[] = new byte[17];
        byte abyte8[] = new byte[4];
        abyte8[0] = -1;
        abyte8[1] = -60;
        for(int k = 0; k < 4; k++)
        {
            int k2 = 0;
            abyte5[l1++ - i3] = (byte)((int[])a629.a394.elementAt(k))[0];
            for(int j1 = 1; j1 < 17; j1++)
            {
                int l2 = ((int[])a629.a394.elementAt(k))[j1];
                abyte5[l1++ - i3] = (byte)l2;
                k2 += l2;
            }

            int j3 = l1;
            byte abyte6[] = new byte[k2];
            for(int k1 = 0; k1 < k2; k1++)
                abyte6[l1++ - j3] = (byte)((int[])a629.a395.elementAt(k))[k1];

            byte abyte7[] = new byte[l1];
            System.arraycopy(abyte8, 0, abyte7, 0, i3);
            System.arraycopy(abyte5, 0, abyte7, i3, 17);
            System.arraycopy(abyte6, 0, abyte7, i3 + 17, k2);
            abyte8 = abyte7;
            i3 = l1;
        }

        abyte8[2] = (byte)(l1 - 2 >> 8 & 0xff);
        abyte8[3] = (byte)(l1 - 2 & 0xff);
        a625(abyte8, bufferedoutputstream);
        byte abyte9[] = new byte[14];
        abyte9[0] = -1;
        abyte9[1] = -38;
        abyte9[2] = 0;
        abyte9[3] = 12;
        abyte9[4] = (byte)a628.a324;
        l1 = 5;
        for(int l = 0; l < abyte9[4]; l++)
        {
            abyte9[l1++] = (byte)a628.a326[l];
            abyte9[l1++] = (byte)((a628.a330[l] << 4) + a628.a331[l]);
        }

        abyte9[l1++] = (byte)a628.a334;
        abyte9[l1++] = (byte)a628.a335;
        abyte9[l1++] = (byte)((a628.a336 << 4) + a628.a337);
        a625(abyte9, bufferedoutputstream);
    }

    void a624(byte abyte0[], BufferedOutputStream bufferedoutputstream)
    {
        try
        {
            bufferedoutputstream.write(abyte0, 0, 2);
            return;
        }
        catch(IOException ioexception)
        {
            System.out.println("IO Error: " + ioexception.getMessage());
        }
    }

    void a625(byte abyte0[], BufferedOutputStream bufferedoutputstream)
    {
        try
        {
            int i = ((abyte0[2] & 0xff) << 8) + (abyte0[3] & 0xff) + 2;
            bufferedoutputstream.write(abyte0, 0, i);
            return;
        }
        catch(IOException ioexception)
        {
            System.out.println("IO Error: " + ioexception.getMessage());
        }
    }

}
