// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a1;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package a0.a1:
//            a2, a827, a661, a37

final class a612 extends DataInputStream
{

    a827 a615;
    int a581;
    int a582;
    int a583;

    a612(InputStream inputstream, a827 a827_1)
    {
        super(inputstream);
        a615 = a827_1;
    }

    void a613()
    {
        a581 = 0;
    }

    final int a556(int i)
        throws IOException
    {
        int j = 0;
        for(int k = 0; k < i; k++)
            j = j << 1 | a557();

        return j;
    }

    final int a557()
        throws IOException
    {
        if(a581 == 0)
        {
            a582 = a614();
            a581 = 8;
            a583 = 128;
        }
        int i = a583 & a582;
        a583 >>= 1;
        a581--;
        return i >> a581;
    }

    final int a614()
        throws IOException
    {
        int i = readUnsignedByte();
        if(i == 255)
        {
            do
                i = readUnsignedByte();
            while(i == 255);
            if(i == 0)
                return 255;
            if(i < 208 || i > 215)
            {
                return 0;
            } else
            {
                a615.a853();
                return a614();
            }
        } else
        {
            return i;
        }
    }
}
