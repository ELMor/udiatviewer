// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a1;

import java.io.IOException;

// Referenced classes of package a0.a1:
//            a2, a827, a612, a37

final class a661
{

    final int a664 = 16;
    int a665;
    int a666[];
    int a667[];
    int a668;
    int a669[];
    int a670[];
    int a671[];
    int a672[];
    int a673[];

    void a662(int i)
    {
        a665 = i;
        a669 = new int[16];
        a670 = new int[16];
        a671 = new int[16];
        a672 = new int[i];
        a673 = new int[i + 1];
        int j = 0;
        int k = 0;
        for(; j < 16; j++)
        {
            for(int l = 0; l < a666[j]; l++)
            {
                a673[k] = j + 1;
                k++;
            }

            a673[k] = 0;
        }

        int i1 = 0;
        int j1 = 0;
        int k1 = a673[0];
        while(a673[j1] != 0) 
        {
            while(a673[j1] == k1) 
            {
                a672[j1] = i1;
                i1++;
                j1++;
            }
            k1++;
            i1 <<= 1;
        }
        int l1 = 0;
        int i2 = 0;
        for(; l1 < 16; l1++)
            if(a666[l1] != 0)
            {
                a671[l1] = i2;
                a670[l1] = a672[i2];
                i2 += a666[l1];
                a669[l1] = a672[i2 - 1];
            } else
            {
                a669[l1] = -1;
                a670[l1] = i + 1;
                a671[l1] = 0;
            }

        for(int j2 = 0; j2 < 16; j2++)
        {
            if(a666[j2] == 0)
                continue;
            a668 = j2 + 1;
            break;
        }

        a672 = null;
        a673 = null;
    }

    final int a663(a612 a612_1)
        throws IOException
    {
        int i = a612_1.a556(a668);
        int j;
        for(j = a668 - 1; i > a669[j] && j < 16; j++)
            i = i << 1 | a612_1.a557();

        if(j >= 16)
        {
            throw new IOException("Bad Huffman Code Length");
        } else
        {
            int k = i - a670[j];
            int l = a671[j] + k;
            return a667[l];
        }
    }

    a661()
    {
    }
}
