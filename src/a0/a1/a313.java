// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a1;

import java.awt.Image;
import java.awt.image.PixelGrabber;

// Referenced classes of package a0.a1:
//            a528, a368, a617

class a313
{

    String a317;
    public Image a318;
    public int a319;
    public int a320;
    public int a321[];
    public int a322[];
    public int a323;
    public int a324;
    public Object a325[];
    public int a326[] = {
        1, 2, 3
    };
    public int a327[] = {
        1, 1, 1
    };
    public int a328[] = {
        1, 1, 1
    };
    public int a329[] = {
        0, 1, 1
    };
    public int a330[] = {
        0, 1, 1
    };
    public int a331[] = {
        0, 1, 1
    };
    public boolean a332[];
    public boolean a333[];
    public int a334;
    public int a335;
    public int a336;
    public int a337;
    public int a338[];
    public int a339[];
    public int a340;
    public int a341;

    public a313(Image image)
    {
        a323 = 8;
        a324 = 3;
        a332 = new boolean[3];
        a333 = new boolean[3];
        a335 = 63;
        a325 = new Object[a324];
        a338 = new int[a324];
        a339 = new int[a324];
        a321 = new int[a324];
        a322 = new int[a324];
        a318 = image;
        a320 = image.getWidth(null);
        a319 = image.getHeight(null);
        a317 = "JPEG Encoder Copyright 1998, James R. Weeks and BioElectroMech.  ";
        a316();
    }

    public void a314(String s)
    {
        a317.concat(s);
    }

    public String a315()
    {
        return a317;
    }

    private void a316()
    {
        int ai[] = new int[a320 * a319];
        PixelGrabber pixelgrabber = new PixelGrabber(a318.getSource(), 0, 0, a320, a319, ai, 0, a320);
        a340 = 1;
        a341 = 1;
        for(int l = 0; l < a324; l++)
        {
            a340 = Math.max(a340, a327[l]);
            a341 = Math.max(a341, a328[l]);
        }

        for(int i1 = 0; i1 < a324; i1++)
        {
            a338[i1] = ((a320 % 8 == 0 ? a320 : (int)Math.ceil((double)a320 / 8D) * 8) / a340) * a327[i1];
            if(a338[i1] != (a320 / a340) * a327[i1])
                a332[i1] = true;
            a321[i1] = (int)Math.ceil((double)a338[i1] / 8D);
            a339[i1] = ((a319 % 8 == 0 ? a319 : (int)Math.ceil((double)a319 / 8D) * 8) / a341) * a328[i1];
            if(a339[i1] != (a319 / a341) * a328[i1])
                a333[i1] = true;
            a322[i1] = (int)Math.ceil((double)a339[i1] / 8D);
        }

        try
        {
            if(!pixelgrabber.grabPixels())
                try
                {
                    throw new Exception("Grabber returned false: " + pixelgrabber.status());
                }
                catch(Exception _ex) { }
        }
        catch(InterruptedException _ex) { }
        float af[][] = new float[a339[0]][a338[0]];
        float af1[][] = new float[a339[0]][a338[0]];
        float af2[][] = new float[a339[0]][a338[0]];
        int l1 = 0;
        for(int j1 = 0; j1 < a319; j1++)
        {
            for(int k1 = 0; k1 < a320; k1++)
            {
                int i = ai[l1] >> 16 & 0xff;
                int j = ai[l1] >> 8 & 0xff;
                int k = ai[l1] & 0xff;
                af[j1][k1] = (float)(0.29899999999999999D * (double)(float)i + 0.58699999999999997D * (double)(float)j + 0.114D * (double)(float)k);
                af2[j1][k1] = 128F + (float)((-0.16874D * (double)(float)i - 0.33126D * (double)(float)j) + 0.5D * (double)(float)k);
                af1[j1][k1] = 128F + (float)(0.5D * (double)(float)i - 0.41869000000000001D * (double)(float)j - 0.081309999999999993D * (double)(float)k);
                l1++;
            }

        }

        a325[0] = af;
        a325[1] = af2;
        a325[2] = af1;
    }
}
