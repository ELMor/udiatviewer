// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a1;

import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package a0.a1:
//            a2, a661, a612, a37

public class a827
{

    final int a859 = 512;
    final int a860 = 4;
    final int a861 = 7;
    final int a862 = 4;
    final int a863 = 4;
    final int a664 = 16;
    final int a864 = 256;
    final int a865 = 4096;
    final int a866 = 0;
    final int a867 = 255;
    final int a868 = 4095;
    final int a10 = 8;
    final int a11 = 64;
    byte a869[];
    char a870[];
    boolean a871;
    boolean a872;
    boolean a837;
    int a873;
    int a874[];
    int a875;
    int a876;
    int a877;
    int a878;
    int a879;
    int a880;
    int a881;
    int a882;
    int a665;
    a2 a421[];
    int a883[][];
    int a884;
    int a885;
    int a886;
    int a887;
    a661 a888[];
    a661 a889[];
    int a890;
    int a891;
    int a892;
    int a893;
    int a894;
    int a895;
    int a896;
    int a897;

    public void a828(byte abyte0[])
    {
        a869 = abyte0;
    }

    public void a829(char ac[])
    {
        a870 = ac;
    }

    public void a830(int i)
    {
        a897 = i;
    }

    public void a416(DataInputStream datainputstream)
    {
        a612 a612_1 = new a612(datainputstream, this);
        if(a839(a612_1))
            while(!a871) 
                if(!a840(a612_1))
                    return;
    }

    public byte[] a831()
    {
        return a869;
    }

    public char[] a832()
    {
        return a870;
    }

    public int a833()
    {
        return a885;
    }

    public int a834()
    {
        return a884;
    }

    public int a835()
    {
        return a887;
    }

    public int a836()
    {
        return a886;
    }

    public boolean a837()
    {
        return a837;
    }

    int a838(a612 a612_1)
        throws IOException
    {
        int i;
        do
        {
            do
                i = a612_1.readUnsignedByte();
            while(i != 255);
            do
                i = a612_1.readUnsignedByte();
            while(i == 255);
        } while(i == 0);
        return i;
    }

    boolean a839(a612 a612_1)
    {
        try
        {
            if(a838(a612_1) != 216)
                return false;
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception: Image File Format not supported");
            System.err.println(ioexception);
            return false;
        }
        return a840(a612_1);
    }

    boolean a840(a612 a612_1)
    {
        try
        {
            while(true) 
            {
                int i = a838(a612_1);
                switch(i)
                {
                case 192: 
                case 193: 
                case 194: 
                case 195: 
                case 197: 
                case 198: 
                case 199: 
                case 201: 
                case 202: 
                case 203: 
                case 205: 
                case 206: 
                case 207: 
                    a873 = i;
                    a843(a612_1);
                    break;

                case 218: 
                    a845(a612_1);
                    return true;

                case 217: 
                    a871 = true;
                    return true;

                case 204: 
                    a841(a612_1);
                    break;

                case 196: 
                    a844(a612_1);
                    break;

                case 219: 
                    a842(a612_1);
                    break;

                case 221: 
                    a895 = a846(a612_1);
                    break;

                case 224: 
                case 225: 
                case 226: 
                case 227: 
                case 228: 
                case 229: 
                case 230: 
                case 231: 
                case 232: 
                case 233: 
                case 234: 
                case 235: 
                case 236: 
                case 237: 
                case 238: 
                case 239: 
                    a841(a612_1);
                    break;

                case 254: 
                    a841(a612_1);
                    break;

                case 220: 
                    a841(a612_1);
                    break;

                case 222: 
                    a841(a612_1);
                    break;

                case 223: 
                    a841(a612_1);
                    break;

                case 191: 
                case 200: 
                case 240: 
                case 241: 
                case 242: 
                case 243: 
                case 244: 
                case 245: 
                case 246: 
                case 247: 
                case 248: 
                case 249: 
                case 250: 
                case 251: 
                case 252: 
                case 253: 
                    a841(a612_1);
                    break;

                default:
                    a841(a612_1);
                    break;

                case 1: // '\001'
                case 208: 
                case 209: 
                case 210: 
                case 211: 
                case 212: 
                case 213: 
                case 214: 
                case 215: 
                case 216: 
                    break;
                }
            }
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception: Image File Format not supported");
            System.err.println(ioexception);
            return false;
        }
    }

    void a841(a612 a612_1)
        throws IOException
    {
        int i = a612_1.readUnsignedShort();
        if(i < 2)
        {
            throw new IOException("Erroneous JPEG marker length in variable length marker");
        } else
        {
            i -= 2;
            a612_1.skipBytes(i);
            return;
        }
    }

    void a842(a612 a612_1)
        throws IOException
    {
        int i = a612_1.readUnsignedShort();
        if(i < 2)
            throw new IOException("Erroneous JPEG marker length on DQT marker");
        for(i -= 2; i > 0;)
        {
            int j = a612_1.readUnsignedByte();
            int k = j >> 4;
            int l = j & 0xf;
            if(l >= 4)
                throw new IOException("Quantization Table Index Too Large");
            if(k == 0)
            {
                for(int i1 = 0; i1 < 64; i1++)
                    a883[l][i1] = a612_1.readUnsignedByte();

            } else
            {
                for(int j1 = 0; j1 < 64; j1++)
                    a883[l][j1] = a612_1.readUnsignedShort();

            }
            i -= 65;
            if(k == 1)
                i -= 64;
        }

        if(i != 0)
            throw new IOException("Bad marker length on DQT");
        else
            return;
    }

    void a843(a612 a612_1)
        throws IOException
    {
        int i = a612_1.readUnsignedShort();
        if(i < 2)
            throw new IOException("Erroneous JPEG marker length in SOFn marker");
        a887 = a612_1.readUnsignedByte();
        a884 = a612_1.readUnsignedShort();
        a885 = a612_1.readUnsignedShort();
        a886 = a612_1.readUnsignedByte();
        if(i != 3 * a886 + 8)
            throw new IOException("Bogus SOF marker length");
        a880 = 1;
        a881 = 1;
        a875 = 7;
        for(int j = 0; j < a886; j++)
        {
            int k = a612_1.readUnsignedByte();
            if(k < a875)
                a875 = k;
            a421[j] = new a2();
            a421[j].a20 = k;
            int l = a612_1.readUnsignedByte();
            int i1 = (byte)(l & 0xf);
            if(i1 > a881)
                a881 = i1;
            int j1 = (byte)(l >> 4 & 0xf);
            if(j1 > a880)
                a880 = j1;
            a421[j].a22 = i1;
            a421[j].a21 = j1;
            a421[j].a23 = a612_1.readUnsignedByte();
        }

        a847();
        a848();
        a872 = true;
    }

    void a844(a612 a612_1)
        throws IOException
    {
        int i = a612_1.readUnsignedShort();
        if(i < 2)
            throw new IOException("Erroneous JPEG marker length in DHT marker");
        for(i -= 2; i > 16;)
        {
            int j = a612_1.readUnsignedByte();
            i--;
            int ai[] = new int[16];
            int k = 0;
            for(int l = 0; l < 16; l++)
            {
                ai[l] = a612_1.readUnsignedByte();
                k += ai[l];
            }

            i -= 16;
            if(k > a665 || k > i)
                throw new IOException("Bad Huffman table in DHT marker");
            int ai1[] = new int[a665];
            for(int i1 = 0; i1 < k; i1++)
                ai1[i1] = a612_1.readUnsignedByte();

            i -= k;
            int j1 = j & 0x10;
            int k1 = j & 0xf;
            if(k1 >= 4)
                throw new IOException("Error on DHT identifier");
            if(j1 == 0)
            {
                a888[k1] = new a661();
                a888[k1].a666 = ai;
                a888[k1].a667 = ai1;
            } else
            {
                a889[k1] = new a661();
                a889[k1].a666 = ai;
                a889[k1].a667 = ai1;
            }
        }

        if(i != 0)
            throw new IOException("Bad marker length on DHT");
        else
            return;
    }

    void a845(a612 a612_1)
        throws IOException
    {
        if(!a872)
            throw new IOException("Scan found before frame defined");
        int i = a612_1.readUnsignedShort();
        a890 = a612_1.readUnsignedByte();
        if(i != 2 * a890 + 6 || a890 < 1 || a890 > 4)
            throw new IOException("Erroneous JPEG marker codification on SOS marker");
        for(int j = 0; j < a890; j++)
        {
            int k = a612_1.readUnsignedByte();
            int i1 = a612_1.readUnsignedByte();
            int j1 = i1 & 0xf;
            int k1 = i1 >> 4 & 0xf;
            int l1 = -1;
            for(int i2 = 0; i2 < a886; i2++)
                if(a421[i2].a20 == k)
                    l1 = i2;

            if(l1 == -1)
                throw new IOException("Component Not Defined");
            a874[j] = l1;
            a421[l1].a26 = a888[k1];
            a421[l1].a27 = a889[j1];
        }

        a891 = a612_1.readUnsignedByte();
        a892 = a612_1.readUnsignedByte();
        int l = a612_1.readUnsignedByte();
        a893 = l >> 4 & 0xf;
        a894 = l & 0xf;
        switch(a873)
        {
        case 192: 
            a551(a612_1, true);
            return;

        case 193: 
            a551(a612_1, true);
            return;

        case 195: 
            a551(a612_1, false);
            return;

        case 194: 
        case 196: 
        case 197: 
        case 198: 
        case 199: 
        case 200: 
        case 201: 
        case 202: 
        case 203: 
        case 204: 
        case 205: 
        case 206: 
        case 207: 
        default:
            return;
        }
    }

    int a846(a612 a612_1)
        throws IOException
    {
        int i = a612_1.readUnsignedShort();
        if(i != 4)
            throw new IOException("Erroneous JPEG marker length on DRI marker");
        else
            return a612_1.readUnsignedShort();
    }

    void a847()
    {
        if(a887 > 8)
        {
            a882 = 4095;
            a665 = 4096;
            return;
        } else
        {
            a882 = 255;
            a665 = 256;
            return;
        }
    }

    void a848()
    {
        a878 = a881 * 8;
        a879 = a880 * 8;
        a876 = ((a884 + a878) - 1) / a878;
        a877 = ((a885 + a879) - 1) / a879;
    }

    void a551(a612 a612_1, boolean flag)
        throws IOException
    {
        a837 = flag;
        for(int i = 0; i < a890; i++)
        {
            a421[a874[i]].a3(this);
            if(a421[a874[i]].a20 > a890)
            {
                a421[a874[i]].a20 = a874[i] + 1;
                for(int j = 0; j < a890; j++)
                    if(a421[a874[j]].a20 < a875)
                        a875 = a421[a874[j]].a20;

            }
        }

        a896 = 0;
        a853();
        a612_1.a581 = 0;
        if(a887 > 8)
        {
            if(a870 == null)
                a870 = new char[a886 * a885 * a884];
        } else
        if(a869 == null)
            a869 = new byte[a886 * a885 * a884];
        if(flag)
        {
            if(a849())
            {
                a850(a612_1);
                return;
            } else
            {
                a851(a612_1);
                return;
            }
        } else
        {
            a855(a612_1);
            return;
        }
    }

    boolean a849()
    {
        return a890 != 1;
    }

    void a850(a612 a612_1)
        throws IOException
    {
        int i = 0;
        for(int j = 0; j < a876; j++)
        {
            for(int k = 0; k < a877; k++)
            {
                if(a895 != 0)
                {
                    if(a895 == i)
                    {
                        a854(a612_1);
                        i = 0;
                    }
                    i++;
                }
                for(int l = 0; l < a890; l++)
                {
                    int i1 = a874[l];
                    int j1 = a421[i1].a22;
                    for(int k1 = 0; k1 < j1; k1++)
                    {
                        int l1 = j * a421[i1].a22 + k1;
                        int i2 = a421[i1].a21;
                        for(int j2 = 0; j2 < i2; j2++)
                        {
                            int k2 = k * a421[i1].a21 + j2;
                            a421[i1].a5(a612_1);
                            a852(l1, k2, i1);
                        }

                    }

                }

            }

        }

    }

    void a851(a612 a612_1)
        throws IOException
    {
        int i = 0;
        int j = a874[0];
        int k = a421[j].a35;
        int l = a421[j].a36;
        for(int i1 = 0; i1 < k; i1++)
        {
            for(int j1 = 0; j1 < l; j1++)
            {
                if(a895 != 0)
                {
                    if(a895 == i)
                    {
                        a854(a612_1);
                        i = 0;
                    }
                    i++;
                }
                a421[j].a5(a612_1);
                a852(i1, j1, j);
            }

        }

    }

    final void a852(int i, int j, int k)
    {
        int l = a421[k].a33;
        int i1 = a421[k].a34;
        int j1 = 8 * l;
        int k1 = 8 * i1;
        int l1 = a421[k].a20 - a875;
        l1 *= a885;
        l1 *= a884;
        l1 += i * j1 * a885;
        l1 += j * k1;
        l1 += a897;
        int i2 = j1 * (i + 1) - a884;
        if(i2 > 0)
            j1 -= i2;
        int j2 = k1 * (j + 1) - a885;
        if(j2 > 0)
            k1 -= j2;
        if(a887 > 8)
        {
            for(int k2 = 0; k2 < j1; k2++)
            {
                int i3 = l1 + k2 * a885;
                int k3 = k2 / l;
                for(int i4 = 0; i4 < k1; i4++)
                {
                    int k4 = i4 / i1;
                    int i5 = a421[k].a31[k3][k4];
                    a870[i3++] = (char)(i5 >= 0 ? i5 <= 4095 ? i5 : 4095 : '\0');
                }

            }

            return;
        }
        for(int l2 = 0; l2 < j1; l2++)
        {
            int j3 = l1 + l2 * a885;
            int l3 = l2 / l;
            for(int j4 = 0; j4 < k1; j4++)
            {
                int l4 = j4 / i1;
                int j5 = a421[k].a31[l3][l4];
                a869[j3++] = (byte)(j5 >= 0 ? j5 <= 255 ? j5 : 255 : '\0');
            }

        }

    }

    void a853()
    {
        for(int i = 0; i < a890; i++)
            a421[a874[i]].a28 = 0;

    }

    void a854(a612 a612_1)
        throws IOException
    {
        a612_1.mark(512);
        int i = a612_1.readUnsignedByte();
        if(i != 255)
        {
            a612_1.reset();
            return;
        }
        for(; i == 255; i = a612_1.readUnsignedByte());
        if(i < 208 || i > 215)
        {
            a612_1.reset();
            return;
        } else
        {
            a896++;
            a896 %= 8;
            a853();
            a612_1.a581 = 0;
            return;
        }
    }

    void a855(a612 a612_1)
        throws IOException
    {
        int ai[][] = new int[a885][a890];
        int ai1[][] = new int[a885][a890];
        int i = a895 / a885;
        a856(ai, a612_1);
        a858(ai, 0);
        int ai2[][] = ai1;
        ai1 = ai;
        ai = ai2;
        for(int j = 1; j < a884; j++)
        {
            if(a895 != 0)
            {
                if(i == 0)
                {
                    a854(a612_1);
                    a856(ai, a612_1);
                    a858(ai, j);
                    int ai3[][] = ai1;
                    ai1 = ai;
                    ai = ai3;
                    i = a895 / a885;
                    continue;
                }
                i--;
            }
            for(int k = 0; k < a890; k++)
            {
                int l = a421[a874[k]].a4(a612_1);
                ai[0][k] = l + ai1[0][k];
            }

            for(int i1 = 1; i1 < a885; i1++)
            {
                for(int j1 = 0; j1 < a890; j1++)
                {
                    int k1 = a421[a874[j1]].a4(a612_1);
                    int l1 = a857(i1, j1, ai, ai1);
                    ai[i1][j1] = k1 + l1;
                }

            }

            a858(ai, j);
            int ai4[][] = ai1;
            ai1 = ai;
            ai = ai4;
        }

    }

    final void a856(int ai[][], a612 a612_1)
        throws IOException
    {
        int i = 1 << a887 - a894 - 1;
        for(int j = 0; j < a890; j++)
        {
            int k = a421[a874[j]].a4(a612_1);
            ai[0][j] = k + i;
        }

        for(int l = 1; l < a885; l++)
        {
            for(int i1 = 0; i1 < a890; i1++)
            {
                int j1 = a421[a874[i1]].a4(a612_1);
                ai[l][i1] = j1 + ai[l - 1][i1];
            }

        }

    }

    final int a857(int i, int j, int ai[][], int ai1[][])
    {
        int k = i - 1;
        int l = ai1[i][j];
        int i1 = ai[k][j];
        int j1 = ai1[k][j];
        switch(a891)
        {
        case 1: // '\001'
            return i1;

        case 2: // '\002'
            return l;

        case 3: // '\003'
            return j1;

        case 4: // '\004'
            return (i1 + l) - j1;

        case 5: // '\005'
            return i1 + (l - j1 >> 1);

        case 6: // '\006'
            return l + (i1 - j1 >> 1);

        case 7: // '\007'
            return i1 + l >> 1;
        }
        return 0;
    }

    final void a858(int ai[][], int i)
    {
        int j = i * a885 * a890 + a897;
        if(a887 > 8)
        {
            for(int k = 0; k < a885; k++)
            {
                for(int i1 = 0; i1 < a890; i1++)
                    a870[j++] = (char)(ai[k][i1] << a894);

            }

            return;
        }
        for(int l = 0; l < a885; l++)
        {
            for(int j1 = 0; j1 < a890; j1++)
                a869[j++] = (byte)(ai[l][j1] << a894);

        }

    }

    public a827()
    {
        a871 = false;
        a872 = false;
        a837 = false;
        a874 = new int[4];
        a882 = 255;
        a665 = 256;
        a421 = new a2[7];
        a883 = new int[4][64];
        a888 = new a661[4];
        a889 = new a661[4];
    }
}
