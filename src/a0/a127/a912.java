// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.Component;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

// Referenced classes of package a0.a127:
//            a546, a584, a128, a898, 
//            a293, a683, a918, a964

public class a912
{

    static byte a917[] = null;

    public static a964 a136(URL url, String s, Component component, a918 a918)
    {
        if(url == null)
            return a136(s, component, a918);
        URL url1 = null;
        try
        {
            url1 = new URL(url, s);
        }
        catch(MalformedURLException _ex)
        {
            System.err.println("MalformedURLException");
        }
        return a136(url1, component, a918);
    }

    static a964 a136(String s, Component component, a918 a918)
    {
        try
        {
            BufferedInputStream bufferedinputstream = new BufferedInputStream(new FileInputStream(s));
            a416(bufferedinputstream);
            if(a916() || a917[0] == -1 && a917[1] == -40 && a917[2] == -1)
            {
                bufferedinputstream.close();
                return a914(s, component, a918);
            } else
            {
                return a913(bufferedinputstream, component, a918);
            }
        }
        catch(IOException ioexception)
        {
            System.err.println(ioexception);
        }
        return null;
    }

    static a964 a136(URL url, Component component, a918 a918)
    {
        try
        {
            BufferedInputStream bufferedinputstream = new BufferedInputStream(url.openStream());
            a416(bufferedinputstream);
            if(a916() || a917[0] == -1 && a917[1] == -40 && a917[2] == -1)
            {
                bufferedinputstream.close();
                return a914(url, component, a918);
            } else
            {
                return a913(bufferedinputstream, component, a918);
            }
        }
        catch(IOException ioexception)
        {
            System.err.println(ioexception);
        }
        return null;
    }

    static a964 a913(BufferedInputStream bufferedinputstream, Component component, a918 a918)
    {
        Object obj = null;
        a546 a546_1 = new a546(bufferedinputstream, component, a918);
        if(a546_1.a559.startsWith("PALETTE COLOR"))
        {
            if(a546_1.a567 != -1)
                obj = new a898(a546_1);
        } else
        if(a546_1.a559.startsWith("RGB"))
            obj = new a293(a546_1);
        else
        if(a546_1.a559.startsWith("ARGB"))
            obj = new a293(a546_1);
        else
        if(a546_1.a559.startsWith("YBR_FULL"))
            obj = new a293(a546_1);
        else
        if(a546_1.a394 == 16)
            obj = new a584(a546_1);
        else
        if(a546_1.a394 == 8)
        {
            obj = new a128(a546_1);
        } else
        {
            a546_1.a555();
            if(a546_1.a394 > 8)
                obj = new a584(a546_1);
            else
                obj = new a128(a546_1);
        }
        return ((a964) (obj));
    }

    static a964 a914(URL url, Component component, a918 a918)
    {
        java.awt.Image image = component.getToolkit().getImage(url);
        return new a683(image, component, a918);
    }

    static a964 a914(String s, Component component, a918 a918)
    {
        java.awt.Image image = component.getToolkit().getImage(s);
        return new a683(image, component, a918);
    }

    static void a416(BufferedInputStream bufferedinputstream)
    {
        a917 = new byte[4];
        try
        {
            bufferedinputstream.mark(4);
            if(bufferedinputstream.read(a917) != 4)
                System.err.println("I/O error ...");
            bufferedinputstream.reset();
            return;
        }
        catch(IOException ioexception)
        {
            System.err.println(ioexception);
        }
    }

    static boolean a915()
    {
        return a917[0] == -1 && a917[1] == -40 && a917[2] == -1;
    }

    static boolean a916()
    {
        String s = new String(a917);
        return s.startsWith("GIF");
    }

    public a912()
    {
    }

}
