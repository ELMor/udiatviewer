// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

// Referenced classes of package a0.a127:
//            a632, a918

abstract class a937
{

    static final int a859 = 512;
    boolean a957;
    boolean a655;
    byte a958[];
    DataInputStream a959;
    String a960;

    abstract void a460();

    void a416()
    {
        a655 = true;
        try
        {
            a945();
            a957 = a946();
            if(a951(2))
            {
                if(a952(2, 16))
                    a960 = new String(a958);
                else
                    a960 = "No Transfer Syntax";
            } else
            {
                a960 = "No Transfer Syntax. Group 0x0002 not found";
            }
            if(a951(8))
                a655 = true;
            else
                a655 = false;
            if(!a951(8))
            {
                System.err.println("Group 0x0008 not found");
                a959.close();
                return;
            } else
            {
                a957 = a946();
                a460();
                return;
            }
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception: DICOM File Format not supported");
            System.err.println(ioexception);
            return;
        }
    }

    String a938(String s)
    {
        int i = s.indexOf("\\");
        String s1;
        if(i > 0)
            s1 = s.substring(0, i);
        else
            s1 = s;
        return s1.trim();
    }

    int a939()
    {
        String s = a938(new String(a958));
        try
        {
            Float float1 = new Float(s);
            return float1.intValue();
        }
        catch(NumberFormatException numberformatexception)
        {
            System.err.println("We were trying to transform " + s);
            System.err.println(numberformatexception);
            return 0;
        }
    }

    float a940()
    {
        String s = a938(new String(a958));
        try
        {
            Float float1 = new Float(s);
            return float1.floatValue();
        }
        catch(NumberFormatException numberformatexception)
        {
            System.err.println("We were trying to transform " + s);
            System.err.println(numberformatexception);
            return 0.0F;
        }
    }

    float a941()
    {
        String s = new String(a958);
        int i = s.indexOf("\\");
        String s1;
        if(i > 0)
            s1 = s.substring(i + 1);
        else
            return 0.0F;
        String s2 = a938(s1);
        try
        {
            Float float1 = new Float(s2);
            return float1.floatValue();
        }
        catch(NumberFormatException numberformatexception)
        {
            System.err.println("We were trying to transform " + s2);
            System.err.println(numberformatexception);
            return 0.0F;
        }
    }

    final int a942(int i)
    {
        if(a655)
            return i << 8 & 0xff00 | i >> 8 & 0xff;
        else
            return i & 0xffff;
    }

    final int a943(int i)
    {
        int j = a655 ? i << 8 & 0xff00 | i >> 8 & 0xff : i & 0xffff;
        j <<= 16;
        int k = i >> 16;
        k = a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
        if(a655)
            return j | k;
        else
            return i;
    }

    void a944(a918 a918_1, int i, int j)
        throws IOException
    {
        int k = -1;
        int l = a918_1.a345.size();
        for(int i1 = 0; i1 < l; i1++)
        {
            a632 a632_1 = (a632)a918_1.a345.elementAt(i1);
            int j1 = a655;
            a632_1.a655 = j1;
            j1 = a632_1.a646();
            j1 = j1 >> 16 & 0xffff;
            if(j1 > i && j1 < j)
            {
                if(j1 != k)
                {
                    if(k > 0 && !a955(k))
                    {
                        System.err.println("Error on group " + Integer.toHexString(k));
                        a959.close();
                        return;
                    }
                    k = j1;
                    if(!a951(j1))
                    {
                        System.err.println("Error group " + Integer.toHexString(j1) + " not found");
                        return;
                    }
                }
                int k1 = a632_1.a646();
                k1 &= 0xffff;
                if(a952(j1, k1))
                {
                    byte abyte0[] = a958;
                    a632_1.a589 = abyte0;
                }
            }
        }

    }

    void a945()
        throws IOException
    {
        a959.mark(512);
        byte abyte0[] = new byte[128];
        a959.readFully(abyte0);
        abyte0 = new byte[4];
        a959.readFully(abyte0);
        String s = new String(abyte0);
        if(!s.equals("DICM"))
            a959.reset();
    }

    boolean a946()
        throws IOException
    {
        a959.mark(512);
        int i = a959.readShort();
        i = a655 ? i << 8 & 0xff00 | i >> 8 & 0xff : i & 0xffff;
        int j = a959.readShort();
        j = a655 ? j << 8 & 0xff00 | j >> 8 & 0xff : j & 0xffff;
        byte abyte0[] = new byte[2];
        a959.readFully(abyte0);
        a959.reset();
        if(a949(new String(abyte0)))
            return true;
        a959.mark(512);
        int k = a959.readShort();
        i = a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
        k = a959.readShort();
        j = a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
        a959.readFully(abyte0);
        k = a959.readShort();
        k = a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
        if(k < 0)
        {
            a959.reset();
            return true;
        }
        if(k > 504)
        {
            a959.reset();
            return true;
        }
        a958 = new byte[k];
        a959.readFully(a958);
        int l = a959.readShort();
        l = a655 ? l << 8 & 0xff00 | l >> 8 & 0xff : l & 0xffff;
        a959.reset();
        if(i == 2 && l == 2)
            return false;
        if(i == 2 && l == 8)
            return false;
        return i != 8 || l != 8;
    }

    boolean a947(int i, int j)
        throws IOException
    {
        if(a952(i, j))
            return a958.length != 0;
        else
            return false;
    }

    boolean a948(String s)
    {
        if(s.equals("OB"))
            return false;
        if(s.equals("OW"))
            return false;
        if(s.equals("SQ"))
            return false;
        if(s.equals("UN"))
            return false;
        return !s.equals("UT");
    }

    boolean a949(String s)
    {
        if(s.equals("AE"))
            return false;
        if(s.equals("AS"))
            return false;
        if(s.equals("AT"))
            return false;
        if(s.equals("CS"))
            return false;
        if(s.equals("DA"))
            return false;
        if(s.equals("DS"))
            return false;
        if(s.equals("DT"))
            return false;
        if(s.equals("FL"))
            return false;
        if(s.equals("FD"))
            return false;
        if(s.equals("IS"))
            return false;
        if(s.equals("LO"))
            return false;
        if(s.equals("LT"))
            return false;
        if(s.equals("OB"))
            return false;
        if(s.equals("OW"))
            return false;
        if(s.equals("PN"))
            return false;
        if(s.equals("SH"))
            return false;
        if(s.equals("SL"))
            return false;
        if(s.equals("SQ"))
            return false;
        if(s.equals("SS"))
            return false;
        if(s.equals("ST"))
            return false;
        if(s.equals("TM"))
            return false;
        if(s.equals("UI"))
            return false;
        if(s.equals("UL"))
            return false;
        if(s.equals("US"))
            return false;
        if(s.equals("UN"))
            return false;
        return !s.equals("UT");
    }

    int a950()
        throws IOException
    {
        if(a957)
            return a943(a959.readInt());
        byte abyte0[] = new byte[4];
        a959.readFully(abyte0);
        if(a948(new String(abyte0, 0, 2)))
        {
            int i = abyte0[3] & 0xff;
            int j = abyte0[2] & 0xff;
            if(a655)
                return i << 8 | j;
            else
                return j << 8 | i;
        } else
        {
            return a943(a959.readInt());
        }
    }

    boolean a951(int i)
        throws IOException
    {
        int j = 0;
        a959.mark(512);
        short word0 = a959.readShort();
        for(j = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff; j < i; j = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff)
        {
            word0 = a959.readShort();
            int k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            int l = a950();
            if(l == -1)
                a956();
            else
            if(l != a959.skipBytes(l))
                return false;
            a959.mark(512);
            word0 = a959.readShort();
        }

        a959.reset();
        return j == i;
    }

    boolean a952(int i, int j)
        throws IOException
    {
        a959.mark(512);
        short word0 = a959.readShort();
        int k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
        word0 = a959.readShort();
        int l;
        for(l = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff; l < j && k == i; l = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff)
        {
            int i1 = a950();
            if(i1 == -1)
                a956();
            else
            if(i1 != a959.skipBytes(i1))
                return false;
            a959.mark(512);
            word0 = a959.readShort();
            k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            word0 = a959.readShort();
        }

        if(l == j && k == i)
        {
            int j1 = a950();
            if(j1 == -1)
            {
                a958 = null;
            } else
            {
                a958 = new byte[j1];
                a959.readFully(a958);
            }
            return true;
        } else
        {
            a959.reset();
            return false;
        }
    }

    int a953(int i, int j)
        throws IOException
    {
        a959.mark(512);
        short word0 = a959.readShort();
        int k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
        word0 = a959.readShort();
        int l;
        for(l = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff; l < j && k == i; l = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff)
        {
            int i1 = a950();
            if(i1 == -1)
                a956();
            else
            if(i1 != a959.skipBytes(i1))
                return -1;
            a959.mark(512);
            word0 = a959.readShort();
            k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            word0 = a959.readShort();
        }

        if(j == l && i == k)
        {
            int j1 = a950();
            if(j1 == 2)
            {
                short word1 = a959.readShort();
                if(a655)
                    return word1 << 8 & 0xff00 | word1 >> 8 & 0xff;
                else
                    return word1 & 0xffff;
            }
            if(j1 == 4)
            {
                return a943(a959.readInt());
            } else
            {
                a959.skip(j1);
                return -1;
            }
        } else
        {
            a959.reset();
            return -1;
        }
    }

    int a954(int i, int j)
        throws IOException
    {
        a959.mark(512);
        short word0 = a959.readShort();
        int k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
        word0 = a959.readShort();
        int l;
        for(l = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff; l < j && k == i; l = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff)
        {
            int i1 = a950();
            if(i1 == -1)
                a956();
            else
            if(i1 != a959.skipBytes(i1))
                return -1;
            a959.mark(512);
            word0 = a959.readShort();
            k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            word0 = a959.readShort();
        }

        if(j == l && i == k)
        {
            int j1 = a950();
            int k1 = a959.readShort();
            k1 = a655 ? k1 << 8 & 0xff00 | k1 >> 8 & 0xff : k1 & 0xffff;
            a959.skip(j1 - 2);
            return k1;
        } else
        {
            a959.reset();
            return -1;
        }
    }

    boolean a955(int i)
        throws IOException
    {
        a959.mark(512);
        short word0 = a959.readShort();
        for(int j = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff; i == j; j = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff)
        {
            word0 = a959.readShort();
            int k = a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            int l = a950();
            if(l == -1)
                a956();
            else
            if(l != a959.skipBytes(l))
                return false;
            a959.mark(512);
            word0 = a959.readShort();
        }

        a959.reset();
        return true;
    }

    void a956()
        throws IOException
    {
        do
        {
            int i = a959.readShort();
            i = a655 ? i << 8 & 0xff00 | i >> 8 & 0xff : i & 0xffff;
            int j = a959.readShort();
            j = a655 ? j << 8 & 0xff00 | j >> 8 & 0xff : j & 0xffff;
            int k;
            if(i == 65534)
                k = a943(a959.readInt());
            else
                k = a950();
            if(i == 65534 && j - 57344 > 0 && k == 0)
                return;
            if(k == -1)
                a956();
            else
                a959.skipBytes(k);
        } while(true);
    }

    a937()
    {
    }
}
