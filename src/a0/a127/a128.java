// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;

// Referenced classes of package a0.a127:
//            a133, a546, a964

public class a128 extends a133
{

    byte a132[];

    public a128(a546 a546_1)
    {
        a3(a546_1);
        super.a165 = super.a161.a559.startsWith("MONOCHROME1");
        if(super.a165 && super.a161.a572.startsWith("OD"))
        {
            super.a161.a563 = (a140() + a141()) / 2;
            super.a161.a562 = a140() - a141();
            super.a966 = super.a161.a563;
            super.a967 = super.a161.a562;
        }
    }

    void a129()
    {
        int i = super.a966 - super.a161.a564;
        int j = i - super.a967 / 2;
        int k = i + super.a967 / 2;
        if(a132 == null)
            a132 = new byte[768];
        int i1 = 0;
        int j1 = 0;
        for(; i1 < 256; i1++)
        {
            int l = i1 >= j ? i1 <= k ? (255 * (i1 - j)) / super.a967 : 255 : 0;
            a132[j1] = (byte)l;
            j1++;
            a132[j1] = (byte)l;
            j1++;
            a132[j1] = (byte)l;
            j1++;
        }

        if(super.a165)
        {
            for(int k1 = 0; k1 < 768; k1++)
                a132[k1] = (byte)(a132[k1] ^ 0xff);

        }
        int l1 = super.a161.a568;
        for(int i2 = 0; i2 < l1; i2++)
            super.a166[i2] = null;

    }

    ImageProducer a130(int i)
    {
        a159(i);
        if(super.a162 == null)
            a131();
        IndexColorModel indexcolormodel = new IndexColorModel(8, 256, a132, 0, false);
        return new MemoryImageSource(super.a161.a560, super.a161.a561, indexcolormodel, super.a162[i], 0, super.a161.a560);
    }

    void a131()
    {
        int i = super.a161.a568;
        super.a162 = new byte[i][];
        if(i > 1)
        {
            int j = super.a161.a560 * super.a161.a561;
            byte abyte0[] = super.a161.a547();
            int k = 0;
            int l = 0;
            for(; k < i; k++)
            {
                super.a162[k] = new byte[j];
                for(int i1 = 0; i1 < j;)
                {
                    super.a162[k][i1] = abyte0[l];
                    i1++;
                    l++;
                }

            }

        } else
        {
            super.a162[0] = super.a161.a547();
        }
        super.a161.a548();
        a129();
    }
}
