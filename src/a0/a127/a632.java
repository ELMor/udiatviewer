// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.io.PrintStream;

final class a632
{

    int a650;
    String a651;
    String a652;
    String a467;
    String a653;
    String a654;
    byte a589[];
    boolean a655;

    public static final int a633(int i, int j)
    {
        return i << 16 | j & 0xffff;
    }

    public static final int a634(int i)
    {
        return i >> 16 & 0xffff;
    }

    public static final int a635(int i)
    {
        return i & 0xffff;
    }

    String a636(int i)
    {
        if(i > 4095)
            return Integer.toHexString(i);
        if(i > 255)
            return "0" + Integer.toHexString(i);
        if(i > 15)
            return "00" + Integer.toHexString(i);
        else
            return "000" + Integer.toHexString(i);
    }

    String a637(int i, int j)
    {
        return a636(i) + a636(j);
    }

    int a638(int i)
    {
        int j;
        int k;
        if(a655)
        {
            j = a589[i];
            j &= 0xff;
            k = a589[i + 1];
            k <<= 8;
        } else
        {
            j = a589[i + 1];
            j &= 0xff;
            k = a589[i];
            k <<= 8;
        }
        return j | k;
    }

    int a639(int i, int j)
    {
        int k = 0;
        if(a655)
        {
            for(int j1 = 0; j1 < j; j1++)
            {
                int l = a589[i + j1];
                l &= 0xff;
                l <<= j1 * 8;
                k |= l;
            }

        } else
        {
            for(int k1 = 0; k1 < j; k1++)
            {
                int i1 = a589[(i + j) - 1 - k1];
                i1 &= 0xff;
                i1 <<= k1 * 8;
                k |= i1;
            }

        }
        return k;
    }

    long a640(int i, int j)
    {
        long l = 0L;
        if(a655)
        {
            for(int k = 0; k < j; k++)
            {
                long l1 = a589[i + k];
                l1 &= 255L;
                l1 <<= k * 8;
                l |= l1;
            }

        } else
        {
            for(int i1 = 0; i1 < j; i1++)
            {
                long l2 = a589[(i + j) - 1 - i1];
                l2 &= 255L;
                l2 <<= i1 * 8;
                l |= l2;
            }

        }
        return l;
    }

    String a641()
    {
        if(a652.equals("AT"))
        {
            int i = a639(0, 2);
            int k1 = a639(2, 2);
            return a636(i) + a636(k1);
        }
        if(a652.equals("FD"))
        {
            long l = a640(0, 8);
            double d = Double.longBitsToDouble(l);
            return String.valueOf(d);
        }
        if(a652.equals("FL"))
        {
            int j = a639(0, 4);
            float f = Float.intBitsToFloat(j);
            return String.valueOf(f);
        }
        if(a652.equals("SL"))
        {
            int k = a639(0, 4);
            return String.valueOf(k);
        }
        if(a652.equals("SS"))
        {
            int i1 = a638(0);
            return String.valueOf(i1);
        }
        if(a652.equals("UL"))
        {
            long l1 = a640(0, 4);
            return String.valueOf(l1);
        }
        if(a652.equals("US"))
        {
            int j1 = a639(0, 2);
            return String.valueOf(j1);
        } else
        {
            return null;
        }
    }

    void a642()
    {
        if(a589 == null)
        {
            a467 = "";
            return;
        }
        if(a652.equals("OT"))
        {
            a467 = "Pixel Data";
            return;
        }
        if(a652.equals("SQ"))
        {
            a467 = "Sequence";
            return;
        }
        if(a652.equals("OB"))
        {
            a467 = "Binary Data (OB)";
            return;
        }
        if(a652.equals("OW"))
        {
            a467 = "Binary Data (OW)";
            return;
        }
        if(a652.equals("ST"))
        {
            a467 = "Short Text";
            return;
        }
        if(a652.equals("LT"))
        {
            a467 = "Long Text";
            return;
        }
        if(a652.equals("UT"))
        {
            a467 = "Unlimited Text";
            return;
        }
        if(a652.equals("UN"))
        {
            a467 = "Unknown";
            return;
        }
        if(a589.length == 0)
            a467 = "";
        else
            a467 = a641();
        if(a467 == null)
            a467 = new String(a589);
    }

    void a643(String s, String s1, String s2, String s3)
    {
        if(s3 == null)
            a654 = super.toString() + " displayDescription";
        else
            a654 = s3;
        if(s2 == null)
            a653 = super.toString() + " description";
        else
            a653 = s2;
        if(s1 == null)
            a652 = "LO";
        else
            a652 = s1;
        if(s == null)
        {
            a651 = "FFFFFFFF";
            return;
        } else
        {
            a651 = s;
            return;
        }
    }

    void a643(int i, String s, String s1)
    {
        if(s1 == null)
            a654 = super.toString() + " displayDescription";
        else
            a654 = s1;
        a653 = super.toString() + " description";
        a652 = "LO";
        a651 = a636(0) + a636(i);
        a467 = s;
    }

    void a644(boolean flag)
    {
        a655 = flag;
    }

    void a645(byte abyte0[])
    {
        a589 = abyte0;
    }

    public boolean equals(Object obj)
    {
        a632 a632_1 = (a632)obj;
        if(a651.equals(a632_1.a651))
            return true;
        if(a653.equals(a632_1.a653))
            return true;
        return a654.equals(a632_1.a654);
    }

    int a646()
    {
        if(a650 == -1)
            try
            {
                a650 = Integer.parseInt(a651, 16);
            }
            catch(NumberFormatException numberformatexception)
            {
                System.err.println("tag is not numeric in DICMElement.getTag()");
                System.err.println(numberformatexception);
                a650 = 0;
            }
        return a650;
    }

    int a647()
    {
        int i = a646();
        return i >> 16 & 0xffff;
    }

    int a648()
    {
        int i = a646();
        return i & 0xffff;
    }

    String a458()
    {
        if(a467 == null)
        {
            a642();
            a589 = null;
        }
        return a467.trim();
    }

    byte[] a649()
    {
        return a589;
    }

    a632()
    {
        a650 = -1;
    }
}
