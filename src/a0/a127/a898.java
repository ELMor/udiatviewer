// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;

// Referenced classes of package a0.a127:
//            a133, a546, a964

public class a898 extends a133
{

    byte a900[];
    byte a901[];
    byte a902[];
    byte a903[];
    byte a904[];
    byte a905[];

    public a898(a546 a546_1)
    {
        a3(a546_1);
    }

    void a129()
    {
        int i = super.a966 - super.a161.a564;
        int j = i - super.a967 / 2;
        int k = i + super.a967 / 2;
        if(a902 == null)
        {
            a131();
            return;
        }
        for(int k1 = 0; k1 < super.a161.a567; k1++)
        {
            int l = a903[k1] & 0xff;
            int i1 = a904[k1] & 0xff;
            int j1 = a905[k1] & 0xff;
            a900[k1] = (byte)(l >= j ? l <= k ? (255 * (l - j)) / super.a967 : 255 : '\0');
            a901[k1] = (byte)(i1 >= j ? i1 <= k ? (255 * (i1 - j)) / super.a967 : 255 : '\0');
            a902[k1] = (byte)(j1 >= j ? j1 <= k ? (255 * (j1 - j)) / super.a967 : 255 : '\0');
        }

        if(super.a165)
        {
            for(int l1 = 0; l1 < super.a161.a567; l1++)
            {
                a900[l1] = (byte)(a900[l1] ^ 0xff);
                a901[l1] = (byte)(a901[l1] ^ 0xff);
                a902[l1] = (byte)(a902[l1] ^ 0xff);
            }

        }
        int i2 = super.a161.a568;
        for(int j2 = 0; j2 < i2; j2++)
            super.a166[j2] = null;

    }

    ImageProducer a130(int i)
    {
        a159(i);
        if(super.a162 == null)
            a131();
        IndexColorModel indexcolormodel = new IndexColorModel(8, super.a161.a567, a900, a901, a902);
        return new MemoryImageSource(super.a161.a560, super.a161.a561, indexcolormodel, super.a162[i], 0, super.a161.a560);
    }

    void a131()
    {
        int i = super.a161.a568;
        super.a162 = new byte[i][];
        if(i > 1)
        {
            int j = super.a161.a560 * super.a161.a561;
            byte abyte0[] = super.a161.a547();
            int k = 0;
            int l = 0;
            for(; k < i; k++)
            {
                super.a162[k] = new byte[j];
                for(int i1 = 0; i1 < j;)
                {
                    super.a162[k][i1] = abyte0[l];
                    i1++;
                    l++;
                }

            }

        } else
        {
            super.a162[0] = super.a161.a547();
        }
        super.a161.a548();
        a900 = new byte[super.a161.a567];
        a901 = new byte[super.a161.a567];
        a902 = new byte[super.a161.a567];
        a903 = a899(super.a161.a576);
        a904 = a899(super.a161.a577);
        a905 = a899(super.a161.a578);
        super.a161.a576 = null;
        super.a161.a577 = null;
        super.a161.a578 = null;
        a129();
    }

    byte[] a899(byte abyte0[])
    {
        int i = abyte0.length / 2;
        byte abyte1[] = new byte[super.a161.a567];
        int j = 0;
        int k = 0;
        for(; j < i; j++)
        {
            k++;
            abyte1[j] = (byte)(abyte0[k] & 0xff);
            k++;
        }

        return abyte1;
    }
}
