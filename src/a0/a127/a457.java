// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Stack;
import java.util.Vector;

// Referenced classes of package a0.a127:
//            a937, a918

public class a457 extends a937
{

    a918 a467;
    Vector a468;
    Stack a469;

    public a457(BufferedInputStream bufferedinputstream)
    {
        a467 = new a918();
        a467.a643("20500020", null, "presentationLUTShape", null);
        super.a959 = new DataInputStream(bufferedinputstream);
        a416();
    }

    public a918 a458()
    {
        return a467;
    }

    public Vector a459()
    {
        return a468;
    }

    void a460()
    {
        try
        {
            if(!a951(8))
            {
                System.err.println("Group 0x0008 not found");
                super.a959.close();
                return;
            }
            if(a952(8, 4373))
                if(super.a958 == null)
                {
                    a463(-1);
                } else
                {
                    a461(new DataInputStream(new ByteArrayInputStream(super.a958)));
                    a463(super.a958.length);
                    a462();
                }
            a944(a467, 8, 32736);
            return;
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception: Presentation State File Format not supported");
            System.err.println(ioexception);
            return;
        }
    }

    void a461(DataInputStream datainputstream)
    {
        if(a469 == null)
            a469 = new Stack();
        a469.push(super.a959);
        super.a959 = datainputstream;
    }

    void a462()
        throws IOException
    {
        if(a469 == null)
            return;
        if(a469.empty())
        {
            return;
        } else
        {
            super.a959.close();
            super.a959 = (DataInputStream)a469.pop();
            return;
        }
    }

    void a463(int i)
        throws IOException
    {
        int l;
        for(; i != 0; i -= l)
        {
            int j = super.a959.readShort();
            j = super.a655 ? j << 8 & 0xff00 | j >> 8 & 0xff : j & 0xffff;
            i -= 2;
            int k = super.a959.readShort();
            k = super.a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
            i -= 2;
            if(j == 65534)
                l = a943(super.a959.readInt());
            else
                l = a950();
            i -= 4;
            if(j == 65534 && k - 57344 > 0 && l == 0)
                break;
            super.a958 = new byte[l];
            super.a959.readFully(super.a958);
            a461(new DataInputStream(new ByteArrayInputStream(super.a958)));
            a464();
            a462();
        }

    }

    void a464()
        throws IOException
    {
        if(!a951(8))
        {
            System.err.println("Group 0x0008 not found");
            super.a959.close();
            return;
        }
        if(a952(8, 4416))
        {
            if(super.a958 == null)
            {
                a465(-1);
                return;
            }
            a461(new DataInputStream(new ByteArrayInputStream(super.a958)));
            a465(super.a958.length);
            a462();
        }
    }

    void a465(int i)
        throws IOException
    {
        int l;
        for(; i != 0; i -= l)
        {
            int j = super.a959.readShort();
            j = super.a655 ? j << 8 & 0xff00 | j >> 8 & 0xff : j & 0xffff;
            i -= 2;
            int k = super.a959.readShort();
            k = super.a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
            i -= 2;
            if(j == 65534)
                l = a943(super.a959.readInt());
            else
                l = a950();
            i -= 4;
            if(j == 65534 && k - 57344 > 0 && l == 0)
                break;
            super.a958 = new byte[l];
            super.a959.readFully(super.a958);
            a461(new DataInputStream(new ByteArrayInputStream(super.a958)));
            a466();
            a462();
        }

    }

    void a466()
        throws IOException
    {
        if(!a951(8))
        {
            System.err.println("Group 0x0008 not found");
            super.a959.close();
            return;
        }
        if(a952(8, 4437))
        {
            String s = new String(super.a958);
            if(a468 == null)
                a468 = new Vector();
            a468.addElement(s);
        }
    }
}
