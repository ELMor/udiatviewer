// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.Component;
import java.awt.Image;
import java.awt.image.ImageProducer;

// Referenced classes of package a0.a127:
//            a964, a546, a918

public abstract class a133 extends a964
{

    a546 a161;
    byte a162[][];
    boolean a163[];
    boolean a164;
    boolean a165;
    Image a166[];

    abstract ImageProducer a130(int i);

    abstract void a129();

    void a3(a546 a546_1)
    {
        a161 = a546_1;
        super.a968 = a546_1.a421;
        int i = a161.a568;
        a166 = new Image[i];
        a163 = new boolean[i];
        for(int j = 0; j < i; j++)
            a163[j] = true;

        super.a966 = a153();
        super.a967 = a154();
    }

    public int a134(int i, int j, int k)
    {
        int l = k * a150() + j;
        return a162[i][l] & 0xff;
    }

    public void a135()
    {
        a165 = !a165;
        a129();
    }

    public Image a136()
    {
        return a136(0);
    }

    public Image a136(int i)
    {
        if(a166[i] == null)
            a166[i] = super.a968.createImage(a130(i));
        else
        if(a163[i])
        {
            a159(i);
            a166[i].flush();
        }
        return a166[i];
    }

    public void a137(int i, int j)
    {
        j = j >= 1 ? j : 1;
        if(a164)
            i = i >= -1024 ? i : -1024;
        else
            i = i >= a161.a564 ? i : a161.a564;
        if(super.a966 != i || super.a967 != j)
        {
            a160();
            super.a966 = i;
            super.a967 = j;
            a129();
        }
    }

    public int a138()
    {
        return 117;
    }

    public int a139()
    {
        return 256;
    }

    public int a140()
    {
        return 256;
    }

    public int a141()
    {
        return 0;
    }

    public boolean a142()
    {
        return a165;
    }

    public boolean a143()
    {
        return a161.a568 > 1;
    }

    public int a144()
    {
        int i = a140() - a141();
        return (int)Math.ceil(Math.log(i) / Math.log(2D));
    }

    public int a145()
    {
        return a161.a568;
    }

    public float a146()
    {
        return a161.a573;
    }

    public float a147()
    {
        return a161.a574;
    }

    public void a148(byte abyte0[])
    {
        a160();
    }

    public void a149(float f)
    {
        a160();
    }

    public int a150()
    {
        return a161.a560;
    }

    public int a151()
    {
        return a161.a561;
    }

    public a918 a152()
    {
        return a161.a467;
    }

    public int a153()
    {
        if(a161.a563 == -33999)
            return (a140() + a141()) / 2;
        else
            return a161.a563;
    }

    public int a154()
    {
        if(a161.a562 == -1)
            return a140() - a141();
        else
            return a161.a562;
    }

    public void a112()
    {
        int i = a161.a568;
        for(int j = 0; j < i; j++)
            a155(a162[j]);

        a160();
    }

    void a155(byte abyte0[])
    {
        int i = a161.a560;
        int j = a161.a561;
        byte abyte1[] = new byte[i];
        int k = j / 2;
        int l = 0;
        int i1 = i * j;
        for(int j1 = 0; j1 < k; j1++)
        {
            i1 -= i;
            System.arraycopy(abyte0, l, abyte1, 0, i);
            System.arraycopy(abyte0, i1, abyte0, l, i);
            System.arraycopy(abyte1, 0, abyte0, i1, i);
            l += i;
        }

    }

    public void a113()
    {
        int i = a161.a568;
        for(int j = 0; j < i; j++)
            a156(a162[j]);

        a160();
    }

    void a156(byte abyte0[])
    {
        int i = a161.a560;
        int j = a161.a561;
        byte abyte1[] = new byte[i];
        int k = 0;
        for(int l = 0; l < j; l++)
        {
            int i1 = i;
            System.arraycopy(abyte0, k, abyte1, 0, i);
            for(int j1 = 0; j1 < i; j1++)
            {
                i1--;
                abyte0[k] = abyte1[i1];
                k++;
            }

        }

    }

    public void a114()
    {
        int i = a161.a560;
        int j = a161.a561;
        byte abyte0[] = new byte[i * j];
        int k = a161.a568;
        for(int l = 0; l < k; l++)
            a157(a162[l], abyte0);

        if(j == i)
        {
            a160();
            return;
        }
        a161.a560 = j;
        a161.a561 = i;
        for(int i1 = 0; i1 < k; i1++)
            a166[i1] = null;

    }

    void a157(byte abyte0[], byte abyte1[])
    {
        int i = a161.a560;
        int j = a161.a561;
        System.arraycopy(abyte0, 0, abyte1, 0, i * j);
        int k = 0;
        boolean flag = false;
        int i1 = j;
        for(int j1 = 0; j1 < j; j1++)
        {
            int l = --i1;
            for(int k1 = 0; k1 < i; k1++)
            {
                abyte0[l] = abyte1[k];
                k++;
                l += j;
            }

        }

    }

    public void a115()
    {
        int i = a161.a560;
        int j = a161.a561;
        byte abyte0[] = new byte[i * j];
        int k = a161.a568;
        for(int l = 0; l < k; l++)
            a158(a162[l], abyte0);

        if(j == i)
        {
            a160();
            return;
        }
        a161.a560 = j;
        a161.a561 = i;
        for(int i1 = 0; i1 < k; i1++)
            a166[i1] = null;

    }

    void a158(byte abyte0[], byte abyte1[])
    {
        int i = a161.a560;
        int j = a161.a561;
        int k = i * j;
        System.arraycopy(abyte0, 0, abyte1, 0, k);
        int l = 0;
        int i1 = k;
        int j1 = k;
        for(int k1 = 0; k1 < j; k1++)
        {
            for(int l1 = 0; l1 < i; l1++)
            {
                i1 -= j;
                abyte0[i1] = abyte1[l];
                l++;
            }

            i1 = ++j1;
        }

    }

    void a159(int i)
    {
        a163[i] = false;
    }

    void a160()
    {
        int i = a161.a568;
        for(int j = 0; j < i; j++)
            a163[j] = true;

    }

    public a133()
    {
        a164 = false;
        a165 = false;
    }
}
