// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import a0.a1.a827;
import java.awt.Component;
import java.awt.image.ImageObserver;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package a0.a127:
//            a937, a918

class a546 extends a937
{

    static final int a558 = 3;
    a918 a467;
    String a559;
    int a560;
    int a561;
    int a562;
    int a563;
    int a564;
    int a394;
    int a565;
    int a566;
    int a567;
    int a568;
    int a569;
    int a570;
    int a571;
    String a572;
    float a573;
    float a574;
    char a575[];
    byte a576[];
    byte a577[];
    byte a578[];
    byte a579[][];
    Component a421;
    int a580;
    int a581;
    int a582;
    int a583;

    public a546(BufferedInputStream bufferedinputstream, Component component, a918 a918_1)
    {
        a467 = (a918)a918_1.clone();
        a421 = component;
        super.a959 = new DataInputStream(bufferedinputstream);
        a416();
    }

    void a460()
    {
        try
        {
            a944(a467, 2, 40);
            if(!a951(40))
            {
                System.err.println("Group 0x0028 not found");
                super.a959.close();
                return;
            }
            a569 = a953(40, 2);
            if(a952(40, 4))
                a559 = new String(super.a958);
            else
                a559 = new String("MONOCHROME2");
            if(a569 == -1)
                if(a559.startsWith("MONOCHROME"))
                    a569 = 1;
                else
                if(a559.startsWith("PALETTE"))
                    a569 = 1;
                else
                if(a559.startsWith("ARGB"))
                    a569 = 4;
                else
                if(a559.startsWith("CMYK"))
                    a569 = 4;
                else
                    a569 = 3;
            a570 = a953(40, 6);
            if(a570 != 1)
                a570 = 0;
            if(a947(40, 8))
                a568 = a939();
            else
                a568 = 1;
            a561 = a953(40, 16);
            if(a561 == -1)
            {
                System.err.println("Element (0x0028,0x0010) not found");
                super.a959.close();
                return;
            }
            a560 = a953(40, 17);
            if(a560 == -1)
            {
                System.err.println("Element (0x0028,0x0011) not found");
                super.a959.close();
                return;
            }
            if(a947(40, 48))
            {
                a573 = a940();
                a574 = a941();
            }
            a394 = a953(40, 256);
            a565 = a953(40, 257);
            a571 = a953(40, 259);
            a566 = a953(40, 263);
            if(a947(40, 4176))
                a563 = a939();
            else
                a563 = -33999;
            if(a947(40, 4177))
                a562 = a939();
            else
                a562 = -1;
            if(a947(40, 4178))
                a564 = a939();
            else
                a564 = 0;
            if(a952(40, 4180))
                a572 = new String(super.a958);
            else
                a572 = new String("US");
            if(a572.startsWith("OD"))
                a564 /= 1000;
            a567 = a954(40, 4353);
            int i = a954(40, 4354);
            if(i > a567)
                a567 = i;
            int j = a954(40, 4355);
            if(j > a567)
                a567 = j;
            if(a952(40, 4609))
                a576 = super.a958;
            if(a952(40, 4610))
                a577 = super.a958;
            if(a952(40, 4611))
                a578 = super.a958;
            if(!a955(40))
            {
                System.err.println("Error on group 0x0028");
                super.a959.close();
                return;
            }
            a944(a467, 40, 32736);
            if(!a951(32736))
            {
                System.err.println("Group 0x7FE0 not found");
                super.a959.close();
                return;
            }
            if(a394 == -1)
            {
                int k = a953(32736, 0);
                if(k == -1)
                {
                    System.err.println("Error on group 0x0028: Not enough information to decode the image");
                    super.a959.close();
                    return;
                }
                a394 = k - 8;
                a394 /= a560;
                a394 /= a561;
                a394 *= 8;
            }
            if(a565 == -1)
                a565 = a394;
            if(a566 == -1)
            {
                a566 = (1 << a565) - 1;
                return;
            }
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception: Image File Format not supported");
            System.err.println(ioexception);
        }
    }

    byte[] a547()
    {
        try
        {
            if(!a549(32736, 16))
            {
                System.err.println("Element (0x7FE0,0x0010) not found");
                super.a959.close();
                return null;
            }
            if(super.a958 == null)
            {
                a550();
                a551();
            }
            super.a959.close();
            return super.a958;
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception: Image Pixel Data could not be loaded");
            System.err.println(ioexception);
            return null;
        }
    }

    void a548()
    {
        super.a958 = null;
        a575 = null;
    }

    boolean a549(int i, int j)
        throws IOException
    {
        super.a959.mark(512);
        short word0 = super.a959.readShort();
        int k = super.a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
        word0 = super.a959.readShort();
        int l;
        for(l = super.a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff; l < j && k == i; l = super.a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff)
        {
            int i1 = a950();
            if(i1 == -1)
                a956();
            else
            if(i1 != super.a959.skipBytes(i1))
                return false;
            super.a959.mark(512);
            word0 = super.a959.readShort();
            k = super.a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            word0 = super.a959.readShort();
        }

        if(l == j && k == i)
        {
            int j1 = a950();
            if(j1 == -1)
            {
                super.a958 = null;
            } else
            {
                super.a958 = new byte[j1];
                int k1 = j1 / a561 / a568;
                int l1 = j1 - a561 * k1 * a568;
                int i2;
                if(l1 == 0)
                    i2 = a561;
                else
                    i2 = a561 + 1;
                int j2 = 0;
                for(int k2 = 0; k2 < a568; k2++)
                {
                    a421.imageUpdate(null, 3, 0, 0, a560, i2);
                    for(int l2 = 0; l2 < a561; l2++)
                    {
                        super.a959.readFully(super.a958, j2, k1);
                        a421.imageUpdate(null, 8, 0, l2, a560, 1);
                        j2 += k1;
                    }

                    if(i2 > a561)
                    {
                        super.a959.readFully(super.a958, j2, l1);
                        a421.imageUpdate(null, 8, 0, a561, a560, 1);
                    }
                    j2 += l1;
                }

            }
            return true;
        } else
        {
            super.a959.reset();
            return false;
        }
    }

    void a550()
        throws IOException
    {
        super.a959.mark(512);
        int i = super.a959.readShort();
        i = super.a655 ? i << 8 & 0xff00 | i >> 8 & 0xff : i & 0xffff;
        int k = super.a959.readShort();
        k = super.a655 ? k << 8 & 0xff00 | k >> 8 & 0xff : k & 0xffff;
        int i1;
        if(i == 65534)
            i1 = a943(super.a959.readInt());
        else
            i1 = a950();
        int k1 = i1 / 4;
        if(k1 == a568)
        {
            for(int l1 = 0; l1 < k1; l1++)
                a943(super.a959.readInt());

        } else
        if(k1 == 0)
        {
            k1 = a568;
        } else
        {
            super.a959.reset();
            k1 = 1;
        }
        a579 = new byte[k1][];
        int i2 = 0;
        do
        {
            short word0 = super.a959.readShort();
            int j = super.a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            word0 = super.a959.readShort();
            int l = super.a655 ? word0 << 8 & 0xff00 | word0 >> 8 & 0xff : word0 & 0xffff;
            int j1 = a943(super.a959.readInt());
            if((j != 65534 || l - 57344 <= 0 || j1 != 0) && i2 < k1)
            {
                a421.imageUpdate(null, 3, 0, 0, a560, a561);
                a579[i2] = new byte[j1];
                int j2 = j1 / 8;
                int k2 = j1 - 8 * j2;
                byte byte0;
                if(k2 == 0)
                    byte0 = 8;
                else
                    byte0 = 9;
                int l2 = 0;
                a421.imageUpdate(null, 3, 0, 0, a560, byte0);
                for(int i3 = 0; i3 < 8; i3++)
                {
                    super.a959.readFully(a579[i2], l2, j2);
                    a421.imageUpdate(null, 8, 0, i3, a560, 1);
                    l2 += j2;
                }

                if(byte0 > 8)
                {
                    super.a959.readFully(a579[i2], l2, k2);
                    a421.imageUpdate(null, 8, 0, 8, a560, 1);
                }
                i2++;
            } else
            {
                return;
            }
        } while(true);
    }

    void a551()
    {
        int i = a394 / 8;
        if(super.a960.startsWith("1.2.840.10008.1.2.4.50"))
            a552();
        else
        if(super.a960.startsWith("1.2.840.10008.1.2.4.51"))
            a552();
        else
        if(super.a960.startsWith("1.2.840.10008.1.2.4.57"))
            a552();
        else
        if(super.a960.startsWith("1.2.840.10008.1.2.4.70"))
            a552();
        else
        if(super.a960.startsWith("1.2.840.10008.1.2.5"))
            a553();
        else
            super.a958 = new byte[a560 * a561 * a569 * a568 * i];
        a579 = null;
    }

    void a552()
    {
        a827 a827_1 = new a827();
        int i = a579.length;
        if(a568 == 1)
        {
            if(i == 1)
            {
                DataInputStream datainputstream = new DataInputStream(new ByteArrayInputStream(a579[0]));
                a827_1.a416(datainputstream);
                if(a827_1.a835() > 8)
                    a575 = a827_1.a832();
                else
                    super.a958 = a827_1.a831();
                a579[0] = null;
            }
        } else
        {
            int j = a560 * a561 * a569;
            if(a394 > 8)
            {
                a575 = new char[j * a568];
                a827_1.a829(a575);
            } else
            {
                super.a958 = new byte[j * a568];
                a827_1.a828(super.a958);
            }
            for(int k = 0; k < i; k++)
            {
                DataInputStream datainputstream1 = new DataInputStream(new ByteArrayInputStream(a579[k]));
                a827_1.a830(j * k);
                a827_1.a416(datainputstream1);
                a579[k] = null;
            }

        }
        if(a827_1.a836() == 3 && a827_1.a837())
        {
            a569 = 3;
            a559 = "YBR_FULL";
            a570 = 1;
        }
    }

    void a553()
    {
        int i = a394 / 8;
        super.a958 = new byte[a560 * a561 * a569 * a568 * i];
        a580 = 0;
        int j = a579.length;
        for(int k = 0; k < j; k++)
            try
            {
                DataInputStream datainputstream = new DataInputStream(new ByteArrayInputStream(a579[k]));
                int l = a943(datainputstream.readInt());
                for(int i1 = 0; i1 < l; i1++)
                {
                    int j1 = a943(datainputstream.readInt());
                    a554(k, j1);
                }

            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
            }

        if(a569 > 1)
            a570 = 1;
    }

    void a554(int i, int j)
    {
        int k = a579[i].length - 1;
        if(a580 >= super.a958.length)
            return;
        for(int l = j; l < k;)
        {
            byte byte0 = a579[i][l];
            if(byte0 >= 0 && byte0 <= 127)
            {
                int i1 = byte0 + 1;
                for(int k1 = 0; k1 < i1; k1++)
                    super.a958[a580++] = a579[i][++l];

                l++;
            }
            if(byte0 <= -1 && byte0 >= -127)
            {
                int j1 = 1 - byte0;
                l++;
                for(int l1 = 0; l1 < j1; l1++)
                    super.a958[a580++] = a579[i][l];

                l++;
            }
            if(byte0 == -128)
                l++;
        }

    }

    void a555()
    {
        int i = a560 * a561;
        try
        {
            DataInputStream datainputstream = new DataInputStream(new ByteArrayInputStream(a547()));
            if(a394 > 8)
            {
                a575 = new char[i];
                for(int j = 0; j < i; j++)
                    a575[j] = (char)a556(datainputstream, a394);

                super.a958 = null;
                return;
            }
            super.a958 = new byte[i];
            for(int k = 0; k < i; k++)
                super.a958[k] = (byte)a556(datainputstream, a394);

            return;
        }
        catch(IOException ioexception)
        {
            System.err.println("Exception ocurred in DICMImage.extendBits");
            System.err.println(ioexception);
            return;
        }
    }

    final int a556(DataInputStream datainputstream, int i)
        throws IOException
    {
        int j = 0;
        for(int k = 0; k < i; k++)
            j = j << 1 | a557(datainputstream);

        return j;
    }

    final int a557(DataInputStream datainputstream)
        throws IOException
    {
        if(a581 == 0)
        {
            a582 = datainputstream.readUnsignedByte();
            a581 = 8;
            a583 = 128;
        }
        int i = a583 & a582;
        a583 >>= 1;
        a581--;
        return i >> a581;
    }
}
