// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.Component;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.PrintStream;

// Referenced classes of package a0.a127:
//            a964, a918

public class a683 extends a964
{

    Image a486;
    int a575[];
    int a686[];
    int a269[];
    a918 a426;
    boolean a687;

    public a683(Image image, Component component, a918 a918)
    {
        a486 = image;
        super.a968 = component;
        super.a967 = 256;
        super.a966 = 128;
        a575 = null;
        a686 = null;
        a687 = false;
        a426 = a918;
    }

    public boolean a142()
    {
        return false;
    }

    public boolean a143()
    {
        return false;
    }

    public int a140()
    {
        return 256;
    }

    public int a141()
    {
        return 0;
    }

    public int a144()
    {
        int i = a140() - a141();
        return (int)Math.ceil(Math.log(i) / Math.log(2D));
    }

    public int a145()
    {
        return 1;
    }

    public float a146()
    {
        return 0.0F;
    }

    public float a147()
    {
        return 0.0F;
    }

    public void a148(byte abyte0[])
    {
    }

    public void a149(float f)
    {
    }

    public void a137(int i, int j)
    {
        j = j >= 1 ? j : 1;
        i = i >= 0 ? i : 0;
        if(i != super.a966 || j != super.a967)
        {
            super.a966 = i;
            super.a967 = j;
            a685(false, false);
        }
    }

    public int a134(int i, int j, int k)
    {
        int ai[] = a684();
        int l = k * a150() + j;
        int i1 = ai[l];
        int j1 = i1 >> 16 & 0xff;
        int k1 = i1 >> 8 & 0xff;
        int l1 = i1 & 0xff;
        return (int)Math.round(0.29899999999999999D * (double)j1 + 0.58699999999999997D * (double)k1 + 0.114D * (double)l1);
    }

    public void a135()
    {
        int ai[] = a684();
        int i = a150() * a151();
        for(int j = 0; j < i; j++)
            ai[j] = ai[j] ^ 0xffffff;

        a685(false, false);
    }

    public void a112()
    {
        int i = a150();
        int j = a151();
        int ai[] = new int[i];
        int ai1[] = a684();
        int k = j / 2;
        int l = 0;
        int i1 = i * j;
        for(int j1 = 0; j1 < k; j1++)
        {
            i1 -= i;
            System.arraycopy(ai1, l, ai, 0, i);
            System.arraycopy(ai1, i1, ai1, l, i);
            System.arraycopy(ai, 0, ai1, i1, i);
            l += i;
        }

        a685(false, false);
    }

    public void a113()
    {
        int i = a150();
        int j = a151();
        int ai[] = new int[i];
        int ai1[] = a684();
        int k = 0;
        for(int l = 0; l < j; l++)
        {
            int i1 = i;
            System.arraycopy(ai1, k, ai, 0, i);
            for(int j1 = 0; j1 < i; j1++)
            {
                i1--;
                ai1[k] = ai[i1];
                k++;
            }

        }

        a685(false, false);
    }

    public void a114()
    {
        int i = a150();
        int j = a151();
        int ai[] = a684();
        a686 = new int[i * j];
        int k = 0;
        boolean flag = false;
        int i1 = j;
        for(int j1 = 0; j1 < j; j1++)
        {
            int l = --i1;
            for(int k1 = 0; k1 < i; k1++)
            {
                a686[l] = ai[k];
                k++;
                l += j;
            }

        }

        if(i == j)
        {
            a685(false, false);
            return;
        } else
        {
            a685(true, true);
            return;
        }
    }

    public void a115()
    {
        int i = a150();
        int j = a151();
        int k = i * j;
        int ai[] = a684();
        a686 = new int[k];
        int l = 0;
        int i1 = k;
        int j1 = k;
        for(int k1 = 0; k1 < j; k1++)
        {
            for(int l1 = 0; l1 < i; l1++)
            {
                i1 -= j;
                a686[i1] = ai[l];
                l++;
            }

            i1 = ++j1;
        }

        if(i == j)
        {
            a685(false, false);
            return;
        } else
        {
            a685(true, true);
            return;
        }
    }

    public int a138()
    {
        return 128;
    }

    public int a139()
    {
        return 256;
    }

    public Image a136()
    {
        if(a687)
        {
            a486.flush();
            a687 = false;
        }
        return a486;
    }

    public Image a136(int i)
    {
        return a136();
    }

    public int a150()
    {
        int i;
        do
            i = a486.getWidth(super.a968);
        while(i < 1);
        return i;
    }

    public int a151()
    {
        int i;
        do
            i = a486.getHeight(super.a968);
        while(i < 1);
        return i;
    }

    public a918 a152()
    {
        return a426;
    }

    public int a153()
    {
        return 128;
    }

    public int a154()
    {
        return 256;
    }

    int[] a684()
    {
        if(a575 == null)
        {
            int i = a150();
            int k = a151();
            a575 = new int[i * k];
        }
        if(a686 == null)
        {
            int j = a150();
            int l = a151();
            int ai[] = new int[j * l];
            PixelGrabber pixelgrabber = new PixelGrabber(a486, 0, 0, j, l, ai, 0, j);
            try
            {
                pixelgrabber.grabPixels();
            }
            catch(InterruptedException _ex)
            {
                System.err.println("interrupted  waiting  for  pixels!");
                return ai;
            }
            if((pixelgrabber.status() & 0x80) != 0)
            {
                System.err.println("image  fetch  aborted  or  errored");
                return ai;
            }
            a686 = ai;
            a685(true, false);
        }
        return a686;
    }

    void a685(boolean flag, boolean flag1)
    {
        int i = a150();
        int j = a151();
        int k = i * j;
        int ai[] = a684();
        int l = super.a966 - super.a967 / 2;
        int i1 = super.a966 + super.a967 / 2;
        if(a269 == null)
            a269 = new int[256];
        for(int j1 = 0; j1 < 256; j1++)
            a269[j1] = j1 > l ? j1 < i1 ? (255 * (j1 - l)) / super.a967 : 255 : 0;

        for(int k1 = 0; k1 < k; k1++)
        {
            int l1 = ai[k1];
            int i2 = l1 >> 16 & 0xff;
            int j2 = l1 >> 8 & 0xff;
            int k2 = l1 & 0xff;
            i2 = a269[i2];
            j2 = a269[j2];
            k2 = a269[k2];
            a575[k1] = l1 & 0xff000000 | i2 << 16 | j2 << 8 | k2;
        }

        if(flag)
        {
            if(flag1)
            {
                a486 = super.a968.createImage(new MemoryImageSource(j, i, a575, 0, j));
                return;
            } else
            {
                a486 = super.a968.createImage(new MemoryImageSource(i, j, a575, 0, i));
                return;
            }
        } else
        {
            a687 = true;
            return;
        }
    }
}
