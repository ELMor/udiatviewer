// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.image.ImageProducer;
import java.awt.image.MemoryImageSource;

// Referenced classes of package a0.a127:
//            a133, a546, a964

public class a293 extends a133
{

    int a295[];
    int a296[];
    int a297[];

    public a293(a546 a546_1)
    {
        a3(a546_1);
    }

    public int a134(int i, int j, int k)
    {
        int l = super.a161.a560 * super.a161.a561 * i + k * super.a161.a560 + j;
        int i1 = a296[l];
        int j1 = i1 >> 16 & 0xff;
        int k1 = i1 >> 8 & 0xff;
        int l1 = i1 & 0xff;
        return (int)Math.round(0.29899999999999999D * (double)j1 + 0.58699999999999997D * (double)k1 + 0.114D * (double)l1);
    }

    public void a135()
    {
        int i = super.a161.a560 * super.a161.a561 * super.a161.a568;
        for(int j = 0; j < i; j++)
            a296[j] = a296[j] ^ 0xffffff;

        a160();
    }

    public boolean a142()
    {
        return false;
    }

    public void a112()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = super.a161.a568;
        int l = i * j;
        int i1 = j / 2;
        int ai[] = new int[i];
        for(int j1 = 0; j1 < k; j1++)
        {
            int k1 = j1 * l;
            int l1 = (j1 + 1) * l;
            for(int i2 = 0; i2 < i1; i2++)
            {
                l1 -= i;
                System.arraycopy(a296, k1, ai, 0, i);
                System.arraycopy(a296, l1, a296, k1, i);
                System.arraycopy(ai, 0, a296, l1, i);
                k1 += i;
            }

        }

        a160();
    }

    public void a113()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = super.a161.a568;
        int ai[] = new int[i];
        int l = 0;
        for(int i1 = 0; i1 < k; i1++)
        {
            for(int j1 = 0; j1 < j; j1++)
            {
                int k1 = i;
                System.arraycopy(a296, l, ai, 0, i);
                for(int l1 = 0; l1 < i; l1++)
                {
                    k1--;
                    a296[l] = ai[k1];
                    l++;
                }

            }

        }

        a160();
    }

    public void a114()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = i * j;
        int ai[] = new int[k];
        int l = super.a161.a568;
        for(int i1 = 0; i1 < l; i1++)
        {
            int j1 = i1 * k;
            System.arraycopy(a296, j1, ai, 0, k);
            int l1 = j1 + j;
            j1 = 0;
            for(int i2 = 0; i2 < j; i2++)
            {
                int j2 = --l1;
                for(int k2 = 0; k2 < i; k2++)
                {
                    a296[j2] = ai[j1];
                    j1++;
                    j2 += j;
                }

            }

        }

        if(j == i)
        {
            a160();
            return;
        }
        super.a161.a560 = j;
        super.a161.a561 = i;
        for(int k1 = 0; k1 < l; k1++)
            super.a166[k1] = null;

    }

    public void a115()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = i * j;
        int ai[] = new int[k];
        int l = super.a161.a568;
        for(int i1 = 0; i1 < l; i1++)
        {
            int j1 = i1 * k;
            System.arraycopy(a296, j1, ai, 0, k);
            int l1 = j1 + k;
            int i2 = l1;
            j1 = 0;
            for(int j2 = 0; j2 < j; j2++)
            {
                for(int k2 = 0; k2 < i; k2++)
                {
                    l1 -= j;
                    a296[l1] = ai[j1];
                    j1++;
                }

                l1 = ++i2;
            }

        }

        if(j == i)
        {
            a160();
            return;
        }
        super.a161.a560 = j;
        super.a161.a561 = i;
        for(int k1 = 0; k1 < l; k1++)
            super.a166[k1] = null;

    }

    ImageProducer a130(int i)
    {
        if(a297 == null)
            a131();
        a159(i);
        int j = i * super.a161.a560 * super.a161.a561;
        return new MemoryImageSource(super.a161.a560, super.a161.a561, a297, j, super.a161.a560);
    }

    void a129()
    {
        int i = super.a966 - super.a161.a564;
        int j = i - super.a967 / 2;
        int k = i + super.a967 / 2;
        if(a295 == null)
            a295 = new int[256];
        for(int l = 0; l < 256; l++)
            a295[l] = l >= j ? l <= k ? (255 * (l - j)) / super.a967 : 255 : 0;

    }

    void a159(int i)
    {
        int j = super.a161.a560 * super.a161.a561 * i;
        int k = super.a161.a560 * super.a161.a561 * (i + 1);
        for(int l = j; l < k; l++)
        {
            int i1 = a296[l];
            int j1 = i1 >> 16 & 0xff;
            int k1 = i1 >> 8 & 0xff;
            int l1 = i1 & 0xff;
            j1 = a295[j1];
            k1 = a295[k1];
            l1 = a295[l1];
            a297[l] = i1 & 0xff000000 | j1 << 16 | k1 << 8 | l1;
        }

        super.a163[i] = false;
    }

    void a131()
    {
        byte abyte0[] = super.a161.a547();
        if(super.a161.a559.startsWith("YBR_FULL"))
            a294(abyte0);
        boolean flag = super.a161.a559.startsWith("ARGB");
        int i = super.a161.a560 * super.a161.a561;
        int j = i * super.a161.a568;
        a296 = new int[j];
        int k = super.a161.a570;
        if(k == 0)
        {
            int l = 0;
            int j1 = 0;
            for(; l < j; l++)
            {
                int l1;
                if(flag)
                    l1 = 255 - (abyte0[j1++] & 0xff);
                else
                    l1 = 255;
                int j2 = abyte0[j1++] & 0xff;
                int j3 = abyte0[j1++] & 0xff;
                int l3 = abyte0[j1++] & 0xff;
                a296[l] = l1 << 24 | j2 << 16 | j3 << 8 | l3;
            }

        } else
        {
            int i1 = 0;
            int k1 = 0;
            for(; i1 < super.a161.a568; i1++)
            {
                int i2 = i1 * i;
                if(flag)
                {
                    for(int k2 = 0; k2 < i;)
                    {
                        a296[i2] = 255 - (abyte0[k1] & 0xff) << 24;
                        k2++;
                        i2++;
                        k1++;
                    }

                } else
                {
                    for(int l2 = 0; l2 < i;)
                    {
                        a296[i2] = 0xff000000;
                        l2++;
                        i2++;
                    }

                }
                i2 = i1 * i;
                for(int i3 = 0; i3 < i;)
                {
                    a296[i2] |= (abyte0[k1] & 0xff) << 16;
                    i3++;
                    i2++;
                    k1++;
                }

                i2 = i1 * i;
                for(int k3 = 0; k3 < i;)
                {
                    a296[i2] |= (abyte0[k1] & 0xff) << 8;
                    k3++;
                    i2++;
                    k1++;
                }

                i2 = i1 * i;
                for(int i4 = 0; i4 < i;)
                {
                    a296[i2] |= abyte0[k1] & 0xff;
                    i4++;
                    i2++;
                    k1++;
                }

            }

        }
        super.a161.a548();
        a129();
        a297 = new int[j];
    }

    void a294(byte abyte0[])
    {
        int i = 1 << super.a161.a565 - 1;
        int j = super.a161.a560 * super.a161.a561;
        int k = j * super.a161.a569;
        int l = super.a161.a568;
        int i1 = super.a161.a570;
        for(int j1 = 0; j1 < l; j1++)
        {
            int k1 = j1 * k;
            if(i1 == 0)
            {
                for(int l1 = 0; l1 < j; l1++)
                {
                    int j2 = k1 + 3 * l1;
                    int l2 = abyte0[j2++];
                    int j3 = abyte0[j2++];
                    int l3 = abyte0[j2];
                    l2 &= 0xff;
                    l2 <<= 12;
                    j3 &= 0xff;
                    j3 -= i;
                    l3 &= 0xff;
                    l3 -= i;
                    int j4 = l2 + 5743 * l3;
                    int l4 = l2 - 1410 * j3 - 2925 * l3;
                    int j5 = l2 + 7258 * j3;
                    j4 >>= 12;
                    l4 >>= 12;
                    j5 >>= 12;
                    j4 = j4 >= 0 ? j4 <= 255 ? j4 : 255 : 0;
                    l4 = l4 >= 0 ? l4 <= 255 ? l4 : 255 : 0;
                    j5 = j5 >= 0 ? j5 <= 255 ? j5 : 255 : 0;
                    j2 = k1 + 3 * l1;
                    abyte0[j2++] = (byte)j4;
                    abyte0[j2++] = (byte)l4;
                    abyte0[j2] = (byte)j5;
                }

            } else
            {
                for(int i2 = 0; i2 < j; i2++)
                {
                    int k2 = k1 + i2;
                    int i3 = abyte0[k2];
                    k2 += j;
                    int k3 = abyte0[k2];
                    k2 += j;
                    int i4 = abyte0[k2];
                    i3 &= 0xff;
                    i3 <<= 12;
                    k3 &= 0xff;
                    k3 -= i;
                    i4 &= 0xff;
                    i4 -= i;
                    int k4 = i3 + 5743 * i4;
                    int i5 = i3 - 1410 * k3 - 2925 * i4;
                    int k5 = i3 + 7258 * k3;
                    k4 >>= 12;
                    i5 >>= 12;
                    k5 >>= 12;
                    k4 = k4 >= 0 ? k4 <= 255 ? k4 : 255 : 0;
                    i5 = i5 >= 0 ? i5 <= 255 ? i5 : 255 : 0;
                    k5 = k5 >= 0 ? k5 <= 255 ? k5 : 255 : 0;
                    k2 = k1 + i2;
                    abyte0[k2] = (byte)k4;
                    k2 += j;
                    abyte0[k2] = (byte)i5;
                    k2 += j;
                    abyte0[k2] = (byte)k5;
                }

            }
        }

    }
}
