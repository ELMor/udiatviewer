// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;

// Referenced classes of package a0.a127:
//            a133, a546, a918, a964

public class a584 extends a133
{

    byte a588[];
    char a589[];
    byte a297[];
    int a590;
    int a591;
    int a592;
    int a593;
    IndexColorModel a594;
    String a595;

    public a584(a546 a546_1)
    {
        a591 = 8192;
        a592 = -1;
        a593 = -33999;
        a3(a546_1);
        a595 = a546_1.a467.a648(null, "modality", null);
        super.a164 = a595.equals("CT") && a546_1.a564 == 0;
        byte abyte0[] = new byte[256];
        super.a165 = super.a161.a559.startsWith("MONOCHROME1");
        if(super.a165)
        {
            int i = 0;
            for(int k = 255; i < 256; k--)
            {
                abyte0[k] = (byte)i;
                i++;
            }

        } else
        {
            for(int j = 0; j < 256; j++)
                abyte0[j] = (byte)j;

        }
        a594 = new IndexColorModel(8, 256, abyte0, abyte0, abyte0);
        if(super.a161.a562 == -1 || super.a161.a563 == -33999)
        {
            a131();
            super.a966 = a153();
            super.a967 = a154();
            a129();
        }
    }

    public int a134(int i, int j, int k)
    {
        int l = super.a161.a560 * super.a161.a561;
        int i1 = l * i + k * a150() + j;
        if(a595.equals("CT"))
            return a589[i1] - 1024;
        else
            return a589[i1] + super.a161.a564;
    }

    public int a138()
    {
        if(a593 == -33999)
            return a153();
        else
            return a593;
    }

    public int a139()
    {
        if(a592 == -1)
            return a154();
        else
            return a592;
    }

    public int a140()
    {
        return Math.min(a590, super.a161.a566);
    }

    public int a141()
    {
        return a591;
    }

    public void a148(byte abyte0[])
    {
        a160();
        if(abyte0.length > a590 - a591)
            a588 = abyte0;
    }

    public void a135()
    {
        byte abyte0[] = new byte[256];
        super.a165 = !super.a165;
        if(super.a165)
        {
            int i = 0;
            for(int l = 255; i < 256; l--)
            {
                abyte0[l] = (byte)i;
                i++;
            }

        } else
        {
            for(int j = 0; j < 256; j++)
                abyte0[j] = (byte)j;

        }
        a594 = new IndexColorModel(8, 256, abyte0, abyte0, abyte0);
        int k = super.a161.a568;
        for(int i1 = 0; i1 < k; i1++)
            super.a166[i1] = null;

    }

    public void a112()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = super.a161.a568;
        int l = i * j;
        int i1 = j / 2;
        char ac[] = new char[i];
        for(int j1 = 0; j1 < k; j1++)
        {
            int k1 = j1 * l;
            int l1 = (j1 + 1) * l;
            for(int i2 = 0; i2 < i1; i2++)
            {
                l1 -= i;
                System.arraycopy(a589, k1, ac, 0, i);
                System.arraycopy(a589, l1, a589, k1, i);
                System.arraycopy(ac, 0, a589, l1, i);
                k1 += i;
            }

        }

        a160();
    }

    public void a113()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = super.a161.a568;
        char ac[] = new char[i];
        int l = 0;
        for(int i1 = 0; i1 < k; i1++)
        {
            for(int j1 = 0; j1 < j; j1++)
            {
                int k1 = i;
                System.arraycopy(a589, l, ac, 0, i);
                for(int l1 = 0; l1 < i; l1++)
                {
                    k1--;
                    a589[l] = ac[k1];
                    l++;
                }

            }

        }

        a160();
    }

    public void a114()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = i * j;
        char ac[] = new char[k];
        int l = super.a161.a568;
        for(int i1 = 0; i1 < l; i1++)
        {
            int j1 = i1 * k;
            System.arraycopy(a589, j1, ac, 0, k);
            int l1 = j1 + j;
            j1 = 0;
            for(int i2 = 0; i2 < j; i2++)
            {
                int j2 = --l1;
                for(int k2 = 0; k2 < i; k2++)
                {
                    a589[j2] = ac[j1];
                    j1++;
                    j2 += j;
                }

            }

        }

        if(j == i)
        {
            a160();
            return;
        }
        super.a161.a560 = j;
        super.a161.a561 = i;
        for(int k1 = 0; k1 < l; k1++)
            super.a166[k1] = null;

    }

    public void a115()
    {
        int i = super.a161.a560;
        int j = super.a161.a561;
        int k = i * j;
        char ac[] = new char[k];
        int l = super.a161.a568;
        for(int i1 = 0; i1 < l; i1++)
        {
            int j1 = i1 * k;
            System.arraycopy(a589, j1, ac, 0, k);
            int l1 = j1 + k;
            int i2 = l1;
            j1 = 0;
            for(int j2 = 0; j2 < j; j2++)
            {
                for(int k2 = 0; k2 < i; k2++)
                {
                    l1 -= j;
                    a589[l1] = ac[j1];
                    j1++;
                }

                l1 = ++i2;
            }

        }

        if(j == i)
        {
            a160();
            return;
        }
        super.a161.a560 = j;
        super.a161.a561 = i;
        for(int k1 = 0; k1 < l; k1++)
            super.a166[k1] = null;

    }

    ImageProducer a130(int i)
    {
        if(a297 == null)
            a131();
        a159(i);
        int j = i * super.a161.a560 * super.a161.a561;
        return new MemoryImageSource(super.a161.a560, super.a161.a561, a594, a297, j, super.a161.a560);
    }

    void a129()
    {
        int i = super.a966 - super.a161.a564;
        if(super.a164)
            i += 1024;
        int j = i - super.a967 / 2;
        int k = i + super.a967 / 2;
        if(a588 == null)
        {
            a131();
            return;
        }
        for(int l = a591; l < a590; l++)
            a588[l] = (byte)(l >= j ? l <= k ? (255 * (l - j)) / super.a967 : 255 : '\0');

    }

    void a159(int i)
    {
        super.a163[i] = false;
        int j = super.a161.a560 * super.a161.a561;
        int k = j / 32;
        k *= 32;
        int l = j * i;
        int i1 = l + k;
        for(int j1 = l; j1 < i1; j1++)
        {
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
            j1++;
            a297[j1] = a588[a589[j1]];
        }

        int k1 = j * (i + 1);
        for(int l1 = i1; l1 < k1; l1++)
            a297[l1] = a588[a589[l1]];

    }

    void a585()
    {
        long l = 0L;
        long l1 = 0L;
        int i = 0;
        for(int j = 0; j < a589.length; j++)
            if(a589[j] > ' ')
            {
                l += a589[j];
                l1 += (long)a589[j] * (long)a589[j];
                i++;
            }

        double d = (double)l1 / (double)i;
        double d1 = (double)l / (double)i;
        double d2 = d - d1 * d1;
        d2 = Math.sqrt(d2);
        a592 = 5 * (int)d2;
        a593 = (int)d1 + (int)(d2 / 2D);
    }

    void a586(byte abyte0[])
    {
        int j = a589.length;
        char c;
        if(super.a161.a564 == 0)
            c = '\u0400';
        else
            c = '\0';
        int k = 0;
        for(int l = 0; l < j; l++)
        {
            int i = abyte0[k] & 0xff;
            k++;
            i |= abyte0[k] << 8;
            k++;
            i += c;
            if(i < 0)
                i = 0;
            if(i > a590)
                a590 = i;
            if(i < a591)
                a591 = i;
            a589[l] = (char)i;
        }

    }

    void a587(byte abyte0[])
    {
        int j = a589.length;
        int k = 0;
        for(int l = 0; l < j; l++)
        {
            int i = abyte0[k] & 0xff;
            k++;
            i |= abyte0[k] << 8 & 0xff00;
            k++;
            if(i > a590)
                a590 = i;
            if(i < a591)
                a591 = i;
            a589[l] = (char)i;
        }

    }

    void a131()
    {
        byte abyte0[] = super.a161.a547();
        a297 = new byte[super.a161.a560 * super.a161.a561 * super.a161.a568];
        if(abyte0 == null)
        {
            a589 = super.a161.a575;
            int i = a589.length;
            if(super.a161.a571 == 1 || super.a164)
            {
                char c;
                if(super.a161.a564 == 0)
                    c = '\u0400';
                else
                    c = '\0';
                for(int l = 0; l < i; l++)
                {
                    int j1 = a589[l];
                    if(j1 > 32767)
                        j1 |= 0xffff0000;
                    j1 += c;
                    if(j1 < 0)
                        j1 = 0;
                    if(j1 > a590)
                        a590 = j1;
                    if(j1 < a591)
                        a591 = j1;
                    a589[l] = (char)j1;
                }

            } else
            {
                for(int k = 0; k < i; k++)
                {
                    int i1 = a589[k];
                    if(i1 > a590)
                        a590 = i1;
                    if(i1 < a591)
                        a591 = i1;
                }

            }
        } else
        {
            int j = abyte0.length / 2;
            a589 = new char[j];
            if(super.a161.a571 == 1 || super.a164)
                a586(abyte0);
            else
                a587(abyte0);
        }
        if(a595.equals("MR"))
            a585();
        abyte0 = null;
        super.a161.a548();
        a590++;
        a588 = new byte[a590];
        a129();
    }
}
