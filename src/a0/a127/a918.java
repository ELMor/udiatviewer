// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0.a127;

import java.util.Vector;

// Referenced classes of package a0.a127:
//            a632

public final class a918
    implements Cloneable
{

    Vector a345;
    int a930;
    int a816;
    int a817;
    int a818;
    int a819;
    int a931;

    public void a643(String s, String s1, String s2, String s3)
    {
        a632 a632_1 = new a632();
        a632_1.a643(s, s1, s2, s3);
        a643(a632_1);
    }

    public void a643(String s, String s1)
    {
        if(s.equals(""))
        {
            return;
        } else
        {
            a632 a632_1 = new a632();
            a632_1.a643(a930++, s, s1);
            a643(a632_1);
            return;
        }
    }

    public void a643(a632 a632_1)
    {
        if(a345 == null)
            a345 = new Vector();
        int i = a345.indexOf(a632_1);
        if(i == -1)
        {
            a928(a632_1);
            return;
        } else
        {
            a345.setElementAt(a632_1, i);
            return;
        }
    }

    public void a919(int i, int j, int k, int l)
    {
        a816 = i;
        a817 = j;
        a818 = k;
        a819 = l;
    }

    public void a920(int i, int j, int k, int l)
    {
        if(i > a816)
            a816 = i;
        if(j > a817)
            a817 = j;
        if(k > a818)
            a818 = k;
        if(l > a819)
            a819 = l;
    }

    public final int a921()
    {
        return a816;
    }

    public final int a922()
    {
        return a817;
    }

    public final int a923()
    {
        return a818;
    }

    public final int a924()
    {
        return a819;
    }

    public String a648(String s, String s1, String s2)
    {
        a632 a632_1 = new a632();
        a632_1.a643(s, null, s1, s2);
        int i = a345.indexOf(a632_1);
        if(i != -1)
        {
            a632 a632_2 = (a632)a345.elementAt(i);
            if(a632_2.a467 == null)
            {
                a632_2.a642();
                a632_2.a589 = null;
            }
            return a632_2.a467.trim();
        } else
        {
            return null;
        }
    }

    public String a925(String s)
    {
        a632 a632_1 = new a632();
        a632_1.a643(s, null, null, null);
        int i = a345.indexOf(a632_1);
        if(i == -1)
        {
            return null;
        } else
        {
            a632 a632_2 = (a632)a345.elementAt(i);
            return a632_2.a654;
        }
    }

    public boolean a926()
    {
        if(a931 == 0)
            return true;
        if(a931 == 1)
        {
            return false;
        } else
        {
            a929();
            return a926();
        }
    }

    public Object clone()
    {
        if(a931 == 2)
            a929();
        a918 a918_1 = new a918();
        for(int i = 0; i < a345.size(); i++)
        {
            a632 a632_1 = (a632)a345.elementAt(i);
            if(a632_1.a467 == null || a632_1.a467.equals(""))
                a918_1.a643(a632_1.a651, a632_1.a652, a632_1.a653, a632_1.a654);
            else
                a918_1.a643(a632_1.a467, a632_1.a654);
        }

        a918_1.a816 = a816;
        a918_1.a817 = a817;
        a918_1.a818 = a818;
        a918_1.a819 = a819;
        a918_1.a931 = a931;
        return a918_1;
    }

    int a927()
    {
        return a345.size();
    }

    a632 a648(int i)
    {
        return (a632)a345.elementAt(i);
    }

    private void a928(a632 a632_1)
    {
        for(int i = 0; i < a345.size(); i++)
        {
            a632 a632_2 = (a632)a345.elementAt(i);
            if(a632_2.a646() > a632_1.a646())
            {
                a345.insertElementAt(a632_1, i);
                return;
            }
        }

        a345.addElement(a632_1);
    }

    private void a929()
    {
        a632 a632_1 = new a632();
        a632_1.a643("00080060", null, null, null);
        int i = a345.indexOf(a632_1);
        if(i == -1)
        {
            a931 = 0;
            a643("00080060", null, "modality", null);
        } else
        {
            a931 = 1;
            a632_1 = (a632)a345.elementAt(i);
            a632_1.a653 = "modality";
        }
        a632_1 = new a632();
        a632_1.a643("00080018", null, null, null);
        i = a345.indexOf(a632_1);
        if(i == -1)
        {
            a643("00080018", null, "SOPInstanceUID", null);
            return;
        } else
        {
            a632 a632_2 = (a632)a345.elementAt(i);
            a632_2.a653 = "SOPInstanceUID";
            return;
        }
    }

    public a918()
    {
        a931 = 2;
    }
}
