// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a0;

import a0.a127.a457;
import a0.a127.a918;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

// Referenced classes of package a0:
//            a167

public class a342
{

    Vector a344;
    a918 a345;

    public a342(URL url, String s)
    {
        a457 a457_1 = null;
        if(url == null)
        {
            try
            {
                a457_1 = new a457(new BufferedInputStream(new FileInputStream(s)));
            }
            catch(FileNotFoundException filenotfoundexception)
            {
                System.err.println(filenotfoundexception);
            }
        } else
        {
            URL url1 = null;
            try
            {
                url1 = new URL(url, s);
            }
            catch(MalformedURLException _ex)
            {
                System.err.println("MalformedURLException");
            }
            try
            {
                URLConnection urlconnection = url1.openConnection();
                urlconnection.setDefaultUseCaches(true);
                urlconnection.setUseCaches(true);
                a457_1 = new a457(new BufferedInputStream(urlconnection.getInputStream()));
            }
            catch(IOException ioexception)
            {
                System.err.println(ioexception);
            }
        }
        if(a457_1 == null)
        {
            return;
        } else
        {
            a344 = a457_1.a459();
            a345 = a457_1.a458();
            return;
        }
    }

    public boolean a343(String s)
    {
        if(a344 == null)
            return false;
        for(int i = 0; i < a344.size(); i++)
        {
            String s1 = (String)a344.elementAt(i);
            if(s1.startsWith(s))
                return true;
        }

        return false;
    }

    public void a171(a167 a167_1)
    {
        if(a345 == null)
            return;
        String s = a345.a648(null, "presentationLUTShape", null);
        if(s.equals("INVERSE"))
            a167_1.a171("invert");
    }
}
