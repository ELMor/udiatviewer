// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

public class a516 extends Component
{

    int a521;
    int a522;
    int a483;
    Color a523;
    Color a524;
    int a525;
    int a526;
    int a484;
    boolean a527;

    public void a475(int i)
    {
        a484 = i;
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        int i = getSize().width;
        int j = getSize().height;
        int k = a521 / 2;
        int l = a521 / 2;
        int i1 = i - a521;
        int j1 = j - a521;
        if(a527)
        {
            g.setColor(a523);
            g.fillRect(0, 0, i, j);
            a519(g, k, l, i1, j1);
            return;
        } else
        {
            i1--;
            j1--;
            a520(g, ++k, ++l, --i1, --j1);
            return;
        }
    }

    public Dimension getPreferredSize()
    {
        return a482();
    }

    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    public void a517(int i)
    {
        a526 = 0;
        a527 = false;
        a525 = i - 1;
        Graphics g = getGraphics();
        paint(g);
        g.dispose();
    }

    public void a518(int i)
    {
        a526 = i;
        if(i == a525)
            a527 = true;
        Graphics g = getGraphics();
        paint(g);
        g.dispose();
    }

    private Dimension a482()
    {
        if(a484 == 0)
            return new Dimension(2 * a522, a522);
        else
            return new Dimension(a484, a522);
    }

    private void a519(Graphics g, int i, int j, int k, int l)
    {
        Color color = a523.darker();
        Color color1 = a523.brighter();
        g.setColor(color);
        g.drawLine(i + a483 / 2, j, (i + k) - a483 / 2, j);
        g.setColor(color1);
        g.drawLine(i + a483 / 2, j + l, (i + k) - a483 / 2, j + l);
        g.setColor(color);
        g.drawArc(i, j, a483, l, 90, 140);
        g.setColor(color1);
        g.drawArc(i, j, a483, l, 230, 40);
        g.setColor(color);
        g.drawArc((i + k) - a483, j, a483, l, 50, 40);
        g.setColor(color1);
        g.drawArc((i + k) - a483, j, a483, l, 270, 140);
    }

    private void a520(Graphics g, int i, int j, int k, int l)
    {
        g.setColor(a524);
        g.fillArc(i, j, a483, l, 90, 180);
        if(a526 < a525)
        {
            int i1 = k - a483;
            i1 *= a526;
            i1 /= a525;
            g.fillRect(i + a483 / 2, j, i1, l);
            g.fillArc(i + i1, j, a483, l, 270, 180);
            return;
        } else
        {
            g.fillRect(i + a483 / 2, j, k - a483, l);
            g.fillArc((i + k) - a483, j, a483, l, 270, 180);
            return;
        }
    }

    public a516()
    {
        a521 = 6;
        a522 = 20;
        a483 = 17;
        a523 = Color.lightGray;
        a524 = Color.blue;
        a527 = true;
    }
}
