// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.EventObject;

// Referenced classes of package a282:
//            a540, a656, a283

public class a429 extends MouseAdapter
    implements MouseMotionListener, ActionListener
{

    static final a429 a430 = new a429();
    a540 a437;
    MouseEvent a438;
    a656 a439;
    private a283 a440;

    public static a429 a430()
    {
        return a430;
    }

    public void a431(a540 a540_1)
    {
        a540_1.removeMouseListener(this);
        a540_1.addMouseListener(this);
    }

    public void a432(a540 a540_1)
    {
        a540_1.removeMouseListener(this);
    }

    public synchronized void mouseEntered(MouseEvent mouseevent)
    {
        a540 a540_1 = (a540)mouseevent.getSource();
        Point point = mouseevent.getPoint();
        if(point.x < 0 || point.x >= a540_1.getBounds().width || point.y < 0 || point.y >= a540_1.getBounds().height)
            return;
        if(a437 != null)
            a435();
        a437 = a540_1;
        a437.addMouseMotionListener(this);
        a438 = mouseevent;
        if(a440 == null)
        {
            a440 = new a283();
            a440.start();
        } else
        if(a440.isAlive())
        {
            a440.resume();
        } else
        {
            a440 = new a283();
            a440.start();
        }
        a440.a287(this);
    }

    public synchronized void mouseExited(MouseEvent mouseevent)
    {
        a435();
    }

    public synchronized void mousePressed(MouseEvent mouseevent)
    {
        a435();
    }

    public void mouseDragged(MouseEvent mouseevent)
    {
        if(a440 != null)
            a440.a286();
    }

    public void mouseMoved(MouseEvent mouseevent)
    {
        if(a440 != null)
            a440.a286();
    }

    public synchronized void actionPerformed(ActionEvent actionevent)
    {
        String s = actionevent.getActionCommand();
        if(s.equals("showTipWindow"))
            a433();
    }

    final void a433()
    {
        if(a437 == null || !a437.isShowing())
            return;
        if(a438 == null)
            return;
        if(a439 != null)
        {
            a439.a657();
            a439 = null;
        }
        a439 = new a656(a437);
        a439.show(a438.getPoint());
    }

    final void a434()
    {
        if(a439 != null)
        {
            a439.a657();
            a439 = null;
        }
    }

    final void a435()
    {
        if(a437 != null)
            a437.removeMouseMotionListener(this);
        a437 = null;
        a438 = null;
        if(a440 != null)
        {
            a440.a288(this);
            a440.a286();
            if(a440.isAlive())
                a440.suspend();
        }
        if(a439 != null)
        {
            a439.a657();
            a439 = null;
        }
    }

    final void a436()
    {
        if(a440 != null)
            a440.a286();
    }

    public a429()
    {
    }

}
