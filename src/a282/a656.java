// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package a282;

import java.applet.Applet;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.io.PrintStream;

// Referenced classes of package a282:
//            a540

class a656 extends Canvas
{

    a540 a659;
    Font a660;

    public a656(a540 a540_1)
    {
        Font font = a540_1.getFont();
        a660 = new Font(font.getName(), font.getStyle(), font.getSize() - 2);
        a659 = a540_1;
        setSize(getPreferredSize());
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        FontMetrics fontmetrics = getFontMetrics(a660);
        int i = getSize().width;
        int j = getSize().height;
        g.setColor(Color.orange);
        g.fillRect(0, 0, i, j);
        g.setColor(Color.black);
        g.setFont(a660);
        String s = a659.a542();
        if(s == null)
            s = "null";
        g.drawString(s, 3, 2 + fontmetrics.getAscent());
        g.drawRect(0, 0, i - 1, j - 1);
    }

    public Dimension getPreferredSize()
    {
        FontMetrics fontmetrics = getFontMetrics(a660);
        String s = a659.a542();
        if(s == null)
            s = "null";
        return new Dimension(fontmetrics.stringWidth(s) + 6, fontmetrics.getHeight() + 4);
    }

    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    public void show(Point point)
    {
        java.awt.Panel panel = a659.a544();
        if(panel == null)
        {
            System.err.println("No Root Panel invoker!!!!!!!!!!!!!!!!!!!!");
            return;
        }
        Point point1 = a659.getLocationOnScreen();
        Point point2 = panel.getLocationOnScreen();
        Dimension dimension = panel.getSize();
        Rectangle rectangle = new Rectangle(point2, dimension);
        int i = point1.x + point.x;
        int j = point1.y + point.y + 20;
        int k = getSize().width;
        int l = getSize().height;
        int i1 = rectangle.x + rectangle.width;
        int j1 = rectangle.y + rectangle.height;
        if(i < rectangle.x)
            i = rectangle.x;
        if(j < rectangle.y)
            j = rectangle.y;
        if(i + k > i1)
            i = i1 - k;
        if(j + l > j1)
            j = j1 - l;
        Point point3 = new Point(i, j);
        a658(point3, panel);
        setBounds(point3.x, point3.y, k, l);
        panel.add(this);
        repaint();
    }

    public void a657()
    {
        Container container = getParent();
        Rectangle rectangle = getBounds();
        if(container != null)
        {
            container.remove(this);
            container.repaint(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        }
    }

    final void a658(Point point, Component component)
    {
        do
        {
            int i;
            int j;
            if(component instanceof Applet)
            {
                Point point1 = component.getLocationOnScreen();
                i = point1.x;
                j = point1.y;
            } else
            {
                Rectangle rectangle = component.getBounds();
                i = rectangle.x;
                j = rectangle.y;
            }
            point.x -= i;
            point.y -= j;
            if(component instanceof Window)
                break;
            if(component instanceof Applet)
                return;
            component = component.getParent();
        } while(component != null);
    }
}
